//
//  CapitalAdequacyViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 1/13/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CapitalAdequacyViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *netWorthTotalAssetsLabelField;
@property (strong, nonatomic) IBOutlet UILabel *tlDelinquentLoanNetWorthLabelField;
@property (strong, nonatomic) IBOutlet UILabel *solvencyEquationLabelField;
@property (strong, nonatomic) IBOutlet UILabel *classifiedAssetsNetWorthLabelField;

@end
