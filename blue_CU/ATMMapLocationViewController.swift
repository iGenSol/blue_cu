//
//  ATMMapLocationViewController.swift
//  blue_CU
//
//  Created by Timothy Milz on 7/24/19.
//  Copyright © 2019 iGenerateSolutions Inc. All rights reserved.
//
//  Converted to Swift 5 by Swiftify v5.0.7505 - https://objectivec2swift.com/
//
//  ATMMapLocationViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 11/20/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

// import Foundation

class ATMMapLocationViewController {
    let debug = 1
    func viewDidLoad() {
        super.viewDidLoad()
        
        // Create instance of location manager
        if locationManager == nil {
            locationManager = CLLocationManager()
            locationManager.delegate = self
        } else {
            nil
        }
        
        if locationManager.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization)) {
            locationManager.requestWhenInUseAuthorization()
        } else {
            nil
        }
        
        // Display map with user location and ATM location
        let strRR = "\(passed_phys_adr_1),\(passed_phys_adr_city),\(passed_phys_adr_state),\(passed_phys_adr_zip)"
        
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(strRR, completionHandler: { placemarks, error in
            if error != nil {
                if let error = error {
                    print("\(error)")
                }
            } else {
                let placemark = placemarks?.last as? CLPlacemark
                //float spanX = 0.00725;
                //float spanY = 0.00725;
                let spanX: Float = 0.1
                let spanY: Float = 0.1
                let region: MKCoordinateRegion
                region.center.latitude = placemark?.location?.coordinate.latitude ?? 0
                region.center.longitude = placemark?.location?.coordinate.longitude ?? 0
                region.span = MKCoordinateSpanMake(spanX, spanY)
                
                // Create coordinate
                let myCoordinate = [region.center.latitude, region.center.longitude] as? CLLocationCoordinate2D
                //Create annotation
                let point = MKPointAnnotation()
                // Set annotation to point at coordinate
                if let myCoordinate = myCoordinate {
                    point.coordinate = myCoordinate
                }
                // Set title and subtitle for annotation
                point.title = self.passed_cu_name
                point.subtitle = self.passed_atm_name
                //Drop pin on map
                self.atmMap.addAnnotation(point)
                
                self.atmMap.setRegion(region, animated: true)
            }
        })
        
    }
    
    // Zoom in on location of ATM (presently zooming in on user location)
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        
        if debug == 1 {
            print("Running \(ATMMapLocationViewController) '\(NSStringFromSelector(#function))'")
        }
        /*
         NSString *strRR = [NSString stringWithFormat:@"%@,%@,%@,%@", self.passed_phys_adr_1, self.passed_phys_adr_city, self.passed_phys_adr_state, self.passed_phys_adr_zip];
         
         CLGeocoder *geocoder = [[CLGeocoder alloc] init];
         [geocoder geocodeAddressString:strRR completionHandler:^(NSArray *placemarks, NSError *error) {
         if (error) {
         NSLog(@"%@", error);
         } else {
         CLPlacemark *placemark = [placemarks lastObject];
         //float spanX = 0.00725;
         //float spanY = 0.00725;
         float spanX = .1;
         float spanY = .1;
         MKCoordinateRegion region;
         region.center.latitude = placemark.location.coordinate.latitude;
         region.center.longitude = placemark.location.coordinate.longitude;
         region.span = MKCoordinateSpanMake(spanX, spanY);
         
         // Create coordinate
         CLLocationCoordinate2D myCoordinate = {region.center.latitude, region.center.longitude};
         //Create annotation
         MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
         // Set annotation to point at coordinate
         point.coordinate = myCoordinate;
         // Set title and subtitle for annotation
         [point setTitle:self.passed_cu_name];
         [point setSubtitle:self.passed_atm_name];
         //Drop pin on map
         [self.atmMap addAnnotation:point];
         
         [self.atmMap setRegion:region animated:YES];
         }
         }];
         */
    }
    
    func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        if debug == 1 {
            print("Running \(ATMMapLocationViewController) '\(NSStringFromSelector(#function))'")
        }
        
        if (error as NSError).code == CLError.Code.locationUnknown.rawValue {
            print("LOCATION ERROR -> Currently unable to retrieve location.")
        } else if (error as NSError).code == CLError.Code.network.rawValue {
            print("LOCATION ERROR -> Network used to retrieve location is unavailable.")
        } else if (error as NSError).code == CLError.Code.denied.rawValue {
            // Turn off location manager updates
            print("LOCATION ERROR -> Permission to retrieve location is denied.")
            //[self.locMan stopUpdatingLocation];
        }
    }
    /*
     #pragma mark - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
    
}
