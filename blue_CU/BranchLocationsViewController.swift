//
//  BranchLocationsViewController.swift
//  blue_CU
//
//  Created by Timothy Milz on 7/24/19.
//  Copyright © 2019 iGenerateSolutions Inc. All rights reserved.
//
//  Converted to Swift 5 by Swiftify v5.0.7505 - https://objectivec2swift.com/
//
//  BranchLocationsViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 11/28/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

// import Foundation

let debug = 1
var branchPhoneNumber = ""
// Variables for Map View
var branch_cu_name = ""
var branch_site_name = ""
var branch_phys_adr_1 = ""
var branch_phys_adr_city = ""
var branch_phys_adr_state = ""
var branch_phys_adr_zip = ""
var branch_phys_adr_country = ""

class BranchLocationsViewController {
    func hideKeyBoardWhenBackgroundIsTapped() {
        
        if debug == 1 {
            print("Running \(BranchLocationsViewController) '\(NSStringFromSelector(#function))'")
        }
        
        let tgr = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tgr.cancelsTouchesInView = false
        view.addGestureRecognizer(tgr)
    }
    
    @objc func hideKeyboard() {
        
        if debug == 1 {
            print("Running \(BranchLocationsViewController) '\(NSStringFromSelector(#function))'")
        }
        
        view.endEditing(true)
    }
    
    // MARK: - VIEW
    func refreshInterface() {
        
        if debug == 1 {
            print("Running \(BranchLocationsViewController) '\(NSStringFromSelector(#function))'")
        }
        
        if selectedBranchID {
            let cdh = (UIApplication.shared.delegate as? AppDelegate)?.cdh()
            var branchLocations: BranchLocations? = nil
            do {
                branchLocations = try cdh?.context.existingObject(with: selectedBranchID) as? BranchLocations
            } catch {
            }
            
            site_nameLabelField.text = branchLocations?.site_name
            site_typeLabelField.text = branchLocations?.site_type_name
            phys_adr_1LabelField.text = branchLocations?.phys_adr_1
            phys_adr_2LabelField.text = branchLocations?.phys_adr_2
            phys_adr_cityLabelField.text = branchLocations?.phys_adr_city
            phys_adr_county_nameLabelField.text = branchLocations?.phys_adr_county_name
            phys_adr_state_codeLabelField.text = branchLocations?.phys_adr_state_code
            phys_adr_postal_codeLabelField.text = branchLocations?.phys_adr_postal_code
            phys_adr_countryLabelField.text = branchLocations?.phys_adr_country
            
            let formattedBranchPhoneNumber = "\(branchLocations?.phone_number.substring(with: NSRange(location: 0, length: 3)) ?? "")-\(branchLocations?.phone_number.substring(with: NSRange(location: 3, length: 3)) ?? "")-\(branchLocations?.phone_number.substring(with: NSRange(location: 6, length: 4)) ?? "")"
            branchCallPhone.setTitle(formattedBranchPhoneNumber, for: .normal)
            branchPhoneNumber = formattedBranchPhoneNumber
            
            // setup map view variables
            branch_cu_name = branchLocations?.cu_name ?? ""
            branch_site_name = branchLocations?.site_name ?? ""
            branch_phys_adr_1 = branchLocations?.phys_adr_1 ?? ""
            branch_phys_adr_city = branchLocations?.phys_adr_city ?? ""
            branch_phys_adr_state = branchLocations?.phys_adr_state_code ?? ""
            branch_phys_adr_zip = branchLocations?.phys_adr_postal_code ?? ""
            branch_phys_adr_country = branchLocations?.phys_adr_country ?? ""
        }
    }
    
    func viewDidLoad() {
        
        if debug == 1 {
            print("Running \(BranchLocationsViewController) '\(NSStringFromSelector(#function))'")
        }
        
        super.viewDidLoad()
        hideKeyBoardWhenBackgroundIsTapped()
    }
    
    func viewWillAppear(_ animated: Bool) {
        
        if debug == 1 {
            print("Running \(BranchLocationsViewController) '\(NSStringFromSelector(#function))'")
        }
        
        refreshInterface()
    }
    
    func viewDidDisappear(_ animated: Bool) {
        
        if debug == 1 {
            print("Running \(BranchLocationsViewController) '\(NSStringFromSelector(#function))'")
        }
    }
    
    // MARK: - SEGUE
    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if debug == 1 {
            print("Running \(BranchLocationsViewController) '\(NSStringFromSelector(#function))'")
        }
        
        let branchMapLocationsVC = segue.destination as? BranchMapLocationViewController
        if (segue.identifier == "Show Branch Map Segue") {
            branchMapLocationsVC?.passed_cu_name = branch_cu_name
            branchMapLocationsVC?.passed_site_name = branch_site_name
            branchMapLocationsVC?.passed_phys_adr_1 = branch_phys_adr_1
            branchMapLocationsVC?.passed_phys_adr_city = branch_phys_adr_city
            branchMapLocationsVC?.passed_phys_adr_state = branch_phys_adr_state
            branchMapLocationsVC?.passed_phys_adr_zip = branch_phys_adr_zip
            branchMapLocationsVC?.passed_phys_adr_country = branch_phys_adr_country
        } else {
            print("Unidentified Segue Attempted")
        }
    }
    
    // MARK: - Call Phone Method
    @IBAction func callBranchPhone(_ sender: Any) {
        let phoneNumberUrl = "telprompt://" + (branchPhoneNumber)
        if let url = URL(string: phoneNumberUrl) {
            UIApplication.shared.openURL(url)
        }
    }
}
