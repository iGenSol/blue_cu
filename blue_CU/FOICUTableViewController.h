//
//  FOICUTableViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 10/29/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import "CoreDataTVC.h"
#import "CoreDataHelper.h"

@interface FOICUTableViewController : CoreDataTVC

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *foicuActivityIndicator;

//@property (strong, nonatomic) NSManagedObjectID *selectedCUID;
//@property (strong, nonatomic) NSString *passedCUID;

// Passed strings for search predicate on table view
@property (strong, nonatomic) NSString *passedCUNameSearch;
@property (strong, nonatomic) NSString *passedCUStateSearch;

@end
