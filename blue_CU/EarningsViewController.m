//
//  EarningsViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 1/14/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "EarningsViewController.h"
#import "AppDelegate.h"
#import "FS220+CoreDataClass.h"
#import "FS220A+CoreDataClass.h"
#import "FS220B+CoreDataClass.h"
#import "FS220_PYE+CoreDataClass.h"
#import "FS220A_PYE+CoreDataClass.h"
#import "RatioTabBarController.h"

@interface EarningsViewController ()

@end

@implementation EarningsViewController

#define debug 1
#define cycle_date 06

NSString *earningsDataCUNumber = @"";

NSString *earningsfs220_007 = @"";
NSString *earningsfs220_008 = @"";
NSString *earningsfs220_010 = @"";
NSString *earningsfs220_025B = @"";
NSString *earningsfs220_300 = @"";
NSString *earningsfs220_340 = @"";
NSString *earningsfs220_380 = @"";
NSString *earningsfs220_671 = @"";
NSString *earningsfs220_980 = @"";

NSString *earningsfs220_010_PYE = @"";
NSString *earningsfs220_025B_PYE = @"";

NSString *earningsfs220a_110 =@"";
NSString *earningsfs220a_115 =@"";
NSString *earningsfs220a_119 =@"";
NSString *earningsfs220a_120 =@"";
NSString *earningsfs220a_124 =@"";
NSString *earningsfs220a_131 =@"";
NSString *earningsfs220a_350 =@"";
NSString *earningsfs220a_381 =@"";
NSString *earningsfs220a_661A =@"";
NSString *earningsfs220a_730B =@"";
NSString *earningsfs220a_730C =@"";
NSString *earningsfs220a_798A =@"";
NSString *earningsfs220a_799I =@"";

NSString *earningsfs220a_799I_PYE = @"";
NSString *earningsfs220a_730B_PYE = @"";
NSString *earningsfs220a_730C_PYE = @"";

NSString *earningsfs220b_659 =@"";

//NSString *earningsfs220g_440A =@"";
//NSString *earningsfs220g_660A =@"";

#pragma mark - VIEW
-(void)refreshInterface {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }

    // Blank out in case no record found
    self.ROALabelField.text = @"0.00";
    self.yieldOnAvgLoansLabelField.text = @"0.00";
    self.feeIncAvgAssetsLabelField.text = @"0.00";
    self.netMarginAvgAssetsLabelField.text = @"0.00";
    self.loanLossAvgAssetsLabelField.text = @"0.00";
    self.opExGrossIncomeLabelField.text = @"0.00";
    self.opExpAvgAssetsLabelField.text = @"0.00";
    self.incomeAssetsLabelField.text = @"0.00";
    self.yieldOnAvgInvestmentsLabelField.text = @"0.00";
    self.costOfFundsAvgAssetsLabelField.text = @"0.00";
    self.netOpExpAvgAssetsLabelField.text = @"0.00";
    self.netInterestMarginAvgAssetsLabelField.text = @"0.00";
    self.assetsTLAssetsLabelField.text = @"0.00";
    
    // Get credit union number for fetch from ExtendedInfoTabBarController
    RatioTabBarController *tabbar = (RatioTabBarController *)self.tabBarController;
    earningsDataCUNumber = [tabbar.receivedCUNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];

    // Get FS220 Data
    NSFetchRequest *fs220FetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220"];
    NSSortDescriptor *fs220SortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220FetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220SortDescriptor]];
    NSPredicate *fs220Filter = [NSPredicate predicateWithFormat:@"cu_number == %@", earningsDataCUNumber];
    [fs220FetchRequest setPredicate:fs220Filter];
    NSArray *fetchedFS220 = [cdh.context executeFetchRequest:fs220FetchRequest error:nil];
    
    for (FS220 *fs220 in fetchedFS220) {

        earningsfs220_007 = fs220.acct_007;
        earningsfs220_008 = fs220.acct_008;
        earningsfs220_010 = fs220.acct_010;
        earningsfs220_025B = fs220.acct_025B;
        earningsfs220_300 = fs220.acct_300;
        earningsfs220_340 = fs220.acct_340;
        earningsfs220_380 = fs220.acct_380;
        earningsfs220_671 = fs220.acct_671;
        earningsfs220_980 = fs220.acct_980;
    }
    
    // Get FS220_PYE Data
    NSFetchRequest *fs220PYEFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220_PYE"];
    NSSortDescriptor *fs220PYESortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220PYEFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220PYESortDescriptor]];
    NSPredicate *fs220PYEFilter = [NSPredicate predicateWithFormat:@"cu_number == %@", earningsDataCUNumber];
    [fs220PYEFetchRequest setPredicate:fs220PYEFilter];
    NSArray *fetchedFS220PYE = [cdh.context executeFetchRequest:fs220PYEFetchRequest error:nil];
    
    for (FS220_PYE *fs220PYE in fetchedFS220PYE) {
        
        earningsfs220_010_PYE = fs220PYE.acct_010;
        earningsfs220_025B_PYE = fs220PYE.acct_025B;
    }
    
    // Get FS220A Data
    NSFetchRequest *fs220aFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220A"];
    NSSortDescriptor *fs220aSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220aFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220aSortDescriptor]];
    NSPredicate *fs220aFilter = [NSPredicate predicateWithFormat:@"cu_number == %@", earningsDataCUNumber];
    [fs220aFetchRequest setPredicate:fs220aFilter];
    NSArray *fetchedFS220a = [cdh.context executeFetchRequest:fs220aFetchRequest error:nil];
    
    for (FS220A *fs220a in fetchedFS220a) {
        
        earningsfs220a_110 = fs220a.acct_110;
        earningsfs220a_115 = fs220a.acct_115;
        earningsfs220a_119 = fs220a.acct_119;
        earningsfs220a_120 = fs220a.acct_120;
        earningsfs220a_124 = fs220a.acct_124;
        earningsfs220a_131 = fs220a.acct_131;
        earningsfs220a_350 = fs220a.acct_350;
        earningsfs220a_381 = fs220a.acct_381;
        earningsfs220a_661A = fs220a.acct_661A;
        earningsfs220a_730B = fs220a.acct_730B;
        earningsfs220a_730C = fs220a.acct_730C;
        earningsfs220a_798A = fs220a.acct_798A;
        earningsfs220a_799I = fs220a.acct_799I;
    }
    
    // Get FS220A_PYE Data
    NSFetchRequest *fs220aPYEFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220A_PYE"];
    NSSortDescriptor *fs220aPYESortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220aPYEFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220aPYESortDescriptor]];
    NSPredicate *fs220aPYEFilter = [NSPredicate predicateWithFormat:@"cu_number == %@", earningsDataCUNumber];
    [fs220aPYEFetchRequest setPredicate:fs220aPYEFilter];
    NSArray *fetchedFS220aPYE = [cdh.context executeFetchRequest:fs220aPYEFetchRequest error:nil];
    
    for (FS220A_PYE *fs220aPYE in fetchedFS220aPYE) {
        
        earningsfs220a_799I_PYE = fs220aPYE.acct_799I;
        earningsfs220a_730B_PYE = fs220aPYE.acct_730B;
        earningsfs220a_730C_PYE = fs220aPYE.acct_730C;
    }
    
    // Get FS220b Data
    NSFetchRequest *fs220bFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220B"];
    NSSortDescriptor *fs220bSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220bFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220bSortDescriptor]];
    NSPredicate *fs220bFilter = [NSPredicate predicateWithFormat:@"cu_number == %@", earningsDataCUNumber];
    [fs220bFetchRequest setPredicate:fs220bFilter];
    NSArray *fetchedFS220b = [cdh.context executeFetchRequest:fs220bFetchRequest error:nil];
    
    for (FS220B *fs220b in fetchedFS220b) {
        
        earningsfs220b_659 = fs220b.acct_659;
    }
    
    [self calculateRatios];
}

-(void)viewDidLoad {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [self refreshInterface];
}

-(void)viewDidDisappear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
}

#pragma mark - STRING CONVERSION METHOD
-(NSNumber*)convertStringToNumberForTextFields:(NSString*)passedString {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    float convertedNumber = 0;
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    convertedNumber = [[numberFormatter numberFromString: passedString] floatValue];
    NSNumber *returnedNumber = [NSNumber numberWithFloat:convertedNumber];
    
    return returnedNumber;
}


#pragma mark - CALCULATE RATIOS
-(void)calculateRatios {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    // Setup variables
    NSNumber *returnedNumberForfs220_007 = [self convertStringToNumberForTextFields:earningsfs220_007];
    NSNumber *returnedNumberForfs220_008 = [self convertStringToNumberForTextFields:earningsfs220_008];
    NSNumber *returnedNumberForfs220_010 = [self convertStringToNumberForTextFields:earningsfs220_010];
    NSNumber *returnedNumberForfs220_025B = [self convertStringToNumberForTextFields:earningsfs220_025B];
    NSNumber *returnedNumberForfs220_300 = [self convertStringToNumberForTextFields:earningsfs220_300];
    NSNumber *returnedNumberForfs220_340 = [self convertStringToNumberForTextFields:earningsfs220_340];
    NSNumber *returnedNumberForfs220_380 = [self convertStringToNumberForTextFields:earningsfs220_380];
    NSNumber *returnedNumberForfs220_671 = [self convertStringToNumberForTextFields:earningsfs220_671];
    NSNumber *returnedNumberForfs220_980 = [self convertStringToNumberForTextFields:earningsfs220_980];
   
    NSNumber *returnedNumberForfs220_010_PYE = [self convertStringToNumberForTextFields:earningsfs220_010_PYE];
    NSNumber *returnedNumberForfs220_025B_PYE = [self convertStringToNumberForTextFields:earningsfs220_025B_PYE];
    
    NSNumber *returnedNumberForfs220a_110 = [self convertStringToNumberForTextFields:earningsfs220a_110];
    NSNumber *returnedNumberForfs220a_115 = [self convertStringToNumberForTextFields:earningsfs220a_115];
    NSNumber *returnedNumberForfs220a_119 = [self convertStringToNumberForTextFields:earningsfs220a_119];
    NSNumber *returnedNumberForfs220a_120 = [self convertStringToNumberForTextFields:earningsfs220a_120];
    NSNumber *returnedNumberForfs220a_124 = [self convertStringToNumberForTextFields:earningsfs220a_124];
    NSNumber *returnedNumberForfs220a_131 = [self convertStringToNumberForTextFields:earningsfs220a_131];
    NSNumber *returnedNumberForfs220a_350 = [self convertStringToNumberForTextFields:earningsfs220a_350];
    NSNumber *returnedNumberForfs220a_381 = [self convertStringToNumberForTextFields:earningsfs220a_381];
    NSNumber *returnedNumberForfs220a_661A = [self convertStringToNumberForTextFields:earningsfs220a_661A];
    NSNumber *returnedNumberForfs220a_730B = [self convertStringToNumberForTextFields:earningsfs220a_730B];
    NSNumber *returnedNumberForfs220a_730C = [self convertStringToNumberForTextFields:earningsfs220a_730C];
    NSNumber *returnedNumberForfs220a_798A = [self convertStringToNumberForTextFields:earningsfs220a_798A];
    NSNumber *returnedNumberForfs220a_799I = [self convertStringToNumberForTextFields:earningsfs220a_799I];
    
    NSNumber *returnedNumberForfs220a_799I_PYE = [self convertStringToNumberForTextFields:earningsfs220a_799I_PYE];
    NSNumber *returnedNumberForfs220a_730B_PYE = [self convertStringToNumberForTextFields:earningsfs220a_730B_PYE];
    NSNumber *returnedNumberForfs220a_730C_PYE = [self convertStringToNumberForTextFields:earningsfs220a_730C_PYE];
    
    NSNumber *returnedNumberForfs220b_659 = [self convertStringToNumberForTextFields:earningsfs220b_659];
    
    //NSNumber *returnedNumberForfs220g_440A = [self convertStringToNumberForTextFields:earningsfs220g_440A];
    //NSNumber *returnedNumberForfs220g_660A = [self convertStringToNumberForTextFields:earningsfs220g_660A];
    
    
    float f007 = [returnedNumberForfs220_007 floatValue];
    float f008 = [returnedNumberForfs220_008 floatValue];
    float f010 = [returnedNumberForfs220_010 floatValue];
    float f025B = [returnedNumberForfs220_025B floatValue];
    float f300 = [returnedNumberForfs220_300 floatValue];
    float f340 = [returnedNumberForfs220_340 floatValue];
    float f380 = [returnedNumberForfs220_380 floatValue];
    float f671 = [returnedNumberForfs220_671 floatValue];
    float f980 = [returnedNumberForfs220_980 floatValue];
    
    float f010_PYE = [returnedNumberForfs220_010_PYE floatValue];
    float f025B_PYE = [returnedNumberForfs220_025B_PYE floatValue];
    
    float f110 = [returnedNumberForfs220a_110 floatValue];
    float f115 = [returnedNumberForfs220a_115 floatValue];
    float f119 = [returnedNumberForfs220a_119 floatValue];
    float f120 = [returnedNumberForfs220a_120 floatValue];
    float f124 = [returnedNumberForfs220a_124 floatValue];
    float f131 = [returnedNumberForfs220a_131 floatValue];
    float f350 = [returnedNumberForfs220a_350 floatValue];
    float f381 = [returnedNumberForfs220a_381 floatValue];
    float f661A = [returnedNumberForfs220a_661A floatValue];
    float f730B = [returnedNumberForfs220a_730B floatValue];
    float f730C = [returnedNumberForfs220a_730C floatValue];
    float f798A = [returnedNumberForfs220a_798A floatValue];
    float f799I = [returnedNumberForfs220a_799I floatValue];

    float f799I_A_PYE = [returnedNumberForfs220a_799I_PYE floatValue];
    float f730B_A_PYE = [returnedNumberForfs220a_730B_PYE floatValue];
    float f730C_A_PYE = [returnedNumberForfs220a_730C_PYE floatValue];
    
    float f659 = [returnedNumberForfs220b_659 floatValue];

    //float f440A = [returnedNumberForfs220g_440A floatValue];
    //float f660A = [returnedNumberForfs220g_660A floatValue];
    
    // Calculate ROA (display rounded)
    if (((f010 + f010_PYE)/2) != 0) {
        float ROARatio = ((f661A / ((f010 + f010_PYE)/2)) * 100) * 12/cycle_date;
        NSString *formattedROARatio = [NSString stringWithFormat:@"%.02f", ROARatio];
        self.ROALabelField.text = formattedROARatio;
    } else {
        self.ROALabelField.text = @"0.00";
    }

    // Calculate Yield on Average Loans (display rounded)
    if (((f025B + f025B_PYE)/2) != 0) {
        float yieldRatio = (((f110 - f119) / ((f025B + f025B_PYE)/2)) * 100) * 12/cycle_date;
        NSString* formattedYieldRatio = [NSString stringWithFormat:@"%.02f", yieldRatio];
        self.yieldOnAvgLoansLabelField.text = formattedYieldRatio;
    } else {
        self.yieldOnAvgLoansLabelField.text = @"0.00";
    }
    
    // Calculate Fee Income / Avg Assets (display rounded)
    if ((f131 + f659) != 0) {
        float feeIncomeRatio = (((f131 + f659) / ((f010 + f010_PYE)/2)) * 100) *12/cycle_date;
        NSString* formattedFeeIncomeRatio = [NSString stringWithFormat:@"%.02f", feeIncomeRatio];
        self.feeIncAvgAssetsLabelField.text = formattedFeeIncomeRatio;
    } else {
        self.feeIncAvgAssetsLabelField.text = @"0.00";
    }
    
    // Calculate Net Margin / Avg Assets (display rounded)
    if (((f010 + f010_PYE)/2) != 0) {
        float netMarginRatio = ((((f115 + f131 + f659) - f350) / ((f010 + f010_PYE)/2)) * 100) *12/cycle_date;
        NSString* formattedNetMarginRatio = [NSString stringWithFormat:@"%.02f", netMarginRatio];
        self.netMarginAvgAssetsLabelField.text = formattedNetMarginRatio;
    } else {
        self.netMarginAvgAssetsLabelField.text = @"0.00";
    }
    
    // Calculate Provision for Loan Loss / Avg Assets (display rounded)
    if (f300 != 0) {
        float loanLossRatio = ((f300 / ((f010 + f010_PYE)/2)) * 100) *12/cycle_date;
        NSString* formattedLoanLossRatio = [NSString stringWithFormat:@"%.02f", loanLossRatio];
        self.loanLossAvgAssetsLabelField.text = formattedLoanLossRatio;
    } else {
        self.loanLossAvgAssetsLabelField.text = @"0.00";
    }

    // Calculate Operating Expenses / Gross Income (display rounded)
    if (f115+f131+f659 != 0) {
        float opExIncomeRatio = (f671 / (f115+f131+f659)) * 100;
        NSString* formattedOpExIncomeRatio = [NSString stringWithFormat:@"%.02f", opExIncomeRatio];
        self.opExGrossIncomeLabelField.text = formattedOpExIncomeRatio;
    } else {
        self.opExGrossIncomeLabelField.text = @"0.00";
    }
    
    // Calculate Net Interest Margin / Avg Assets (display rounded)
    if (((f010 + f010_PYE)/2) != 0) {
        float netInterestMarginRatio = (((f115 - f350) / ((f010 + f010_PYE)/2)) * 100) *12/cycle_date;
        NSString* formattedNetInterestMarginRatio = [NSString stringWithFormat:@"%.02f", netInterestMarginRatio];
        self.netInterestMarginAvgAssetsLabelField.text = formattedNetInterestMarginRatio;
    } else {
        self.netInterestMarginAvgAssetsLabelField.text = @"0.00";
    }
 
    // Calculate Gross Income / Average Assets (display rounded)
    if (((f010 + f010_PYE)/2) != 0) {
        float grossIncAssetsRatio = (((f115+f131+f659) / ((f010 + f010_PYE)/2)) * 100) * 12/cycle_date;
        NSString* formattedGrossIncAssetsRatio = [NSString stringWithFormat:@"%.02f", grossIncAssetsRatio];
        self.incomeAssetsLabelField.text = formattedGrossIncAssetsRatio;
    } else {
        self.incomeAssetsLabelField.text = @"0.00";
    }
 
    // Calculate Yield on Average Investments (display rounded)
    if (((f799I+f730B+f730C+f799I_A_PYE+f730B_A_PYE+f730C_A_PYE)/2) != 0) {
        float yieldAvgInvRatio = (((f120 + f124) / ((f799I+f730B+f730C+f799I_A_PYE+f730B_A_PYE+f730C_A_PYE)/2)) * 100) * 12/cycle_date;
        NSString* formattedYieldAvgInvRatio = [NSString stringWithFormat:@"%.02f", yieldAvgInvRatio];
        self.yieldOnAvgInvestmentsLabelField.text = formattedYieldAvgInvRatio;
    } else {
        self.yieldOnAvgInvestmentsLabelField.text = @"0.00";
    }
    
    // Calculate Cost of Funds / Average Assets (display rounded)
    if (((f010 + f010_PYE)/2) != 0) {
        float costOfFundsRatio = (((f340 + f380 + f381) / ((f010 + f010_PYE)/2)) * 100) * 12/cycle_date;
        NSString* formattedCostOfFundsRatio = [NSString stringWithFormat:@"%.02f", costOfFundsRatio];
        self.costOfFundsAvgAssetsLabelField.text = formattedCostOfFundsRatio;
    } else {
        self.costOfFundsAvgAssetsLabelField.text = @"0.00";
    }

    // Calculate Operating Expense / Average Assets (display rounded)
    if (((f010 + f010_PYE)/2) != 0) {
        float opExpAvgAssetsRatio = ((f671 / ((f010 + f010_PYE)/2)) * 100) * 12/cycle_date;
        NSString* formattedOpExpAvgAssetsRatio = [NSString stringWithFormat:@"%.02f", opExpAvgAssetsRatio];
        self.opExpAvgAssetsLabelField.text = formattedOpExpAvgAssetsRatio;
    } else {
        self.opExpAvgAssetsLabelField.text = @"0.00";
    }
    
    // Calculate Net Operating Expense / Average Assets (display rounded)
    if (((f010 + f010_PYE)/2) != 0) {
        float netOpExpAvgAssetsRatio = (((f671 - f131) / ((f010 + f010_PYE)/2)) * 100) * 12/cycle_date;
        NSString* formattedNetOpExpAvgAssetsRatio = [NSString stringWithFormat:@"%.02f", netOpExpAvgAssetsRatio];
        self.netOpExpAvgAssetsLabelField.text = formattedNetOpExpAvgAssetsRatio;
    } else {
        self.netOpExpAvgAssetsLabelField.text = @"0.00";
    }

    // Calculate Fixed, Foreclosed, Repossesed Assets / Total Assets (display rounded)
    if (f010 != 0) {
        float assetsTotalAssetsRatio = ((f007 +f008 + f798A +f980) / f010) * 100;
        NSString* formattedAssetsTotalAssetsRatio = [NSString stringWithFormat:@"%.02f", assetsTotalAssetsRatio];
        self.assetsTLAssetsLabelField.text = formattedAssetsTotalAssetsRatio;
    } else {
        self.assetsTLAssetsLabelField.text = @"0.00";
    }
}


@end
