//
//  CreditUnionWebsiteViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 12/21/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import "AppDelegate.h"
#import "CreditUnionWebsiteViewController.h"


@interface CreditUnionWebsiteViewController ()

@end

@implementation CreditUnionWebsiteViewController

#define debug 1

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tabBarController.delegate = self;
    self.cuWebView.delegate = self;
    self.tabBarController.title = @"Credit Union Website";
    
    //Create a URL object
    NSURL *websiteUrl = [NSURL URLWithString:self.receivedCUWebsiteAddress];
    
    //URL Requst Object
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
    
    //Load the request in the UIWebView
    [self.cuWebView loadRequest:urlRequest];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
