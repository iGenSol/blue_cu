//
//  IndustryLoanInfoViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 3/21/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IndustryLoanInfoViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *totalLoansLabelField;
@property (strong, nonatomic) IBOutlet UILabel *averageLoanBalanceLabelField;
@property (strong, nonatomic) IBOutlet UILabel *loanToshareRatioLabelField;
@property (strong, nonatomic) IBOutlet UILabel *mortgagesRealEstateLabelField;
@property (strong, nonatomic) IBOutlet UILabel *autoLoansLabelField;
@property (strong, nonatomic) IBOutlet UILabel *unsecuredCreditCardsLabelField;
@property (strong, nonatomic) IBOutlet UILabel *otherLabelField;
@property (strong, nonatomic) IBOutlet UILabel *delinquencyRatioLabelField;


@end
