//
//  ExtendedInfoViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 11/6/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExtendedInfoViewController : UIViewController <UITabBarControllerDelegate>

//@property (strong, nonatomic) NSManagedObjectID *selectedCUID;
@property (weak, nonatomic) IBOutlet UILabel *charterStateLabelField;
@property (weak, nonatomic) IBOutlet UILabel *congDistLabelField;
@property (weak, nonatomic) IBOutlet UILabel *countyCodeLabelField;
@property (weak, nonatomic) IBOutlet UILabel *cuTypeLabelField;
@property (weak, nonatomic) IBOutlet UILabel *ncuaDistrictLabelField;
@property (weak, nonatomic) IBOutlet UILabel *charterInsuranceEffectiveDateLabelField;
@property (weak, nonatomic) IBOutlet UILabel *limitedIncomeCodeLabelField;
@property (weak, nonatomic) IBOutlet UILabel *peerGroupLabelField;
@property (weak, nonatomic) IBOutlet UILabel *quarterFlagLabelField;
@property (weak, nonatomic) IBOutlet UILabel *ncuaRegionLabelField;
@property (weak, nonatomic) IBOutlet UILabel *rssdLabelField;
@property (weak, nonatomic) IBOutlet UILabel *smsaLabelField;

// Define variables passed for Extended Info View, from FOICUViewController
@property (weak, nonatomic) NSString *receivedCharterState;
@property (weak, nonatomic) NSString *receivedCongDist;
@property (weak, nonatomic) NSString *receivedCountyCode;
@property (weak, nonatomic) NSString *receivedCUType;
@property (weak, nonatomic) NSString *receivedDistrict;
@property (weak, nonatomic) NSString *receivedIssueDate;
@property (weak, nonatomic) NSString *receivedLimitedInc;
@property (weak, nonatomic) NSString *receivedPeerGroup;
@property (weak, nonatomic) NSString *receivedQuarterFlag;
@property (weak, nonatomic) NSString *receivedRegion;
@property (weak, nonatomic) NSString *receivedSSD;
@property (weak, nonatomic) NSString *receivedSMSA;


@end
