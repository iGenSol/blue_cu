//
//  ATMMapLocationViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 11/20/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@import CoreLocation;
@import MapKit;

@interface ATMMapLocationViewController : UIViewController <CLLocationManagerDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *atmMap;
@property (strong, nonatomic) CLLocationManager *locationManager;

@property (strong, nonatomic) NSString *passed_cu_name;
@property (strong, nonatomic) NSString *passed_atm_name;
@property (strong, nonatomic) NSString *passed_phys_adr_1;
@property (strong, nonatomic) NSString *passed_phys_adr_city;
@property (strong, nonatomic) NSString *passed_phys_adr_state;
@property (strong, nonatomic) NSString *passed_phys_adr_zip;

@end
