//
//  GeneralIndustryStatisticsViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 3/20/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GeneralIndustryStatisticsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *federallyInsuredCUCountLabelField;
@property (strong, nonatomic) IBOutlet UILabel *totalMembersLabelField;
@property (strong, nonatomic) IBOutlet UILabel *totalAssetsLabelField;
@property (strong, nonatomic) IBOutlet UILabel *averageCUAssetsLabelField;
@property (strong, nonatomic) IBOutlet UILabel *returnOnAverageAssetsLabelField;
@property (strong, nonatomic) IBOutlet UILabel *totalInsuredSharesDepositsLabelField;
@property (strong, nonatomic) IBOutlet UILabel *netIncomeLabelField;
@property (strong, nonatomic) IBOutlet UILabel *netWorthRatioLabelField;
@property (strong, nonatomic) IBOutlet UILabel *averageSharesPerMemberLabelField;

@end
