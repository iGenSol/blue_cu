//
//  ProductivityViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 1/15/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductivityViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *membersPotentialMembersLabelField;
@property (strong, nonatomic) IBOutlet UILabel *borrowersMembersLabelField;
@property (strong, nonatomic) IBOutlet UILabel *membersFTELabelField;
@property (strong, nonatomic) IBOutlet UILabel *averageSharesMemberLabelField;
@property (strong, nonatomic) IBOutlet UILabel *averageLoanBalanceLabelField;
@property (strong, nonatomic) IBOutlet UILabel *salaryFTELabelField;

@end
