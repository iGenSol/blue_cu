//
//  CapitalAdequacyViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 1/13/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "CapitalAdequacyViewController.h"
#import "AppDelegate.h"
#import "FS220+CoreDataClass.h"
#import "FS220A+CoreDataClass.h"
#import "RatioTabBarController.h"

@interface CapitalAdequacyViewController ()

@end

@implementation CapitalAdequacyViewController

#define debug 1

NSString *capitalAdequacyDataCUNumber = @"";

NSString *capitalAdequacyfs220_010 = @"";
NSString *capitalAdequacyfs220_018 = @"";
NSString *capitalAdequacyfs220_041B = @"";
NSString *capitalAdequacyfs220_668 = @"";
NSString *capitalAdequacyfs220_719 = @"";
NSString *capitalAdequacyfs220_825 = @"";
NSString *capitalAdequacyfs220_860C = @"";

NSString *capitalAdequacyfs220a_820A =@"";
NSString *capitalAdequacyfs220a_997 =@"";
NSString *capitalAdequacyfs220a_925A =@"";


#pragma mark - VIEW
-(void)refreshInterface {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    // Blank out in case no record found
    
    self.netWorthTotalAssetsLabelField.text = @"0.00";
    self.tlDelinquentLoanNetWorthLabelField.text = @"0.00";
    self.solvencyEquationLabelField.text = @"0.00";
    self.classifiedAssetsNetWorthLabelField.text = @"0.00";
    
    // Get credit union number for fetch from RatioTabBarController
    RatioTabBarController *tabbar = (RatioTabBarController *)self.tabBarController;
    capitalAdequacyDataCUNumber = [tabbar.receivedCUNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    // Get FS220 Data
    NSFetchRequest *fs220FetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220"];
    NSSortDescriptor *fs220SortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220FetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220SortDescriptor]];
    NSPredicate *fs220Filter = [NSPredicate predicateWithFormat:@"cu_number == %@", capitalAdequacyDataCUNumber];
    [fs220FetchRequest setPredicate:fs220Filter];
    NSArray *fetchedFS220 = [cdh.context executeFetchRequest:fs220FetchRequest error:nil];
    
    for (FS220 *fs220 in fetchedFS220) {
        
        // Convert Total Assets to Currency String
        capitalAdequacyfs220_010 = fs220.acct_010;
        capitalAdequacyfs220_018 = fs220.acct_018;
        capitalAdequacyfs220_041B = fs220.acct_041B;
        capitalAdequacyfs220_668 = fs220.acct_668;
        capitalAdequacyfs220_719 = fs220.acct_719;
        capitalAdequacyfs220_825 = fs220.acct_825;
        capitalAdequacyfs220_860C = fs220.acct_860C;
    }
    
    // Get FS220a Data
    NSFetchRequest *fs220aFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220A"];
    NSSortDescriptor *fs220aSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220aFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220aSortDescriptor]];
    NSPredicate *fs220aFilter = [NSPredicate predicateWithFormat:@"cu_number == %@", capitalAdequacyDataCUNumber];
    [fs220aFetchRequest setPredicate:fs220aFilter];
    NSArray *fetchedFS220a = [cdh.context executeFetchRequest:fs220aFetchRequest error:nil];
    
    for (FS220A *fs220a in fetchedFS220a) {

        capitalAdequacyfs220a_820A = fs220a.acct_820A;
        capitalAdequacyfs220a_925A = fs220a.acct_925A;
        capitalAdequacyfs220a_997 = fs220a.acct_997;
    }
    
    [self calculateRatios];
}

-(void)viewDidLoad {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [self refreshInterface];
}

-(void)viewDidDisappear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
}

#pragma mark - STRING CONVERSION METHOD
-(NSNumber*)convertStringToNumberForTextFields:(NSString*)passedString {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    float convertedNumber = 0;
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    convertedNumber = [[numberFormatter numberFromString: passedString] floatValue];
    NSNumber *returnedNumber = [NSNumber numberWithFloat:convertedNumber];
    
    return returnedNumber;
}


#pragma mark - CALCULATE RATIOS
-(void)calculateRatios {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    // Setup variables
    NSNumber *returnedNumberForfs220_010 = [self convertStringToNumberForTextFields:capitalAdequacyfs220_010];
    NSNumber *returnedNumberForfs220_018 = [self convertStringToNumberForTextFields:capitalAdequacyfs220_018];
    NSNumber *returnedNumberForfs220_041B = [self convertStringToNumberForTextFields:capitalAdequacyfs220_041B];
    NSNumber *returnedNumberForfs220_668 = [self convertStringToNumberForTextFields:capitalAdequacyfs220_668];
    NSNumber *returnedNumberForfs220_719 = [self convertStringToNumberForTextFields:capitalAdequacyfs220_719];
    NSNumber *returnedNumberForfs220_825 = [self convertStringToNumberForTextFields:capitalAdequacyfs220_825];
    NSNumber *returnedNumberForfs220_860C = [self convertStringToNumberForTextFields:capitalAdequacyfs220_860C];

    NSNumber *returnedNumberForfs220a_820A = [self convertStringToNumberForTextFields:capitalAdequacyfs220a_820A];
    NSNumber *returnedNumberForfs220a_925A = [self convertStringToNumberForTextFields:capitalAdequacyfs220a_925A];
    NSNumber *returnedNumberForfs220a_997 = [self convertStringToNumberForTextFields:capitalAdequacyfs220a_997];

    float f010 = [returnedNumberForfs220_010 floatValue];
    float f018 = [returnedNumberForfs220_018 floatValue];
    float f041B = [returnedNumberForfs220_041B floatValue];
    float f668 = [returnedNumberForfs220_668 floatValue];
    float f719 = [returnedNumberForfs220_719 floatValue];
    float f825 = [returnedNumberForfs220_825 floatValue];
    float f860C = [returnedNumberForfs220_860C floatValue];
    
    float f820A = [returnedNumberForfs220a_820A floatValue];
    float f925A = [returnedNumberForfs220a_925A floatValue];
    float f997 = [returnedNumberForfs220a_997 floatValue];
    
    // Calculate Net Worth / Total Assets (display truncated)
    if (f010 != 0) {
        float netWorthRatio = (f997 / f010) * 100;
        NSString *formattedNetWorthRatio = [NSString stringWithFormat:@"%.03f", netWorthRatio];
        NSString *truncatedNetWorthRatio = [self truncateCapitalAdequacyRatios:formattedNetWorthRatio];
        self.netWorthTotalAssetsLabelField.text = truncatedNetWorthRatio;
    } else {
        self.netWorthTotalAssetsLabelField.text = @"0.00";
    }

    // Calculate TL Delinquent Loans / Net Worth (display rounded)
    if (f997 != 0) {
        float TLDelinqNetWorthRatio = (f041B / f997) * 100;
        NSString* formattedTLDelinqLoansRatio = [NSString stringWithFormat:@"%.02f", TLDelinqNetWorthRatio];
        self.tlDelinquentLoanNetWorthLabelField.text = formattedTLDelinqLoansRatio;
    } else {
        self.tlDelinquentLoanNetWorthLabelField.text = @"0.00";
    }
    
    // Calculate Solvency Equation (display rounded)
    if (f018 != 0) {
        float SolvencyEquationRatio = ((f010 - (f860C - f925A) - f825 - f668 - f820A) / f018) * 100;
        NSString* formattedSolvencyEquationRatio = [NSString stringWithFormat:@"%.02f", SolvencyEquationRatio];
        self.solvencyEquationLabelField.text = formattedSolvencyEquationRatio;
    } else {
        self.solvencyEquationLabelField.text = @"0.00";
    }
    
    // Calculate Classified Assets / Net Worth (display rounded)
    if (f997 != 0) {
        float ClassifiedAssetsRatio = ((f719 + f668) / f997) * 100;
        NSString* formattedClassifiedAssetsRatio = [NSString stringWithFormat:@"%.02f", ClassifiedAssetsRatio];
        self.classifiedAssetsNetWorthLabelField.text = formattedClassifiedAssetsRatio;
    } else {
        self.classifiedAssetsNetWorthLabelField.text = @"0.00";
    }
}

-(NSString*)truncateCapitalAdequacyRatios:(NSString*)passedCapitalAdequacyRatio {
    
    NSString *fullString = passedCapitalAdequacyRatio;
    NSString *prefix = nil;

    if ([fullString length] > 0) {
        
        NSRange searchResult = [fullString rangeOfString:@"."];
        if (searchResult.location == NSNotFound) {
            NSLog(@"Search string was not found");
            prefix = fullString;
        } else {
            for(NSInteger i = 0; i < searchResult.location+4; i++) // Truncate at decimal plus 2
            {
                prefix = [fullString substringToIndex:i];

            }
        }
    }

    return prefix;
}


@end
