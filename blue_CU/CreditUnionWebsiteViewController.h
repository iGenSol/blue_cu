//
//  CreditUnionWebsiteViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 12/21/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreditUnionWebsiteViewController : UIViewController <UITabBarControllerDelegate, UIWebViewDelegate, NSURLConnectionDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *cuWebView;

// Define variable passed for website address
@property (weak, nonatomic) NSString *receivedCUWebsiteAddress;


@end
