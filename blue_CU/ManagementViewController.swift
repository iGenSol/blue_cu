//
//  ManagementViewController.swift
//  blue_CU
//
//  Created by Timothy Milz on 7/25/19.
//  Copyright © 2019 iGenerateSolutions Inc. All rights reserved.
//
//  Converted to Swift 5 by Swiftify v5.0.7505 - https://objectivec2swift.com/
//
//  ManagementViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 12/8/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

// import Foundation

let debug = 1
var managementPhoneNumber = ""
var managementCUNumber = ""
var passedCUWebsiteAddress = ""

class ManagementViewController {
    // MARK: - VIEW
    func refreshInterface() {
        
        if debug == 1 {
            print("Running \(ManagementViewController) '\(NSStringFromSelector(#function))'")
        }
        
        // Blank out in case no record found
        preparedByLabelField.text = ""
        certifiedByLabelField.text = ""
        managerCEOLabelField.text = ""
        boardPresidentLabelField.text = ""
        cuWebsiteButton.setTitle("Not Supplied", for: .normal)
        
        // Load views with passed variables from FOICUViewController via ExtendedInfoTabBarController
        let tabbar = tabBarController as? ExtendedInfoTabBarContoller
        managementCUNumber = tabbar?.receivedCUNumber.replacingOccurrences(of: " ", with: "") ?? ""
        
        let cdh = (UIApplication.shared.delegate as? AppDelegate)?.cdh()
        
        let managementInfoFetchRequest = NSFetchRequest(entityName: "FS220D")
        let managementInfoSortDescriptor = NSSortDescriptor(key: "cu_number", ascending: true)
        managementInfoFetchRequest.sortDescriptors = [managementInfoSortDescriptor]
        let managementInfoFilter = NSPredicate(format: "cu_number == %@", managementCUNumber)
        managementInfoFetchRequest.predicate = managementInfoFilter
        var fetchedInfo: [Any]? = nil
        do {
            fetchedInfo = try cdh?.context.fetch(managementInfoFetchRequest)
        } catch {
        }
        
        for fs220d in fetchedInfo ?? [] {
            guard let fs220d = fs220d as? FS220D else {
                continue
            }
            
            preparedByLabelField.text = combineNameStrings(fs220d.preparer_first_name, second: fs220d.preparer_middle_initial, third: fs220d.preparer_last_name)
            certifiedByLabelField.text = combineNameStrings(fs220d.certified_first_name, second: fs220d.certified_middle_initial, third: fs220d.certified_last_name)
            managerCEOLabelField.text = combineNameStrings(fs220d.ceo_first_name, second: fs220d.ceo_middle_initial, third: fs220d.ceo_last_name)
            boardPresidentLabelField.text = combineNameStrings(fs220d.board_pres_first_name, second: fs220d.board_pres_middle_initial, third: fs220d.board_pres_last_name)
            
            let formattedManagementPhoneNumber = "\(fs220d.phone_number.substring(with: NSRange(location: 0, length: 3)))-\(fs220d.phone_number.substring(with: NSRange(location: 3, length: 3)))-\(fs220d.phone_number.substring(with: NSRange(location: 6, length: 4)))"
            callPhone.setTitle(formattedManagementPhoneNumber, for: .normal)
            managementPhoneNumber = formattedManagementPhoneNumber
            
            let fullWebString = fs220d.website_address
            var webPrefix: String? = nil
            
            if fullWebString.count >= 5 {
                webPrefix = (fullWebString as? NSString)?.substring(to: 5)
            } else {
                webPrefix = fullWebString
            }
            
            // setup webiste address defaults
            cuWebsiteButton.enabled = true
            secureBrowserLabel.hidden = true
            httpsWebSiteTextView.hidden = true
            
            if (webPrefix == "https") {
                passedCUWebsiteAddress = fs220d.website_address
                cuWebsiteButton.setTitle(fs220d.website_address, for: .disabled)
                cuWebsiteButton.enabled = false
                secureBrowserLabel.hidden = false
                httpsWebSiteTextView.hidden = false
                httpsWebSiteTextView.text = fs220d.website_address
            } else if !(fs220d.website_address == "") {
                cuWebsiteButton.setTitle(fs220d.website_address, for: .normal)
                cuWebsiteButton.enabled = true
                secureBrowserLabel.hidden = true
                passedCUWebsiteAddress = fs220d.website_address
                httpsWebSiteTextView.hidden = true
            } else if (fs220d.website_address == "") {
                cuWebsiteButton.setTitle("Not Supplied", for: .disabled)
                cuWebsiteButton.enabled = false
                secureBrowserLabel.hidden = true
                passedCUWebsiteAddress = "Not Supplied"
                httpsWebSiteTextView.hidden = true
            }
        }
    }
    
    func viewDidLoad() {
        
        if debug == 1 {
            print("Running \(ManagementViewController) '\(NSStringFromSelector(#function))'")
        }
        
        super.viewDidLoad()
        
        tabBarController.delegate = self
        tabBarController.title = "Management Info"
    }
    
    func viewWillAppear(_ animated: Bool) {
        
        if debug == 1 {
            print("Running \(ManagementViewController) '\(NSStringFromSelector(#function))'")
        }
        
        refreshInterface()
    }
    
    func viewDidDisappear(_ animated: Bool) {
        
        if debug == 1 {
            print("Running \(ManagementViewController) '\(NSStringFromSelector(#function))'")
        }
    }
    
    // MARK: - STRING CONCATENATION METHOD
    func combineNameStrings(_ fname: String?, second mname: String?, third lname: String?) -> String? {
        var fname = fname
        var mname = mname
        var lname = lname
        
        if debug == 1 {
            print("Running \(ManagementViewController) '\(NSStringFromSelector(#function))'")
        }
        
        fname = fname?.replacingOccurrences(of: " ", with: "")
        mname = mname?.replacingOccurrences(of: " ", with: "")
        lname = lname?.replacingOccurrences(of: " ", with: "")
        let combineNameStrings = [fname, mname, lname]
        let joinedFullNameString = combineNameStrings.joined(separator: " ")
        
        return joinedFullNameString
    }
    
    // MARK: - Call Phone Method
    @IBAction func callPhone(_ sender: Any) {
        let phoneNumberUrl = "telprompt://" + (managementPhoneNumber)
        if let url = URL(string: phoneNumberUrl) {
            UIApplication.shared.openURL(url)
        }
    }
    
    // MARK: - SEGUE
    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if debug == 1 {
            print("Running \(ManagementViewController) '\(NSStringFromSelector(#function))'")
        }
        
        if (segue.identifier == "Show Website Segue") {
            let cuVC = segue.destination as? CreditUnionWebsiteViewController
            cuVC?.receivedCUWebsiteAddress = passedCUWebsiteAddress
        } else {
            print("Unidentified Segue Attempted")
        }
    }
}
