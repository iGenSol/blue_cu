//
//  ExtendedInfoViewController.swift
//  blue_CU
//
//  Created by Timothy Milz on 7/24/19.
//  Copyright © 2019 iGenerateSolutions Inc. All rights reserved.
//
//  Converted to Swift 5 by Swiftify v5.0.7505 - https://objectivec2swift.com/
//
//  ExtendedInfoViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 11/6/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

// import Foundation


class ExtendedInfoViewController {
    private var foicuVC: FOICUViewController?
    
    let debug = 1
    
    // MARK: - KEYBOARD PROCESSING
    func hideKeyBoardWhenBackgroundIsTapped() {
        
        if debug == 1 {
            print("Running \(ExtendedInfoViewController) '\(NSStringFromSelector(#function))'")
        }
        
        let tgr = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tgr.cancelsTouchesInView = false
        view.addGestureRecognizer(tgr)
    }
    
    @objc func hideKeyboard() {
        
        if debug == 1 {
            print("Running \(ExtendedInfoViewController) '\(NSStringFromSelector(#function))'")
        }
        
        view.endEditing(true)
    }
    
    // MARK: - VIEW
    func refreshInterface() {
        
        if debug == 1 {
            print("Running \(ExtendedInfoViewController) '\(NSStringFromSelector(#function))'")
        }
        
        // Load views with passed variables from FOICUViewController via ExtendedInfoTabBarController
        let tabbar = tabBarController as? ExtendedInfoTabBarContoller
        
        // For ExtendedInfo tab:
        //if ([tabbar.receivedAttentionOf isEqualToString:@""]) {
        //    self.attentionOfLabelField.text = @"Not Supplied By Credit Union";
        //} else {
        //    self.attentionOfLabelField.text = tabbar.receivedAttentionOf;
        // }
        
        if (tabbar?.receivedCharterState == "") {
            charterStateLabelField.text = "N/A"
        } else {
            charterStateLabelField.text = tabbar?.receivedCharterState
        }
        
        if (tabbar?.receivedCongDist == "") {
            congDistLabelField.text = "N/A"
        } else {
            congDistLabelField.text = tabbar?.receivedCongDist
        }
        
        if (tabbar?.receivedCountyCode == "") {
            countyCodeLabelField.text = "N/A"
        } else {
            countyCodeLabelField.text = tabbar?.receivedCountyCode
        }
        
        if (tabbar?.receivedCUType == "") {
            cuTypeLabelField.text = "N/A"
        } else {
            cuTypeLabelField.text = tabbar?.receivedCUType
        }
        
        if (tabbar?.receivedDistrict == "") {
            ncuaDistrictLabelField.text = "N/A"
        } else {
            ncuaDistrictLabelField.text = tabbar?.receivedDistrict
        }
        
        if (tabbar?.receivedIssueDate == "") {
            charterInsuranceEffectiveDateLabelField.text = "N/A"
        } else {
            
            let fullString = tabbar?.receivedIssueDate
            var prefix: String? = nil
            
            if (fullString?.count ?? 0) >= 10 {
                
                let length = fullString?.count ?? 0
                let buffer = [unichar](repeating: 0, count: length + 1)
                (fullString as NSString?)?.getCharacters(&buffer, range: NSRange(location: 0, length: length))
                
                for i in 0..<length {
                    if buffer[i] == 32 {
                        //if string is a space, stop iteration
                        prefix = (fullString as? NSString)?.substring(to: i)
                        i = length + 1
                    }
                }
            } else {
                
                prefix = fullString
            }
            
            charterInsuranceEffectiveDateLabelField.text = prefix
        }
        
        if (tabbar?.receivedLimitedInc == "") {
            limitedIncomeCodeLabelField.text = "N/A"
        } else {
            limitedIncomeCodeLabelField.text = tabbar?.receivedLimitedInc
        }
        
        if (tabbar?.receivedPeerGroup == "") {
            peerGroupLabelField.text = "N/A"
        } else {
            peerGroupLabelField.text = tabbar?.receivedPeerGroup
        }
        
        if (tabbar?.receivedQuarterFlag == "") {
            quarterFlagLabelField.text = "N/A"
        } else {
            quarterFlagLabelField.text = tabbar?.receivedQuarterFlag
        }
        
        if (tabbar?.receivedRegion == "") {
            ncuaRegionLabelField.text = "N/A"
        } else {
            
            let myInt = tabbar?.receivedRegion.intValue ?? 0
            
            switch myInt {
            case 1:
                ncuaRegionLabelField.text = "Albany"
            case 2:
                ncuaRegionLabelField.text = "Capital"
            case 3:
                ncuaRegionLabelField.text = "Atlanta"
            case 4:
                ncuaRegionLabelField.text = "Austin"
            case 5:
                ncuaRegionLabelField.text = "Tempe"
            default:
                ncuaRegionLabelField.text = "N/A"
            }
        }
        
        if (tabbar?.receivedRSSD == "") {
            rssdLabelField.text = "N/A"
        } else {
            rssdLabelField.text = tabbar?.receivedRSSD
        }
        
        if (tabbar?.receivedSMSA == "") {
            smsaLabelField.text = "N/A"
        } else {
            smsaLabelField.text = tabbar?.receivedSMSA
        }
    }
    
    func viewDidLoad() {
        
        if debug == 1 {
            print("Running \(ExtendedInfoViewController) '\(NSStringFromSelector(#function))'")
        }
        
        super.viewDidLoad()
        tabBarController.delegate = self
        tabBarController.title = "Call Report Codes"
        
        // [self hideKeyBoardWhenBackgroundIsTapped];
    }
    
    func viewWillAppear(_ animated: Bool) {
        
        if debug == 1 {
            print("Running \(ExtendedInfoViewController) '\(NSStringFromSelector(#function))'")
        }
        /*
         // Register for keyboard notifications while thew view is visible
         [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidShow:) name:UIKeyboardDidShowNotification object:self.view.window];
         [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];
         */
        refreshInterface()
    }
    
    func viewDidDisappear(_ animated: Bool) {
        
        if debug == 1 {
            print("Running \(ExtendedInfoViewController) '\(NSStringFromSelector(#function))'")
        }
        /*
         // Unregister for keyboard notifications while the view is not visible
         [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
         [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil]; */
    }
    
    // MARK: - INTERACTION
    func keyBoardDidShow(_ n: Notification?) {
        
        if debug == 1 {
            print("Running \(ExtendedInfoViewController) '\(NSStringFromSelector(#function))'")
        }
        /*
         // Find TOP of keyboard input view
         CGRect keyBoardRect = [[[ n userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
         keyBoardRect = [self.view convertRect:keyBoardRect fromView:nil];
         CGFloat keyBoardTop = keyBoardRect.origin.y;
         
         // Resize Scroll View
         CGRect newScrollViewFrame = CGRectMake(0, 0, self.view.bounds.size.width, keyBoardTop);
         newScrollViewFrame.size.height = keyBoardTop - self.view.bounds.origin.y;
         [self.scrollView setFrame:newScrollViewFrame];
         
         //Scroll to the active Text Field
         
         [self.scrollView scrollRectToVisible:self.activeField.frame animated:YES];
         */
    }
    
    func keyBoardWillHide(_ n: Notification?) {
        
        if debug == 1 {
            print("Running \(ExtendedInfoViewController) '\(NSStringFromSelector(#function))'")
        }
        /*
         CGRect defaultFrame = CGRectMake(self.scrollView.frame.origin.x, self.scrollView.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
         
         // Reset Scrollview to the same size as the containing view
         [self.scrollView setFrame:defaultFrame];
         
         // Scoll to the top again
         [self.scrollView scrollRectToVisible:self.recipeStartingGrainsTextField.frame animated:YES];
         */
    }
}
