//
//  FOICUViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 10/30/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataHelper.h"
#import "ExtendedInfoViewController.h"

@interface FOICUViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate, NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *selectedExtendedInfo;

@property (strong, nonatomic) IBOutlet UILabel *cu_nameLabelField;
@property (strong, nonatomic) IBOutlet UILabel *cityLabelField;
@property (strong, nonatomic) IBOutlet UILabel *streetLabelField;
@property (strong, nonatomic) IBOutlet UILabel *state_codeLabelField;
@property (strong, nonatomic) IBOutlet UILabel *zip_codeLabelField;
@property (strong, nonatomic) IBOutlet UILabel *year_openedLabelField;
@property (strong, nonatomic) IBOutlet UILabel *totalAssetsLabelField;
@property (strong, nonatomic) IBOutlet UILabel *currentMembersLabelField;
@property (strong, nonatomic) IBOutlet UILabel *potentialMembersLabelField;
@property (strong, nonatomic) IBOutlet UILabel *cycle_dateLabelField;

@property (strong, nonatomic) NSManagedObjectID *selectedCUID;


@end
