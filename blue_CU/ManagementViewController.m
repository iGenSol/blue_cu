//
//  ManagementViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 12/8/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import "AppDelegate.h"
#import "ManagementViewController.h"
#import "ExtendedInfoTabBarContoller.h"
#import "CreditUnionWebsiteViewController.h"
#import "FS220D+CoreDataProperties.h"

@interface ManagementViewController ()

@end

@implementation ManagementViewController

#define debug 1

NSString *managementPhoneNumber = @"";
NSString *managementCUNumber = @"";
NSString *passedCUWebsiteAddress = @"";

#pragma mark - VIEW
-(void)refreshInterface {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    // Blank out in case no record found
    self.preparedByLabelField.text = @"";
    self.certifiedByLabelField.text = @"";
    self.managerCEOLabelField.text = @"";
    self.boardPresidentLabelField.text = @"";
    [self.cuWebsiteButton setTitle:@"Not Supplied" forState:UIControlStateNormal];
    
    // Load views with passed variables from FOICUViewController via ExtendedInfoTabBarController
    ExtendedInfoTabBarContoller *tabbar = (ExtendedInfoTabBarContoller *)self.tabBarController;
    managementCUNumber = [tabbar.receivedCUNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *managementInfoFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220D"];
    NSSortDescriptor *managementInfoSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [managementInfoFetchRequest setSortDescriptors:[NSArray arrayWithObject:managementInfoSortDescriptor]];
    NSPredicate *managementInfoFilter = [NSPredicate predicateWithFormat:@"cu_number == %@", managementCUNumber];
    [managementInfoFetchRequest setPredicate:managementInfoFilter];
    NSArray *fetchedInfo = [cdh.context executeFetchRequest:managementInfoFetchRequest error:nil];

    for (FS220D *fs220d in fetchedInfo) {

        self.preparedByLabelField.text = [self combineNameStrings:fs220d.preparer_first_name second:fs220d.preparer_middle_initial third:fs220d.preparer_last_name];
        self.certifiedByLabelField.text = [self combineNameStrings:fs220d.certified_first_name second:fs220d.certified_middle_initial third:fs220d.certified_last_name];
        self.managerCEOLabelField.text = [self combineNameStrings:fs220d.ceo_first_name second:fs220d.ceo_middle_initial third:fs220d.ceo_last_name];
        self.boardPresidentLabelField.text = [self combineNameStrings:fs220d.board_pres_first_name second:fs220d.board_pres_middle_initial third:fs220d.board_pres_last_name];
        
        NSString *formattedManagementPhoneNumber = [NSString stringWithFormat: @"%@-%@-%@", [fs220d.phone_number substringWithRange:NSMakeRange(0,3)],[fs220d.phone_number substringWithRange:NSMakeRange(3,3)],
                                          [fs220d.phone_number substringWithRange:NSMakeRange(6,4)]];
        [self.callPhone setTitle:formattedManagementPhoneNumber forState:UIControlStateNormal];
        managementPhoneNumber = formattedManagementPhoneNumber;
        
        NSString *fullWebString = fs220d.website_address;
        NSString *webPrefix = nil;
        
        if ([fullWebString length] >= 5) {
            webPrefix = [fullWebString substringToIndex:5];
        } else {
            webPrefix = fullWebString;
        }
    
        // setup webiste address defaults
        self.cuWebsiteButton.enabled = YES;
        self.secureBrowserLabel.hidden = YES;
        self.httpsWebSiteTextView.hidden = YES;
        
        if ([webPrefix isEqualToString:@"https"]) {
            passedCUWebsiteAddress = fs220d.website_address;
            [self.cuWebsiteButton setTitle:fs220d.website_address forState:UIControlStateDisabled];
            self.cuWebsiteButton.enabled = NO;
            self.secureBrowserLabel.hidden = NO;
            self.httpsWebSiteTextView.hidden = NO;
            self.httpsWebSiteTextView.text = fs220d.website_address;
        
        } else if (![fs220d.website_address isEqualToString:@""]) {
            [self.cuWebsiteButton setTitle:fs220d.website_address forState:UIControlStateNormal];
            self.cuWebsiteButton.enabled = YES;
            self.secureBrowserLabel.hidden = YES;
            passedCUWebsiteAddress = fs220d.website_address;
            self.httpsWebSiteTextView.hidden = YES;
        }
        else if ([fs220d.website_address isEqualToString:@""]) {
            [self.cuWebsiteButton setTitle:@"Not Supplied" forState:UIControlStateDisabled];
            self.cuWebsiteButton.enabled = NO;
            self.secureBrowserLabel.hidden = YES;
            passedCUWebsiteAddress = @"Not Supplied";
            self.httpsWebSiteTextView.hidden = YES;
        }
    }
}

-(void)viewDidLoad {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [super viewDidLoad];
    
    self.tabBarController.delegate = self;
    self.tabBarController.title = @"Management Info";
}

-(void)viewWillAppear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [self refreshInterface];
}

-(void)viewDidDisappear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
}

#pragma mark - STRING CONCATENATION METHOD
- (NSString*)combineNameStrings:(NSString *)fname second:(NSString *)mname third:(NSString *)lname {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    fname = [fname stringByReplacingOccurrencesOfString:@" " withString:@""];
    mname = [mname stringByReplacingOccurrencesOfString:@" " withString:@""];
    lname = [lname stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSArray *combineNameStrings = [[NSArray alloc] initWithObjects:fname, mname, lname, nil];
    NSString *joinedFullNameString = [combineNameStrings componentsJoinedByString:@" "];

    return joinedFullNameString;
}

#pragma mark - Call Phone Method
-(IBAction)callPhone:(id)sender {
    NSString *phoneNumberUrl = [@"telprompt://" stringByAppendingString:managementPhoneNumber];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumberUrl]];
}

#pragma mark - SEGUE
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    if ([segue.identifier isEqualToString:@"Show Website Segue"]) {
        CreditUnionWebsiteViewController *cuVC = segue.destinationViewController;
        cuVC.receivedCUWebsiteAddress = passedCUWebsiteAddress;
    } else {
        NSLog(@"Unidentified Segue Attempted");
    }
}

@end
