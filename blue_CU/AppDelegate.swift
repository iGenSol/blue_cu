//
//  AppDelegate.swift
//  blue_CU
//
//  Created by Timothy Milz on 7/25/19.
//  Copyright © 2019 iGenerateSolutions Inc. All rights reserved.
//
//  Converted to Swift 5 by Swiftify v5.0.7505 - https://objectivec2swift.com/
//
//  AppDelegate.m
//  blue_CU
//
//  Created by Timothy Milz on 10/15/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

import Foundation

class AppDelegate {
    let debug = 0
    func demo() {
        if debug == 1 {
            print("Running \(AppDelegate) '\(NSStringFromSelector(#function))'")
        }
        
        //[_coreDataHelper createBlueCUFirstData];
        cdh()?.backgroundSaveContext()
    }
    
    func cdh() -> CoreDataHelper? {
        if debug == 1 {
            print("Running \(AppDelegate) '\(NSStringFromSelector(#function))'")
        }
        if !coreDataHelper {
            // TODO: [Swiftify] ensure that the code below is executed only once (`dispatch_once()` is deprecated)
            {
                coreDataHelper = CoreDataHelper()
            }
            coreDataHelper.setupCoreData()
        }
        return coreDataHelper
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        // Override point for customization after application launch.
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // [[self cdh] backgroundSaveContext];
        userPressedHome = "YES"
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        if debug == 1 {
            print("Running \(AppDelegate) '\(NSStringFromSelector(#function))'")
        }
        
        if !(userPressedHome == "YES") {
            // Load first data only if user has not pressed home and made app enter background
            cdh()
            demo()
        }
        
        /*
         // Clean up any non-listed records when app starts
         NSFetchRequest *request = [[[_coreDataHelper model] fetchRequestTemplateForName:@"selectCaliberNotListed"] copy];
         
         NSArray *fetchedObjects = [_coreDataHelper.context executeFetchRequest:request error:nil];
         
         for (Calibers *caliber in fetchedObjects) {
         
         if (debug == 1) {
         NSLog(@"Deleting Object '%@'", caliber.caliber_name);
         }
         
         [_coreDataHelper.context deleteObject:caliber];
         }
         
         NSFetchRequest *powderRequest = [[[_coreDataHelper model] fetchRequestTemplateForName:@"selectPowderNotListed"] copy];
         
         NSArray *fetchedPowderObjects = [_coreDataHelper.context executeFetchRequest:powderRequest error:nil];
         
         for (Powders *powder in fetchedPowderObjects) {
         
         if (debug == 1) {
         NSLog(@"Deleting Object '%@'", powder.powder_name);
         }
         
         [_coreDataHelper.context deleteObject:powder];
         }
         
         NSFetchRequest *primerRequest = [[[_coreDataHelper model] fetchRequestTemplateForName:@"selectPrimerNotListed"] copy];
         
         NSArray *fetchedPrimerObjects = [_coreDataHelper.context executeFetchRequest:primerRequest error:nil];
         
         for (Primers *primer in fetchedPrimerObjects) {
         
         if (debug == 1) {
         NSLog(@"Deleting Object '%@'", primer.primer_name);
         }
         
         [_coreDataHelper.context deleteObject:primer];
         }
         
         NSFetchRequest *caseRequest = [[[_coreDataHelper model] fetchRequestTemplateForName:@"selectCaseNotListed"] copy];
         
         NSArray *fetchedCaseObjects = [_coreDataHelper.context executeFetchRequest:caseRequest error:nil];
         
         for (Cases *cases in fetchedCaseObjects) {
         
         if (debug == 1) {
         NSLog(@"Deleting Object '%@'", cases.case_name);
         }
         
         [_coreDataHelper.context deleteObject:cases];
         }
         
         NSFetchRequest *bulletRequest = [[[_coreDataHelper model] fetchRequestTemplateForName:@"selectBulletNotListed"] copy];
         
         NSArray *fetchedBulletObjects = [_coreDataHelper.context executeFetchRequest:bulletRequest error:nil];
         
         for (Bullets *bullets in fetchedBulletObjects) {
         
         if (debug == 1) {
         NSLog(@"Deleting Object '%@'", bullets.bullet_name);
         }
         
         [_coreDataHelper.context deleteObject:bullets];
         }
         
         NSFetchRequest *recipeRequest = [[[_coreDataHelper model] fetchRequestTemplateForName:@"selectRecipeNotListed"] copy];
         
         NSArray *fetchedRecipeObjects = [_coreDataHelper.context executeFetchRequest:recipeRequest error:nil];
         
         for (Recipes *recipes in fetchedRecipeObjects) {
         
         if (debug == 1) {
         NSLog(@"Deleting Object '%@'", recipes.recipe_name);
         }
         
         [_coreDataHelper.context deleteObject:recipes];
         }
         
         NSFetchRequest *recipeBatchRequest = [[[_coreDataHelper model] fetchRequestTemplateForName:@"selectRecipeBatchNotListed"] copy];
         
         NSArray *fetchedRecipeBatchObjects = [_coreDataHelper.context executeFetchRequest:recipeBatchRequest error:nil];
         
         for (RecipesBatch *recipesBatch in fetchedRecipeBatchObjects) {
         
         if (debug == 1) {
         NSLog(@"Deleting Object '%@'", recipesBatch.batch_name);
         }
         
         [_coreDataHelper.context deleteObject:recipesBatch];
         }
         
         */
        
        if !(userPressedHome == "YES") {
            // save only if not returning from Home press
            cdh()?.backgroundSaveContext()
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        cdh()?.backgroundSaveContext()
    }
}
