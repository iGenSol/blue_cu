//
//  ExtendedInfoTabBarContollerViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 11/12/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved..
//

#import "ExtendedInfoTabBarContoller.h"
#import "ExtendedInfoViewController.h"

@interface ExtendedInfoTabBarContoller ()

@end

@implementation ExtendedInfoTabBarContoller

#define debug 0

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationItem setTitle:@"ATMs"];
    //[self.navigationItem setHidesBackButton:YES];
}


-(void)tabBar:(UITabBar *)theTabBar didSelectItem:(UIViewController *)viewController
{
    NSLog(@"Tab index = %@ ", theTabBar.selectedItem);
    for(int i = 0; i < theTabBar.items.count; i++)
    {
        if(theTabBar.selectedItem == theTabBar.items[i])
        {
            switch (i) {
                case 0:
                    [self.navigationItem setTitle:@"ATMs"];
                    break;
                case 1:
                    [self.navigationItem setTitle:@"Branches"];
                    break;
                case 2:
                    [self.navigationItem setTitle:@"Financial Data"];
                    break;
                case 3:
                    [self.navigationItem setTitle:@"Management Info"];
                    break;
                    
                default:
                    [self.navigationItem setTitle:@"Extended Info"];
                    break;
            }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
