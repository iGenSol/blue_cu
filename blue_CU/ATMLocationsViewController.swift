//
//  ATMLocationsViewController.swift
//  blue_CU
//
//  Created by Timothy Milz on 7/24/19.
//  Copyright © 2019 iGenerateSolutions Inc. All rights reserved.
//
//  Converted to Swift 5 by Swiftify v5.0.7505 - https://objectivec2swift.com/
//
//  ATMLocationsViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 11/10/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

//import Foundation

let debug = 1

// Variables for Map View
var atm_cu_name = ""
var atm_atm_name = ""
var atm_phys_adr_1 = ""
var atm_phys_adr_city = ""
var atm_phys_adr_state = ""
var atm_phys_adr_zip = ""

class ATMLocationsViewController {
    func hideKeyBoardWhenBackgroundIsTapped() {
        
        if debug == 1 {
            print("Running \(ATMLocationsViewController) '\(NSStringFromSelector(#function))'")
        }
        
        let tgr = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tgr.cancelsTouchesInView = false
        view.addGestureRecognizer(tgr)
    }
    
    @objc func hideKeyboard() {
        
        if debug == 1 {
            print("Running \(ATMLocationsViewController) '\(NSStringFromSelector(#function))'")
        }
        
        view.endEditing(true)
    }
    
    // MARK: - VIEW
    func refreshInterface() {
        
        if debug == 1 {
            print("Running \(ATMLocationsViewController) '\(NSStringFromSelector(#function))'")
        }
        
        if selectedATMID {
            let cdh = (UIApplication.shared.delegate as? AppDelegate)?.cdh()
            var atmLocations: ATMLocations? = nil
            do {
                atmLocations = try cdh?.context.existingObject(with: selectedATMID) as? ATMLocations
            } catch {
            }
            
            site_NameLabelField.text = atmLocations?.site_name
            site_type_nameLabelField.text = atmLocations?.site_type_name
            phys_adr_1LabelField.text = atmLocations?.phys_adr_1
            phy_adr_2LabelField.text = atmLocations?.phys_adr_2
            phys_adr_cityLabelField.text = atmLocations?.phys_adr_city
            phys_adr_countyLabelField.text = atmLocations?.phys_adr_county_name
            phys_adr_state_codeLabelField.text = atmLocations?.phys_adr_state_code
            phys_adr_postal_codeLabelField.text = atmLocations?.phys_adr_postal_code
            
            // setup map view variables
            atm_cu_name = atmLocations?.cu_name ?? ""
            atm_atm_name = atmLocations?.site_name ?? ""
            atm_phys_adr_1 = atmLocations?.phys_adr_1 ?? ""
            atm_phys_adr_city = atmLocations?.phys_adr_city ?? ""
            atm_phys_adr_state = atmLocations?.phys_adr_state_code ?? ""
            atm_phys_adr_zip = atmLocations?.phys_adr_postal_code ?? ""
        }
    }
    
    func viewDidLoad() {
        
        if debug == 1 {
            print("Running \(ATMLocationsViewController) '\(NSStringFromSelector(#function))'")
        }
        
        super.viewDidLoad()
        hideKeyBoardWhenBackgroundIsTapped()
    }
    
    func viewWillAppear(_ animated: Bool) {
        
        if debug == 1 {
            print("Running \(ATMLocationsViewController) '\(NSStringFromSelector(#function))'")
        }
        
        refreshInterface()
    }
    
    func viewDidDisappear(_ animated: Bool) {
        
        if debug == 1 {
            print("Running \(ATMLocationsViewController) '\(NSStringFromSelector(#function))'")
        }
    }
    
    // MARK: - SEGUE
    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if debug == 1 {
            print("Running \(ATMLocationsViewController) '\(NSStringFromSelector(#function))'")
        }
        
        let atmMapLocationsVC = segue.destination as? ATMMapLocationViewController
        if (segue.identifier == "Show ATM Map Segue") {
            atmMapLocationsVC?.passed_cu_name = atm_cu_name
            atmMapLocationsVC?.passed_atm_name = atm_atm_name
            atmMapLocationsVC?.passed_phys_adr_1 = atm_phys_adr_1
            atmMapLocationsVC?.passed_phys_adr_city = atm_phys_adr_city
            atmMapLocationsVC?.passed_phys_adr_state = atm_phys_adr_state
            atmMapLocationsVC?.passed_phys_adr_zip = atm_phys_adr_zip
        } else {
            print("Unidentified Segue Attempted")
        }
    }
}
