//
//  RatioTabBarController.h
//  blue_CU
//
//  Created by Timothy Milz on 3/13/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RatioTabBarController : UITabBarController <UITabBarControllerDelegate>

@property (weak, nonatomic) NSString *receivedCUNumber;

@end
