//
//  ManagementViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 12/8/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManagementViewController : UIViewController <UITabBarControllerDelegate>

@property (strong, nonatomic) IBOutlet UILabel *preparedByLabelField;
@property (strong, nonatomic) IBOutlet UILabel *certifiedByLabelField;
@property (strong, nonatomic) IBOutlet UILabel *managerCEOLabelField;
@property (strong, nonatomic) IBOutlet UILabel *boardPresidentLabelField;
@property (strong, nonatomic) IBOutlet UIButton *callPhone;
@property (strong, nonatomic) IBOutlet UIButton *cuWebsiteButton;
@property (strong, nonatomic) IBOutlet UILabel *secureBrowserLabel;
@property (strong, nonatomic) IBOutlet UITextView *httpsWebSiteTextView;

@end
