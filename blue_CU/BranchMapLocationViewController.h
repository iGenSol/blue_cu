//
//  BranchMapLocationViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 11/29/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@import CoreLocation;
@import MapKit;


@interface BranchMapLocationViewController : UIViewController <CLLocationManagerDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *branchMap;
@property (strong, nonatomic) CLLocationManager *branchlocationManager;

@property (strong, nonatomic) NSString *passed_cu_name;
@property (strong, nonatomic) NSString *passed_site_name;
@property (strong, nonatomic) NSString *passed_phys_adr_1;
@property (strong, nonatomic) NSString *passed_phys_adr_city;
@property (strong, nonatomic) NSString *passed_phys_adr_state;
@property (strong, nonatomic) NSString *passed_phys_adr_zip;
@property (strong, nonatomic) NSString *passed_phys_adr_country;

@end
