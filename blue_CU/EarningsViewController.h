//
//  EarningsViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 1/14/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EarningsViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *ROALabelField;
@property (strong, nonatomic) IBOutlet UILabel *yieldOnAvgLoansLabelField;
@property (strong, nonatomic) IBOutlet UILabel *feeIncAvgAssetsLabelField;
@property (strong, nonatomic) IBOutlet UILabel *netMarginAvgAssetsLabelField;
@property (strong, nonatomic) IBOutlet UILabel *loanLossAvgAssetsLabelField;
@property (strong, nonatomic) IBOutlet UILabel *opExGrossIncomeLabelField;
@property (strong, nonatomic) IBOutlet UILabel *netOpExpAvgAssetsLabelField;
@property (strong, nonatomic) IBOutlet UILabel *incomeAssetsLabelField;
@property (strong, nonatomic) IBOutlet UILabel *yieldOnAvgInvestmentsLabelField;
@property (strong, nonatomic) IBOutlet UILabel *costOfFundsAvgAssetsLabelField;
@property (strong, nonatomic) IBOutlet UILabel *opExpAvgAssetsLabelField;
@property (strong, nonatomic) IBOutlet UILabel *netInterestMarginAvgAssetsLabelField;
@property (strong, nonatomic) IBOutlet UILabel *assetsTLAssetsLabelField;

@end
