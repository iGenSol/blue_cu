//
//  GrowthViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 1/16/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GrowthViewController : UIViewController
@property (weak, nonatomic) NSString *receivedCUNumber;
@property (strong, nonatomic) IBOutlet UILabel *netWorthGrowthLabelField;
@property (strong, nonatomic) IBOutlet UILabel *marketShareGrowthLabelField;
@property (strong, nonatomic) IBOutlet UILabel *loanGrowthLabelField;
@property (strong, nonatomic) IBOutlet UILabel *investmentGrowthLabelField;
@property (strong, nonatomic) IBOutlet UILabel *membershipGrowthLabelField;
@property (strong, nonatomic) IBOutlet UILabel *assetGrowthLabelField;

@end
