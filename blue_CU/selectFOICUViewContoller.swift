//
//  FOICUViewController.swift
//  blue_CU
//
//  Created by Timothy Milz on 7/24/19.
//  Copyright © 2019 iGenerateSolutions Inc. All rights reserved.
//
//  Converted to Swift 5 by Swiftify v5.0.7505 - https://objectivec2swift.com/
//
//  selectFOICUViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 10/30/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

//import Foundation

let debug = 1
var totalCU = 5696
var userSelectedState = false

class selectFOICUViewController {
    private var pickerData: [Any] = [] // for state picker
    
    @IBAction func selectedShowMeButton(_ sender: Any) {
        
        if debug == 1 {
            print("Running \(selectFOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        if (nameSearchTextField.text == "") && (!userSelectedState) {
            showNameSearchAlert()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if debug == 1 {
            print("Running \(selectFOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        textField.resignFirstResponder() // Dismiss the keyboard.
        // Execute any additional code
        
        if (nameSearchTextField.text == "") && (!userSelectedState) {
            showNameSearchAlert()
        } else {
            performSegue(withIdentifier: "Show Me The CU Segue", sender: self)
        }
        
        return true
    }
    
    func hideKeyBoardWhenBackgroundIsTapped() {
        
        if debug == 1 {
            print("Running \(selectFOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        let tgr = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tgr.cancelsTouchesInView = false
        view.addGestureRecognizer(tgr)
    }
    
    @objc func hideKeyboard() {
        
        if debug == 1 {
            print("Running \(selectFOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        view.endEditing(true)
    }
    
    // MARK: - VIEW
    func refreshInterface() {
        
        if debug == 1 {
            print("Running \(selectFOICUViewController) '\(NSStringFromSelector(#function))'")
        }
    }
    
    func viewDidLoad() {
        
        if debug == 1 {
            print("Running \(selectFOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        super.viewDidLoad()
        hideKeyBoardWhenBackgroundIsTapped()
        
        // Delegate = self
        nameSearchTextField.delegate = self
        
        // Initialize Data
        pickerData = [
            "AL",
            "AK",
            "AZ",
            "AR",
            "CA",
            "CO",
            "CT",
            "DE",
            "FL",
            "GA",
            "HI",
            "ID",
            "IL",
            "IN",
            "IA",
            "KS",
            "KY",
            "LA",
            "ME",
            "MD",
            "MA",
            "MI",
            "MN",
            "MS",
            "MO",
            "MT",
            "NE",
            "NV",
            "NH",
            "NJ",
            "NM",
            "NY",
            "NC",
            "ND",
            "OH",
            "OK",
            "OR",
            "PA",
            "RI",
            "SC",
            "SD",
            "TN",
            "TX",
            "UT",
            "VT",
            "VA",
            "WA",
            "WV",
            "WI",
            "WY"
        ]
        
        // Connect data for state picker
        statePickerTextField.delegate = self
        statePickerTextField.dataSource = self
    }
    
    func viewWillAppear(_ animated: Bool) {
        
        if debug == 1 {
            print("Running \(selectFOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        // Register for keyboard notifications while thew view is visible
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardDidShow(_:)), name: UIResponder.keyboardDidShowNotification, object: view.window)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: view.window)
        
        //[self.nameSearchTextField becomeFirstResponder];
        
        numberOfFOICU.text = String(format: "%li", totalCU)
        passedCUStateSearch = "" // initiatlize for next select
        userSelectedState = false
        
        statePickerTextField.reloadAllComponents()
        statePickerTextField.selectRow(0, inComponent: 0, animated: true)
    }
    
    func viewDidDisappear(_ animated: Bool) {
        
        if debug == 1 {
            print("Running \(selectFOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        // Unregister for keyboard notifications while the view is not visible
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    // MARK: - PICKERS
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        if debug == 1 {
            print("Running \(selectFOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if debug == 1 {
            print("Running \(selectFOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if debug == 1 {
            print("Running \(selectFOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        return pickerData[row] as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if debug == 1 {
            print("Running \(selectFOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        passedCUStateSearch = pickerData[row]
        userSelectedState = true
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        if debug == 1 {
            print("Running \(selectFOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        let attString = NSAttributedString(string: (pickerData[row]) as? String ?? "", attributes: [
            NSAttributedString.Key.foregroundColor: UIColor.white
            ])
        
        return attString
    }
    
    // MARK: - INTERACTION
    @objc func keyBoardDidShow(_ n: Notification?) {
        
        if debug == 1 {
            print("Running \(selectFOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        /*
         // Find TOP of keyboard input view
         CGRect keyBoardRect = [[[ n userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
         keyBoardRect = [self.view convertRect:keyBoardRect fromView:nil];
         CGFloat keyBoardTop = keyBoardRect.origin.y;
         
         // Resize Scroll View
         CGRect newScrollViewFrame = CGRectMake(0, 0, self.view.bounds.size.width, keyBoardTop);
         newScrollViewFrame.size.height = keyBoardTop - self.view.bounds.origin.y;
         [self.scrollView setFrame:newScrollViewFrame];
         
         //Scroll to the active Text View - Description Field
         [self.scrollView scrollRectToVisible:self.activeTextView.frame animated:YES];
         */
    }
    
    @objc func keyBoardWillHide(_ n: Notification?) {
        
        if debug == 1 {
            print("Running \(selectFOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        /*
         CGRect defaultFrame = CGRectMake(self.scrollView.frame.origin.x, self.scrollView.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
         
         // Reset Scrollview to the same size as the containing view
         [self.scrollView setFrame:defaultFrame];
         
         // Scoll to the top again
         [self.scrollView scrollRectToVisible:self.descriptionTextField.frame animated:YES];
         
         */
    }
    
    // MARK: - ALERT PROCESSING
    func showNameSearchAlert() {
        
        if debug == 1 {
            print("Running \(selectFOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        let nameSearchAlert = UIAlertController(title: "Name Search Alert", message: "You must enter search criteria in the name search box, or you must select a state \n", preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: { action in
            self.nameSearchTextField.becomeFirstResponder()
            self.viewDidLoad()
        })
        
        nameSearchAlert.addAction(defaultAction)
        present(nameSearchAlert, animated: true)
    }
    
    // MARK: - SEGUE
    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if debug == 1 {
            print("Running \(selectFOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        if (segue.identifier == "Show Me The CU Segue") {
            let foicuTVC = segue.destination as? FOICUTableViewController
            foicuTVC?.passedCUNameSearch = nameSearchTextField.text
            if userSelectedState {
                foicuTVC?.passedCUStateSearch = passedCUStateSearch
            } else {
                
                foicuTVC?.passedCUStateSearch = ""
            }
        } else {
            
            print("Unidentified Segue Attempted")
        }
    }
}
