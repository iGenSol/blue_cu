//
//  BranchTableViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 11/28/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import "BranchTableViewController.h"
#import "BranchLocationsViewController.h"
#import "BranchLocations+CoreDataClass.h"
#import "ExtendedInfoTabBarContoller.h"
#import "AppDelegate.h"

@interface BranchTableViewController ()

@end

@implementation BranchTableViewController

#define debug 1

NSString *branchCUNumber = @"";

#pragma mark - DATA
-(void)configureFetch{
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"BranchLocations"];
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"site_name" ascending:YES];
    [request setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"cu_number ==%@", branchCUNumber];
    
    [request setPredicate:filter];
    
    [request setFetchBatchSize:50];
    
    self.frc = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:cdh.context sectionNameKeyPath:@"site_name" cacheName:nil];
    self.frc.delegate = self;
}


#pragma mark - VIEW
-(void)viewDidLoad {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    
    ExtendedInfoTabBarContoller *tabbar = (ExtendedInfoTabBarContoller *)self.tabBarController;
    branchCUNumber = [tabbar.receivedCUNumber stringByReplacingOccurrencesOfString:@" " withString:@""];

    [self.branchActivityIndicator startAnimating];
    self.branchActivityIndicator.hidden = NO;
    
    [self configureFetch];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // load on background thread
        [self performFetch];
        
        dispatch_async(dispatch_get_main_queue(), ^{ // return to main thread
            
            if ([self.foundTableDataYN isEqualToString:@"N"]) {
                [self.branchActivityIndicator stopAnimating];
                self.branchActivityIndicator.hidden = YES;
                [self showNoBranchesFoundAlert];
            }
        });
    });
}

-(void)viewWillAppear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    static NSString *cellIdentifier = @"Branch Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryDetailButton;
    
    BranchLocations *branchLocations = [self.frc objectAtIndexPath:indexPath];
    
    NSMutableString *title = [NSMutableString stringWithFormat:@"%@ \n   %@ %@", branchLocations.site_name, branchLocations.phys_adr_city, branchLocations.phys_adr_state_code];
    [title replaceOccurrencesOfString:@"(null)" withString:@"" options:0 range:NSMakeRange(0, [title length])];
    
    cell.textLabel.numberOfLines = 0; // removes number of lines limitation, allows city, state on second line
    cell.textLabel.text = title;

    // make selected items white
    [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica Neue" size:14]];
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    
    // Now that we have data, turn off the activity indicator
    [self.branchActivityIndicator stopAnimating];
    self.branchActivityIndicator.hidden = YES;
    
    return cell;
}

-(NSArray*)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    return nil; // We don't want a section index
}

// Override titleForHeaderInSection from CoreDataTVC as we don't want section titles
- (NSString *)tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    return @""; // No Title
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleNone;
}

#pragma mark - SEGUE
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    BranchLocationsViewController *branchLocationsVC = segue.destinationViewController;
    if ([segue.identifier isEqualToString:@"Show Branch Location Detail Object Segue"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        branchLocationsVC.selectedBranchID = [[self.frc objectAtIndexPath:indexPath] objectID];
    }
    else {
        NSLog(@"Unidentified Segue Attempted");
    }
}

-(void) tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    BranchLocationsViewController *branchLocationsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BranchLocationsViewController"];
    branchLocationsVC.selectedBranchID = [[self.frc objectAtIndexPath:indexPath] objectID];
    [self.navigationController pushViewController:branchLocationsVC animated:YES];
}


#pragma mark - ALERT PROCESSING

- (void)showNoBranchesFoundAlert {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    UIAlertController *noBranchesFoundAlert = [UIAlertController alertControllerWithTitle:@"No Branches Found"
                                                                              message:[NSString stringWithFormat:@"No Branch data supplied for this credit union  \n"]
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [noBranchesFoundAlert addAction:defaultAction];
    [self presentViewController:noBranchesFoundAlert animated:YES completion:nil];
}



@end
