//
//  IndustryLoanInfoViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 3/21/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "AppDelegate.h"
#import "IndustryLoanInfoViewController.h"
#import "GeneralLoanStats+CoreDataClass.h"

@interface IndustryLoanInfoViewController ()

@end

@implementation IndustryLoanInfoViewController

#define debug 1


#pragma mark - VIEW
-(void)refreshInterface {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }

    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *generalStatsFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"GeneralLoanStats"];
    NSArray *fetchedGeneralStats = [cdh.context executeFetchRequest:generalStatsFetchRequest error:nil];
    
    for (GeneralLoanStats *generalStats in fetchedGeneralStats) {
        
        int zeroDecimals = 0;
        int oneDecimals = 1;
        
        NSNumber *zeroDecimalNumber = @(zeroDecimals);
        NSNumber *oneDecimalNumber = @(oneDecimals);
        
        NSNumber *totalLoansNSNumber = @(generalStats.totalLoans);
        NSString *returnedCurrencyStringForTotalLoans = [self convertNumberToCurrencyStringForLabelFields:totalLoansNSNumber passedDecimalPlaces:oneDecimalNumber];
        self.totalLoansLabelField.text = returnedCurrencyStringForTotalLoans;
        
        NSNumber *averageLoanBalanceNSNumber = @(generalStats.averageLoanBalance);
        NSString *returnedCurrencyStringForAverageLoanBalance = [self convertNumberToCurrencyStringForLabelFields:averageLoanBalanceNSNumber passedDecimalPlaces:zeroDecimalNumber];
        self.averageLoanBalanceLabelField.text = returnedCurrencyStringForAverageLoanBalance;
        
        NSNumber *loanToShareRatioNSNumber = @(generalStats.loanToShareRatio);
        self.loanToshareRatioLabelField.text = [self convertNumberToFormattedStringForTextFields:loanToShareRatioNSNumber];
        
        NSNumber *mortgagesRealEstateRatioNSNumber = @(generalStats.mortgagesRealEstate);
        self.mortgagesRealEstateLabelField.text = [self convertNumberToFormattedStringForTextFields:mortgagesRealEstateRatioNSNumber];

        NSNumber *autoLoansRatioNSNumber = @(generalStats.autoLoans);
        self.autoLoansLabelField.text = [self convertNumberToFormattedStringForTextFields:autoLoansRatioNSNumber];
        
        NSNumber *unsecuredCreditCardsNSNumber = @(generalStats.unsecuredCreditCards);
        self.unsecuredCreditCardsLabelField.text = [self convertNumberToFormattedStringForTextFields:unsecuredCreditCardsNSNumber];

        NSNumber *otherNSNumber = @(generalStats.other);
        self.otherLabelField.text = [self convertNumberToFormattedStringForTextFields:otherNSNumber];
      
        NSNumber *delinquencyRatioNSNumber = @(generalStats.delinquencyRatio);
        self.delinquencyRatioLabelField.text = [self convertNumberToFormattedStringForTextFields:delinquencyRatioNSNumber];
    }
}

-(void)viewDidLoad {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [self refreshInterface];
}

-(void)viewDidDisappear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
}


#pragma mark - STRING CONVERSION METHOD
-(NSString*)convertNumberToFormattedStringForTextFields:(NSNumber*)passedNumber {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
    
    NSString *formattedString = [formatter stringFromNumber:passedNumber];
    
    return formattedString;
    
}

#pragma mark - STRING TO CURRENCY CONVERSION METHOD
-(NSString*)convertNumberToCurrencyStringForLabelFields:(NSNumber*) currencyNumber
                                   passedDecimalPlaces :(NSNumber*) passedDecimalPlaces {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    NSInteger requiredDecimalPlaces = [passedDecimalPlaces integerValue];
    
    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSString *currencyGroupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
    [currencyFormatter setGroupingSeparator:currencyGroupingSeparator];
    [currencyFormatter setGroupingSize:3];
    [currencyFormatter setAlwaysShowsDecimalSeparator:NO];
    [currencyFormatter setUsesGroupingSeparator:YES];
    [currencyFormatter setMaximumFractionDigits:requiredDecimalPlaces];
    
    NSString *formattedCurrencyString = [currencyFormatter stringFromNumber:currencyNumber];
    
    return formattedCurrencyString;
    
}



@end
