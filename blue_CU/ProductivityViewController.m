//
//  ProductivityViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 1/15/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "ProductivityViewController.h"
#import "AppDelegate.h"
#import "FS220+CoreDataClass.h"
#import "FS220A+CoreDataClass.h"
#import "RatioTabBarController.h"
#import "GrowthViewController.h"

@interface ProductivityViewController ()

@end

@implementation ProductivityViewController

#define debug 1
#define cycle_date 06

NSString *productivityDataCUNumber = @"";

NSString *productivityfs220_018 = @"";
NSString *productivityfs220_025A = @"";
NSString *productivityfs220_025B = @"";
NSString *productivityfs220_083 = @"";
NSString *productivityfs220_084 = @"";

NSString *productivityfs220a_210 =@"";
NSString *productivityfs220a_564A =@"";
NSString *productivityfs220a_564B =@"";

#pragma mark - VIEW
-(void)refreshInterface {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    // Blank out in case no record found
    self.membersPotentialMembersLabelField.text = @"0.00";
    self.borrowersMembersLabelField.text = @"0.00";
    self.membersFTELabelField.text = @"0.00";
    self.averageSharesMemberLabelField.text = @"0";
    self.averageLoanBalanceLabelField.text = @"0";
    self.salaryFTELabelField.text = @"0";
    
    // Get credit union number for fetch from RatioTabBarController
    RatioTabBarController *tabbar = (RatioTabBarController *)self.tabBarController;
    productivityDataCUNumber = [tabbar.receivedCUNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    // Get FS220 Data
    NSFetchRequest *fs220FetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220"];
    NSSortDescriptor *fs220SortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220FetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220SortDescriptor]];
    NSPredicate *fs220Filter = [NSPredicate predicateWithFormat:@"cu_number == %@", productivityDataCUNumber];
    [fs220FetchRequest setPredicate:fs220Filter];
    NSArray *fetchedFS220 = [cdh.context executeFetchRequest:fs220FetchRequest error:nil];
    
    for (FS220 *fs220 in fetchedFS220) {
        productivityfs220_018 = fs220.acct_018;
        productivityfs220_025A = fs220.acct_025A;
        productivityfs220_025B = fs220.acct_025B;
        productivityfs220_083 = fs220.acct_083;
        productivityfs220_084 = fs220.acct_084;
    }
    
    // Get FS220A Data
    NSFetchRequest *fs220aFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220A"];
    NSSortDescriptor *fs220aSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220aFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220aSortDescriptor]];
    NSPredicate *fs220aFilter = [NSPredicate predicateWithFormat:@"cu_number == %@", productivityDataCUNumber];
    [fs220aFetchRequest setPredicate:fs220aFilter];
    NSArray *fetchedFS220a = [cdh.context executeFetchRequest:fs220aFetchRequest error:nil];
    
    for (FS220A *fs220a in fetchedFS220a) {
        
        productivityfs220a_210 = fs220a.acct_210;
        productivityfs220a_564A = fs220a.acct_564A;
        productivityfs220a_564B = fs220a.acct_564B;
    }

    [self calculateRatios];
}

-(void)viewDidLoad {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [self refreshInterface];
}

-(void)viewDidDisappear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
}

#pragma mark - STRING CONVERSION METHOD
-(NSNumber*)convertStringToNumberForTextFields:(NSString*)passedString {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    float convertedNumber = 0;
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    convertedNumber = [[numberFormatter numberFromString: passedString] floatValue];
    NSNumber *returnedNumber = [NSNumber numberWithFloat:convertedNumber];
    
    return returnedNumber;
}


#pragma mark - CALCULATE RATIOS
-(void)calculateRatios {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    // Setup variables
    NSNumber *returnedNumberForfs220_018 = [self convertStringToNumberForTextFields:productivityfs220_018];
    NSNumber *returnedNumberForfs220_025A = [self convertStringToNumberForTextFields:productivityfs220_025A];
    NSNumber *returnedNumberForfs220_025B = [self convertStringToNumberForTextFields:productivityfs220_025B];
    NSNumber *returnedNumberForfs220_083 = [self convertStringToNumberForTextFields:productivityfs220_083];
    NSNumber *returnedNumberForfs220_084 = [self convertStringToNumberForTextFields:productivityfs220_084];

    NSNumber *returnedNumberForfs220a_210 = [self convertStringToNumberForTextFields:productivityfs220a_210];
    NSNumber *returnedNumberForfs220a_564A = [self convertStringToNumberForTextFields:productivityfs220a_564A];
    NSNumber *returnedNumberForfs220a_564B = [self convertStringToNumberForTextFields:productivityfs220a_564B];
    
    float f018 = [returnedNumberForfs220_018 floatValue];
    float f025A = [returnedNumberForfs220_025A floatValue];
    float f025B = [returnedNumberForfs220_025B floatValue];
    float f083 = [returnedNumberForfs220_083 floatValue];
    float f084 = [returnedNumberForfs220_084 floatValue];
    
    float f210 = [returnedNumberForfs220a_210 floatValue];
    float f564A = [returnedNumberForfs220a_564A floatValue];
    float f564B = [returnedNumberForfs220a_564B floatValue];
    
    // Calculate members / potential members (display rounded)
    if (f084 != 0) {
        float membersPotentialMembersRatio = (f083 / f084) * 100;
        NSString *formattedMembersPotentialMembersRatio = [NSString stringWithFormat:@"%.02f", membersPotentialMembersRatio];
        self.membersPotentialMembersLabelField.text = formattedMembersPotentialMembersRatio;
    } else {
        self.membersPotentialMembersLabelField.text = @"0.00";
    }
    
    // Calculate Borrowers / Members (display rounded)
    if (f083 != 0) {
        float borrowersMembersRatio = (f025A / f083) * 100;
        NSString *formattedBorrowersMembersRatio = [NSString stringWithFormat:@"%.02f", borrowersMembersRatio];
        self.borrowersMembersLabelField.text = formattedBorrowersMembersRatio;
    } else {
        self.borrowersMembersLabelField.text = @"0.00";
    }
    
    // Calculate Members / FTE (display rounded)
    if ((f564A + (f564B/2)) != 0) {
        float membersFTERatio = (f083 / (f564A + (f564B/2)));
        NSString *formattedMembersFTERatio = [NSString stringWithFormat:@"%.02f", membersFTERatio];
        self.membersFTELabelField.text = formattedMembersFTERatio;
    } else {
        self.membersFTELabelField.text = @"0.00";
    }
    
    // Calculate Average Shares per Member (display rounded)
    if (f083 != 0) {
        float avgSharesPerMemberRatio = (f018 / f083);
        int roundedAvgSharesPerMemberRatio = roundf(avgSharesPerMemberRatio);

        // Change integer to nsnumber:
        NSNumber *num = [NSNumber numberWithInt:roundedAvgSharesPerMemberRatio];
        
        // Set up the formatter:
        NSNumberFormatter *numFormatter = [[NSNumberFormatter alloc] init];
        [numFormatter setUsesGroupingSeparator:YES];
        [numFormatter setGroupingSeparator:@","];
        [numFormatter setGroupingSize:3];
        
        // Get the formatted string:
        NSString *formattedAvgSharesPerMemberRatio = [numFormatter stringFromNumber:num];
        self.averageSharesMemberLabelField.text = [NSString stringWithFormat:@"$%@",formattedAvgSharesPerMemberRatio];
    } else {
        self.averageSharesMemberLabelField.text = @"0";
    }
    
    // Calculate Average Loan Balance (display rounded)
    if (f025A != 0) {
        float avgLoanBalanceRatio = (f025B / f025A);
        int roundedAvgLoanBalanceRatio = roundf(avgLoanBalanceRatio);
        
        // Change integer to nsnumber:
        NSNumber *num = [NSNumber numberWithInt:roundedAvgLoanBalanceRatio];
        
        // Set up the formatter:
        NSNumberFormatter *numFormatter = [[NSNumberFormatter alloc] init];
        [numFormatter setUsesGroupingSeparator:YES];
        [numFormatter setGroupingSeparator:@","];
        [numFormatter setGroupingSize:3];
        
        // Get the formatted string:
        NSString *formattedAvgLoanBalanceRatio = [numFormatter stringFromNumber:num];
        self.averageLoanBalanceLabelField.text = [NSString stringWithFormat:@"$%@",formattedAvgLoanBalanceRatio];
    } else {
        self.averageLoanBalanceLabelField.text = @"0";
    }
    
    // Calculate Salary and Benefits / FTE (display rounded)
    if ((f564A + (f564B/2)) != 0) {
        float salaryBeneiftsFTERatio = ((f210 / (f564A + (f564B/2)))*12/cycle_date);
        int roundedSalaryBeneiftsFTERatio = roundf(salaryBeneiftsFTERatio);
        
        // Change integer to nsnumber:
        NSNumber *num = [NSNumber numberWithInt:roundedSalaryBeneiftsFTERatio];
        
        // Set up the formatter:
        NSNumberFormatter *numFormatter = [[NSNumberFormatter alloc] init];
        [numFormatter setUsesGroupingSeparator:YES];
        [numFormatter setGroupingSeparator:@","];
        [numFormatter setGroupingSize:3];
        
        // Get the formatted string:
        NSString *formattedSalaryBeneiftsFTERatio = [numFormatter stringFromNumber:num];
        self.salaryFTELabelField.text = [NSString stringWithFormat:@"$%@",formattedSalaryBeneiftsFTERatio];
    } else {
        self.salaryFTELabelField.text = @"0";
    }
}

#pragma mark - SEGUE
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    GrowthViewController *growthVC = segue.destinationViewController;
    if ([segue.identifier isEqualToString:@"Show Growth Ratios Segue"]) {
        growthVC.receivedCUNumber = productivityDataCUNumber;
    }
    else {
        NSLog(@"Unidentified Segue Attempted");
    }
}

@end
