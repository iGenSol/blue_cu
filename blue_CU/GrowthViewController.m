//
//  GrowthViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 1/16/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "GrowthViewController.h"
#import "AppDelegate.h"
#import "FS220+CoreDataClass.h"
#import "FS220A+CoreDataClass.h"
#import "FS220B+CoreDataClass.h"
#import "FS220_PYE+CoreDataClass.h"
#import "FS220A_PYE+CoreDataClass.h"
#import "FS220B_PYE+CoreDataClass.h"

@interface GrowthViewController ()

@end

@implementation GrowthViewController

#define debug 1
#define cycle_date 06

NSString *growthfs220_010 = @"";
NSString *growthfs220_018 = @"";
NSString *growthfs220_025B = @"";
NSString *growthfs220_083 = @"";

NSString *growthfs220_010_PYE = @"";
NSString *growthfs220_018_PYE = @"";
NSString *growthfs220_025B_PYE = @"";
NSString *growthfs220_083_PYE = @"";

NSString *growthfs220a_730B =@"";
NSString *growthfs220a_730C =@"";
NSString *growthfs220a_799I =@"";
NSString *growthfs220a_997 =@"";

NSString *growthfs220a_730B_PYE = @"";
NSString *growthfs220a_730C_PYE = @"";
NSString *growthfs220a_799I_PYE = @"";
NSString *growthfs220a_997_PYE = @"";

NSString *growthfs220b_781 =@"";

NSString *growthfs220b_781_PYE = @"";

#pragma mark - VIEW
-(void)refreshInterface {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    // Blank out in case no record found
    self.netWorthGrowthLabelField.text = @"0.00";
    self.marketShareGrowthLabelField.text = @"0.00";
    self.loanGrowthLabelField.text = @"0.00";
    self.investmentGrowthLabelField.text = @"0.00";
    self.membershipGrowthLabelField.text = @"0.00";
    self.assetGrowthLabelField.text = @"0.00";
 
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    // Get FS220 Data
    NSFetchRequest *fs220FetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220"];
    NSSortDescriptor *fs220SortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220FetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220SortDescriptor]];
    NSPredicate *fs220Filter = [NSPredicate predicateWithFormat:@"cu_number == %@", self.receivedCUNumber];
    [fs220FetchRequest setPredicate:fs220Filter];
    NSArray *fetchedFS220 = [cdh.context executeFetchRequest:fs220FetchRequest error:nil];
    
    for (FS220 *fs220 in fetchedFS220) {
        growthfs220_010 = fs220.acct_010;
        growthfs220_018 = fs220.acct_018;
        growthfs220_025B = fs220.acct_025B;
        growthfs220_083 = fs220.acct_083;
    }
    
    // Get FS220_PYE Data
    NSFetchRequest *fs220PYEFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220_PYE"];
    NSSortDescriptor *fs220PYESortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220PYEFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220PYESortDescriptor]];
    NSPredicate *fs220PYEFilter = [NSPredicate predicateWithFormat:@"cu_number == %@", self.receivedCUNumber];
    [fs220PYEFetchRequest setPredicate:fs220PYEFilter];
    NSArray *fetchedFS220PYE = [cdh.context executeFetchRequest:fs220PYEFetchRequest error:nil];
    
    for (FS220_PYE *fs220PYE in fetchedFS220PYE) {
        growthfs220_010_PYE = fs220PYE.acct_010;
        growthfs220_018_PYE = fs220PYE.acct_018;
        growthfs220_083_PYE = fs220PYE.acct_083;
        growthfs220_025B_PYE = fs220PYE.acct_025B;
    }
    
    // Get FS220A Data
    NSFetchRequest *fs220aFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220A"];
    NSSortDescriptor *fs220aSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220aFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220aSortDescriptor]];
    NSPredicate *fs220aFilter = [NSPredicate predicateWithFormat:@"cu_number == %@", self.receivedCUNumber];
    [fs220aFetchRequest setPredicate:fs220aFilter];
    NSArray *fetchedFS220a = [cdh.context executeFetchRequest:fs220aFetchRequest error:nil];
    
    for (FS220A *fs220a in fetchedFS220a) {
        growthfs220a_730B = fs220a.acct_730B;
        growthfs220a_730C = fs220a.acct_730C;
        growthfs220a_799I = fs220a.acct_799I;
        growthfs220a_997 = fs220a.acct_997;
    }
    
    // Get FS220A_PYE Data
    NSFetchRequest *fs220aPYEFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220A_PYE"];
    NSSortDescriptor *fs220aPYESortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220aPYEFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220aPYESortDescriptor]];
    NSPredicate *fs220aPYEFilter = [NSPredicate predicateWithFormat:@"cu_number == %@", self.receivedCUNumber];
    [fs220aPYEFetchRequest setPredicate:fs220aPYEFilter];
    NSArray *fetchedFS220aPYE = [cdh.context executeFetchRequest:fs220aPYEFetchRequest error:nil];
    
    for (FS220A_PYE *fs220aPYE in fetchedFS220aPYE) {
        growthfs220a_730B_PYE = fs220aPYE.acct_730B;
        growthfs220a_730C_PYE = fs220aPYE.acct_730C;
        growthfs220a_799I_PYE = fs220aPYE.acct_799I;
        growthfs220a_997_PYE = fs220aPYE.acct_997;
    }

    // Get FS220B Data
    NSFetchRequest *fs220bFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220B"];
    NSSortDescriptor *fs220bSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220bFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220bSortDescriptor]];
    NSPredicate *fs220bFilter = [NSPredicate predicateWithFormat:@"cu_number == %@", self.receivedCUNumber];
    [fs220bFetchRequest setPredicate:fs220bFilter];
    NSArray *fetchedFS220b = [cdh.context executeFetchRequest:fs220bFetchRequest error:nil];
    
    for (FS220B *fs220b in fetchedFS220b) {
        growthfs220b_781 = fs220b.acct_781;
    }
    
    // Get FS220B_PYE Data
    NSFetchRequest *fs220bPYEFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220B_PYE"];
    NSSortDescriptor *fs220bPYESortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220bPYEFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220bPYESortDescriptor]];
    NSPredicate *fs220bPYEFilter = [NSPredicate predicateWithFormat:@"cu_number == %@", self.receivedCUNumber];
    [fs220bPYEFetchRequest setPredicate:fs220bPYEFilter];
    NSArray *fetchedFS220bPYE = [cdh.context executeFetchRequest:fs220bPYEFetchRequest error:nil];
    
    for (FS220B_PYE *fs220bPYE in fetchedFS220bPYE) {
        growthfs220b_781_PYE = fs220bPYE.acct_781;
    }


    [self calculateRatios];
}

-(void)viewDidLoad {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [self refreshInterface];
}

-(void)viewDidDisappear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
}

#pragma mark - STRING CONVERSION METHOD
-(NSNumber*)convertStringToNumberForTextFields:(NSString*)passedString {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    float convertedNumber = 0;
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    convertedNumber = [[numberFormatter numberFromString: passedString] floatValue];
    NSNumber *returnedNumber = [NSNumber numberWithFloat:convertedNumber];
    
    return returnedNumber;
}


#pragma mark - CALCULATE RATIOS
-(void)calculateRatios {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    // Setup variables
    NSNumber *returnedNumberForfs220_010 = [self convertStringToNumberForTextFields:growthfs220_010];
    NSNumber *returnedNumberForfs220_018 = [self convertStringToNumberForTextFields:growthfs220_018];
    NSNumber *returnedNumberForfs220_025B = [self convertStringToNumberForTextFields:growthfs220_025B];
    NSNumber *returnedNumberForfs220_083 = [self convertStringToNumberForTextFields:growthfs220_083];
    
    NSNumber *returnedNumberForfs220_010_PYE = [self convertStringToNumberForTextFields:growthfs220_010_PYE];
    NSNumber *returnedNumberForfs220_018_PYE = [self convertStringToNumberForTextFields:growthfs220_018_PYE];
    NSNumber *returnedNumberForfs220_025B_PYE = [self convertStringToNumberForTextFields:growthfs220_025B_PYE];
    NSNumber *returnedNumberForfs220_083_PYE = [self convertStringToNumberForTextFields:growthfs220_083_PYE];
    
    NSNumber *returnedNumberForfs220a_730B = [self convertStringToNumberForTextFields:growthfs220a_730B];
    NSNumber *returnedNumberForfs220a_730C = [self convertStringToNumberForTextFields:growthfs220a_730C];
    NSNumber *returnedNumberForfs220a_799I = [self convertStringToNumberForTextFields:growthfs220a_799I];
    NSNumber *returnedNumberForfs220a_997 = [self convertStringToNumberForTextFields:growthfs220a_997];

    NSNumber *returnedNumberForfs220a_730B_PYE = [self convertStringToNumberForTextFields:growthfs220a_730B_PYE];
    NSNumber *returnedNumberForfs220a_730C_PYE = [self convertStringToNumberForTextFields:growthfs220a_730C_PYE];
    NSNumber *returnedNumberForfs220a_799I_PYE = [self convertStringToNumberForTextFields:growthfs220a_799I_PYE];
    NSNumber *returnedNumberForfs220a_997_PYE = [self convertStringToNumberForTextFields:growthfs220a_997_PYE];

    NSNumber *returnedNumberForfs220b_781 = [self convertStringToNumberForTextFields:growthfs220b_781];

    NSNumber *returnedNumberForfs220b_781_PYE = [self convertStringToNumberForTextFields:growthfs220b_781_PYE];
    
    float f010 = [returnedNumberForfs220_010 floatValue];
    float f018 = [returnedNumberForfs220_018 floatValue];
    float f025B = [returnedNumberForfs220_025B floatValue];
    float f083 = [returnedNumberForfs220_083 floatValue];

    float f010_PYE = [returnedNumberForfs220_010_PYE floatValue];
    float f018_PYE = [returnedNumberForfs220_018_PYE floatValue];
    float f025B_PYE = [returnedNumberForfs220_025B_PYE floatValue];
    float f083_PYE = [returnedNumberForfs220_083_PYE floatValue];
    
    float f730B = [returnedNumberForfs220a_730B floatValue];
    float f730C = [returnedNumberForfs220a_730C floatValue];
    float f799I = [returnedNumberForfs220a_799I floatValue];
    float f997 = [returnedNumberForfs220a_997 floatValue];

    float f730B_PYE = [returnedNumberForfs220a_730B_PYE floatValue];
    float f730C_PYE = [returnedNumberForfs220a_730C_PYE floatValue];
    float f799I_PYE = [returnedNumberForfs220a_799I_PYE floatValue];
    float f997_PYE = [returnedNumberForfs220a_997_PYE floatValue];

    float f781 = [returnedNumberForfs220b_781 floatValue];

    float f781_PYE = [returnedNumberForfs220b_781_PYE floatValue];
    
    // Calculate Net Worth Growth (display rounded)
    float f997_PYE_ABS = 0;
    if (f997_PYE < 0) {
        f997_PYE_ABS = f997_PYE * -1;
    } else {
        f997_PYE_ABS = f997_PYE;
    }
    
    if (f997_PYE_ABS != 0) {
        float netWorthGrowthRatio = (((f997 - f997_PYE) / f997_PYE_ABS) * 100)*12/cycle_date;
        NSString *formattedNetWorthGrowthRatio = [NSString stringWithFormat:@"%.02f", netWorthGrowthRatio];
        self.netWorthGrowthLabelField.text = formattedNetWorthGrowthRatio;
    } else {
        self.netWorthGrowthLabelField.text = @"0.00";
    }
    
    // Calculate Market Share Growth (display rounded)
    if (f018_PYE != 0) {
        float marketShareGrowthRatio = (((f018 - f018_PYE) / f018_PYE) * 100)*12/cycle_date;
        NSString *formattedMarketShareGrowthRatio = [NSString stringWithFormat:@"%.02f", marketShareGrowthRatio];
        self.marketShareGrowthLabelField.text = formattedMarketShareGrowthRatio;
    } else {
        self.marketShareGrowthLabelField.text = @"0.00";
    }

    // Calculate Loan Growth (display rounded)
    if (f025B_PYE != 0) {
        float loanGrowthRatio = (((f025B - f025B_PYE) / f025B_PYE) * 100) *12/cycle_date;
        NSString *formattedLoanGrowthRatio = [NSString stringWithFormat:@"%.02f", loanGrowthRatio];
        self.loanGrowthLabelField.text = formattedLoanGrowthRatio;
    } else {
        self.loanGrowthLabelField.text = @"0.00";
    }
    
    // Calculate Asset Growth (display rounded)
    if (f010_PYE != 0) {
        float assetGrowthRatio = (((f010 - f010_PYE) / f010_PYE) * 100) *12/cycle_date;
        NSString *formattedAssetGrowthRatio = [NSString stringWithFormat:@"%.02f", assetGrowthRatio];
        self.assetGrowthLabelField.text = formattedAssetGrowthRatio;
    } else {
        self.assetGrowthLabelField.text = @"0.00";
    }

    // Calculate Investment Growth (display rounded)
    if ((f799I_PYE+f730B_PYE+f730C_PYE-f781_PYE) != 0) {
        float investmentGrowthRatio = ((((f799I+f730B+f730C-f781) - (f799I_PYE+f730B_PYE+f730C_PYE-f781_PYE)) / (f799I_PYE+f730B_PYE+f730C_PYE-f781_PYE)) * 100) *12/cycle_date;
        NSString *formattedInvestmentGrowthRatio = [NSString stringWithFormat:@"%.02f", investmentGrowthRatio];
        self.investmentGrowthLabelField.text = formattedInvestmentGrowthRatio;
    } else {
        self.investmentGrowthLabelField.text = @"0.00";
    }
    
    // Calculate Member Growth (display rounded)
    if (f083_PYE != 0) {
        float memberGrowthRatio = (((f083 - f083_PYE) / f083_PYE) * 100) *12/cycle_date;
        NSString *formattedMemberGrowthRatio = [NSString stringWithFormat:@"%.02f", memberGrowthRatio];
        self.membershipGrowthLabelField.text = formattedMemberGrowthRatio;
    } else {
        self.membershipGrowthLabelField.text = @"0.00";
    }
    
}

@end
