//
//  BranchMapLocationViewController.swift
//  blue_CU
//
//  Created by Timothy Milz on 7/25/19.
//  Copyright © 2019 iGenerateSolutions Inc. All rights reserved.
//
//  Converted to Swift 5 by Swiftify v5.0.7505 - https://objectivec2swift.com/
//
//  BranchMapLocationViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 11/29/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

// import Foundation

class BranchMapLocationViewController {
    let debug = 1
    func viewDidLoad() {
        super.viewDidLoad()
        
        // Create instance of location manager
        if branchlocationManager == nil {
            branchlocationManager = CLLocationManager()
            branchlocationManager.delegate = self
        } else {
            nil
        }
        
        if branchlocationManager.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization)) {
            branchlocationManager.requestWhenInUseAuthorization()
        } else {
            nil
        }
        
        // Display map with user location and Branch location
        let strRR = "\(passed_phys_adr_1),\(passed_phys_adr_city),\(passed_phys_adr_state),\(passed_phys_adr_zip)"
        
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(strRR, completionHandler: { placemarks, error in
            if error != nil {
                if let error = error {
                    print("\(error)")
                }
            } else {
                let placemark = placemarks?.last as? CLPlacemark
                //float spanX = 0.00725;
                //float spanY = 0.00725;
                let spanX: Float = 0.1
                let spanY: Float = 0.1
                let region: MKCoordinateRegion
                region.center.latitude = placemark?.location?.coordinate.latitude ?? 0
                region.center.longitude = placemark?.location?.coordinate.longitude ?? 0
                region.span = MKCoordinateSpanMake(spanX, spanY)
                
                // Create coordinate
                let myCoordinate = [region.center.latitude, region.center.longitude] as? CLLocationCoordinate2D
                //Create annotation
                let point = MKPointAnnotation()
                // Set annotation to point at coordinate
                if let myCoordinate = myCoordinate {
                    point.coordinate = myCoordinate
                }
                // Set title and subtitle for annotation
                point.title = self.passed_cu_name
                point.subtitle = self.passed_site_name
                //Drop pin on map
                self.branchMap.addAnnotation(point)
                
                self.branchMap.setRegion(region, animated: true)
            }
        })
        
    }
    
    // Zoom in on location of Branch
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        
        if debug == 1 {
            print("Running \(BranchMapLocationViewController) '\(NSStringFromSelector(#function))'")
        }
    }
    
    func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        if debug == 1 {
            print("Running \(BranchMapLocationViewController) '\(NSStringFromSelector(#function))'")
        }
        
        if (error as NSError).code == CLError.Code.locationUnknown.rawValue {
            print("LOCATION ERROR -> Currently unable to retrieve location.")
        } else if (error as NSError).code == CLError.Code.network.rawValue {
            print("LOCATION ERROR -> Network used to retrieve location is unavailable.")
        } else if (error as NSError).code == CLError.Code.denied.rawValue {
            // Turn off location manager updates
            print("LOCATION ERROR -> Permission to retrieve location is denied.")
            //[self.locMan stopUpdatingLocation];
        }
    }
    /*
     #pragma mark - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
    
}
