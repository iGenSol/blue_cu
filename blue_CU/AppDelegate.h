//
//  AppDelegate.h
//  blue_CU
//
//  Created by Timothy Milz on 10/15/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "CoreDataHelper.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong, readonly) CoreDataHelper *coreDataHelper;
@property (strong, atomic) NSString *userPressedHome;

-(CoreDataHelper*)cdh;

@end



