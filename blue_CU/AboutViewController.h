//
//  AboutViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 3/20/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *appVersionLabelField;

@end
