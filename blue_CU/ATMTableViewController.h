//
//  ATMTableViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 11/16/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataTVC.h"

@interface ATMTableViewController : CoreDataTVC


@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *atmActivityIndicator;

@end
