//
//  AssetQualityViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 1/13/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "AssetQualityViewController.h"
#import "AppDelegate.h"
#import "FS220+CoreDataClass.h"
#import "FS220B+CoreDataClass.h"
#import "FS220_PYE+CoreDataClass.h"
#import "RatioTabBarController.h"

@interface AssetQualityViewController ()

@end

@implementation AssetQualityViewController

#define debug 1
#define cycle_date 06

NSString *assetQualityDataCUNumber = @"";

NSString *assetQualityfs220_010 = @"";
NSString *assetQualityfs220_025B = @"";
NSString *assetQualityfs220_041B = @"";
NSString *assetQualityfs220_550 = @"";
NSString *assetQualityfs220_551 = @"";

NSString *assetQualityfs220_025B_PYE = @"";

NSString *assetQualityfs220b_796E =@"";
NSString *assetQualityfs220b_797E =@"";
NSString *assetQualityfs220b_801 =@"";
NSString *assetQualityfs220b_945 =@"";

#pragma mark - VIEW
-(void)refreshInterface {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    // Blank out in case no record found
    
    self.DelinqLoansTotalLoansLabelField.text = @"0.00";
    self.netChargeOffsAvgLoansLabelField.text = @"0.00";
    self.fairMarketBookValueLabelField.text = @"0.00";
    self.AccumulatedGainLossLabelField.text = @"0.00";
    self.DelinqLoansAssetsLabelField.text = @"0.00";
    
    // Get credit union number for fetch from ExtendedInfoTabBarController
    RatioTabBarController *tabbar = (RatioTabBarController *)self.tabBarController;
    assetQualityDataCUNumber = [tabbar.receivedCUNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    // Get FS220 Data
    NSFetchRequest *fs220FetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220"];
    NSSortDescriptor *fs220SortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220FetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220SortDescriptor]];
    NSPredicate *fs220Filter = [NSPredicate predicateWithFormat:@"cu_number == %@", assetQualityDataCUNumber];
    [fs220FetchRequest setPredicate:fs220Filter];
    NSArray *fetchedFS220 = [cdh.context executeFetchRequest:fs220FetchRequest error:nil];
    
    for (FS220 *fs220 in fetchedFS220) {
        
        assetQualityfs220_010 = fs220.acct_010;
        assetQualityfs220_025B = fs220.acct_025B;
        assetQualityfs220_041B = fs220.acct_041B;
        assetQualityfs220_550 = fs220.acct_550;
        assetQualityfs220_551 = fs220.acct_551;
    }
    
    // Get FS220_PYE Data
    NSFetchRequest *fs220PYEFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220_PYE"];
    NSSortDescriptor *fs220PYESortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220PYEFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220PYESortDescriptor]];
    NSPredicate *fs220PYEFilter = [NSPredicate predicateWithFormat:@"cu_number == %@", assetQualityDataCUNumber];
    [fs220PYEFetchRequest setPredicate:fs220PYEFilter];
    NSArray *fetchedFS220PYE = [cdh.context executeFetchRequest:fs220PYEFetchRequest error:nil];
    
    for (FS220_PYE *fs220PYE in fetchedFS220PYE) {
        
        assetQualityfs220_025B_PYE = fs220PYE.acct_025B;
    }
    
    // Get FS220b Data
    NSFetchRequest *fs220bFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220B"];
    NSSortDescriptor *fs220bSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220bFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220bSortDescriptor]];
    NSPredicate *fs220bFilter = [NSPredicate predicateWithFormat:@"cu_number == %@", assetQualityDataCUNumber];
    [fs220bFetchRequest setPredicate:fs220bFilter];
    NSArray *fetchedFS220b = [cdh.context executeFetchRequest:fs220bFetchRequest error:nil];
    
    for (FS220B *fs220b in fetchedFS220b) {
        
        assetQualityfs220b_796E = fs220b.acct_796E;
        assetQualityfs220b_797E = fs220b.acct_797E;
        assetQualityfs220b_801 = fs220b.acct_801;
        assetQualityfs220b_945 = fs220b.acct_945;
    }
    
    [self calculateRatios];
}

-(void)viewDidLoad {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [self refreshInterface];
}

-(void)viewDidDisappear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
}

#pragma mark - STRING CONVERSION METHOD
-(NSNumber*)convertStringToNumberForTextFields:(NSString*)passedString {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    float convertedNumber = 0;
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    convertedNumber = [[numberFormatter numberFromString: passedString] floatValue];
    NSNumber *returnedNumber = [NSNumber numberWithFloat:convertedNumber];
    
    return returnedNumber;
}


#pragma mark - CALCULATE RATIOS
-(void)calculateRatios {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    // Setup variables
    NSNumber *returnedNumberForfs220_010 = [self convertStringToNumberForTextFields:assetQualityfs220_010];
    NSNumber *returnedNumberForfs220_025B = [self convertStringToNumberForTextFields:assetQualityfs220_025B];
    NSNumber *returnedNumberForfs220_041B = [self convertStringToNumberForTextFields:assetQualityfs220_041B];
    NSNumber *returnedNumberForfs220_550 = [self convertStringToNumberForTextFields:assetQualityfs220_550];
    NSNumber *returnedNumberForfs220_551 = [self convertStringToNumberForTextFields:assetQualityfs220_551];

    NSNumber *returnedNumberForfs220_025B_PYE = [self convertStringToNumberForTextFields:assetQualityfs220_025B_PYE];
    
    NSNumber *returnedNumberForfs220b_796E = [self convertStringToNumberForTextFields:assetQualityfs220b_796E];
    NSNumber *returnedNumberForfs220b_797E = [self convertStringToNumberForTextFields:assetQualityfs220b_797E];
    NSNumber *returnedNumberForfs220b_801 = [self convertStringToNumberForTextFields:assetQualityfs220b_801];
    NSNumber *returnedNumberForfs220b_945 = [self convertStringToNumberForTextFields:assetQualityfs220b_945];
    
    float f010 = [returnedNumberForfs220_010 floatValue];
    float f025B = [returnedNumberForfs220_025B floatValue];
    float f041B = [returnedNumberForfs220_041B floatValue];
    float f550 = [returnedNumberForfs220_550 floatValue];
    float f551 = [returnedNumberForfs220_551 floatValue];

    float f025B_PYE = [returnedNumberForfs220_025B_PYE floatValue];
    
    float f796E = [returnedNumberForfs220b_796E floatValue];
    float f797E = [returnedNumberForfs220b_797E floatValue];
    float f801 = [returnedNumberForfs220b_801 floatValue];
    float f945 = [returnedNumberForfs220b_945 floatValue];
    
    // Calculate Delinquent Loans / Total Loans (display rounded)
    if (f025B != 0) {
        float DlLoansRatio = (f041B / f025B) * 100;
        NSString *formattedDlLoansRatio = [NSString stringWithFormat:@"%.02f", DlLoansRatio];
        self.DelinqLoansTotalLoansLabelField.text = formattedDlLoansRatio;
    } else {
        self.DelinqLoansTotalLoansLabelField.text = @"0.00";
    }
    
    // Calculate Net Charge Offs / Average loans (display rounded)
    if (((f025B + f025B_PYE)/2) != 0) {
        float netChargeOffsAvgLoansRatio = (((f550 - f551) / ((f025B + f025B_PYE)/2)) * 100) * 12/cycle_date;
        NSString* formattednetChargeOffsAvgLoansRatio = [NSString stringWithFormat:@"%.02f", netChargeOffsAvgLoansRatio];
        self.netChargeOffsAvgLoansLabelField.text = formattednetChargeOffsAvgLoansRatio;
    } else {
        self.netChargeOffsAvgLoansLabelField.text = @"0.00";
    }
    
    // Calculate Fair Market Investments / Book Value Investments (display rounded)
    if (f796E != 0) {
        float fairMarketRatio = (f801 / f796E) * 100;
        NSString* formattedfairMarketRatio = [NSString stringWithFormat:@"%.02f", fairMarketRatio];
        self.fairMarketBookValueLabelField.text = formattedfairMarketRatio;
    } else {
        self.fairMarketBookValueLabelField.text = @"0.00";
    }
    
    // Calculate Accum Gain/Loss / Cost of Sale Investments (display rounded)
    if ((f797E - f945) != 0) {
        float accumulatedGainLossRatio = (f945 / (f797E - f945)) * 100;
        NSString* formattedAccumulatedGainLossRatio = [NSString stringWithFormat:@"%.02f", accumulatedGainLossRatio];
        self.AccumulatedGainLossLabelField.text = formattedAccumulatedGainLossRatio;
    } else {
        self.AccumulatedGainLossLabelField.text = @"0.00";
    }
    
    // Calculate Delinquent Loans / Assets (display rounded)
    if (f010 != 0) {
        float delinqLoansAssetsRatio = (f041B / f010) * 100;
        NSString* formattedDelinqLoansAssetsRatio = [NSString stringWithFormat:@"%.02f", delinqLoansAssetsRatio];
        self.DelinqLoansAssetsLabelField.text = formattedDelinqLoansAssetsRatio;
    } else {
        self.DelinqLoansAssetsLabelField.text = @"0.00";
    }
}


@end
