//
//  ExtendedInfoTabBarContollerViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 11/12/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExtendedInfoTabBarContoller : UITabBarController <UITabBarControllerDelegate>

// Define variables passed for Extended Info View, from FOICUViewController
@property (weak, nonatomic) NSString *receivedAttentionOf;
@property (weak, nonatomic) NSString *receivedCharterState;
@property (weak, nonatomic) NSString *receivedCongDist;
@property (weak, nonatomic) NSString *receivedCountyCode;
@property (weak, nonatomic) NSString *receivedCUType;
@property (weak, nonatomic) NSString *receivedDistrict;
@property (weak, nonatomic) NSString *receivedIssueDate;
@property (weak, nonatomic) NSString *receivedLimitedInc;
@property (weak, nonatomic) NSString *receivedPeerGroup;
@property (weak, nonatomic) NSString *receivedQuarterFlag;
@property (weak, nonatomic) NSString *receivedRegion;
@property (weak, nonatomic) NSString *receivedRSSD;
@property (weak, nonatomic) NSString *receivedSMSA;
@property (weak, nonatomic) NSString *receivedCUNumber;

@end
