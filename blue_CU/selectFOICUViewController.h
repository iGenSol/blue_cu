//
//  selectFOICUViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 10/30/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataHelper.h"

@interface selectFOICUViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;

@property (strong, nonatomic) IBOutlet UITextField *nameSearchTextField;
@property (strong, nonatomic) IBOutlet UIButton *showMeButton;
@property (weak, nonatomic) IBOutlet UILabel *numberOfFOICU;
@property (weak, nonatomic) IBOutlet UIPickerView *statePickerTextField;


// Passed strings for search predicate on table view
@property (strong, nonatomic) NSString *passedCUNameSearch;
@property (strong, nonatomic) NSString *passedCUStateSearch;

@end
