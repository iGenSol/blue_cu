//
//  ATMTableViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 11/16/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import "ATMTableViewController.h"
#import "ATMLocationsViewController.h"
#import "ATMLocations+CoreDataClass.h"
#import "ExtendedInfoTabBarContoller.h"
#import "AppDelegate.h"

@interface ATMTableViewController ()

@end

@implementation ATMTableViewController

#define debug 1

NSString *atmCUNumber = @"";

#pragma mark - DATA
-(void)configureFetch{
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"ATMLocations"];
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"site_name" ascending:YES];
    [request setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"cu_number == %@", atmCUNumber];
    [request setPredicate:filter];
    
    [request setFetchBatchSize:50];
    
    self.frc = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:cdh.context sectionNameKeyPath:@"site_name" cacheName:nil];
    self.frc.delegate = self;
}


#pragma mark - VIEW
-(void)viewDidLoad {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }

    [super viewDidLoad];
    
    self.tableView.delegate = self;
    
    ExtendedInfoTabBarContoller *tabbar = (ExtendedInfoTabBarContoller *)self.tabBarController;
    atmCUNumber = [tabbar.receivedCUNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [self.atmActivityIndicator startAnimating];
    self.atmActivityIndicator.hidden = NO;
    
    [self configureFetch];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // load on background thread
        [self performFetch];
        
        dispatch_async(dispatch_get_main_queue(), ^{ // return to main thread
            
            if ([self.foundTableDataYN isEqualToString:@"N"]) {
                [self.atmActivityIndicator stopAnimating];
                self.atmActivityIndicator.hidden = YES;
                [self showNoATMsFoundAlert];
            }
        });
    });
}

-(void)viewWillAppear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    static NSString *cellIdentifier = @"ATM Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryDetailButton;
    
    ATMLocations *atmLocations = [self.frc objectAtIndexPath:indexPath];
    
    NSMutableString *title = [NSMutableString stringWithFormat:@"%@ \n   %@ %@", atmLocations.site_name, atmLocations.phys_adr_city, atmLocations.phys_adr_state_code];
    [title replaceOccurrencesOfString:@"(null)" withString:@"" options:0 range:NSMakeRange(0, [title length])];
    
    cell.textLabel.numberOfLines = 0; // removes number of lines limitation, allows city, state on second line
    cell.textLabel.text = title;
    
    // make selected items white
        [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica Neue" size:14]];
        [cell.textLabel setTextColor:[UIColor whiteColor]];
    
    // Now that we have data, turn off the activity indicator
    [self.atmActivityIndicator stopAnimating];
    self.atmActivityIndicator.hidden = YES;
    
    return cell;
}

-(NSArray*)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    return nil; // We don't want a section index
}

// Override titleForHeaderInSection from CoreDataTVC as we don't want section titles
- (NSString *)tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    return @""; // No Title
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleNone;
}

#pragma mark - SEGUE
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    ATMLocationsViewController *atmLocationsVC = segue.destinationViewController;
    if ([segue.identifier isEqualToString:@"Show ATM Location Detail Object Segue"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        atmLocationsVC.selectedATMID = [[self.frc objectAtIndexPath:indexPath] objectID];
    }
    else {
        NSLog(@"Unidentified Segue Attempted");
    }
}

-(void) tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    ATMLocationsViewController *atmLocationsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ATMLocationsViewController"];
    atmLocationsVC.selectedATMID = [[self.frc objectAtIndexPath:indexPath] objectID];
    [self.navigationController pushViewController:atmLocationsVC animated:YES];
}


#pragma mark - ALERT PROCESSING

- (void)showNoATMsFoundAlert {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    UIAlertController *noATMsFoundAlert = [UIAlertController alertControllerWithTitle:@"No ATMs Found"
                                                                             message:[NSString stringWithFormat:@"No ATM data supplied for this credit union  \n"]
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [noATMsFoundAlert addAction:defaultAction];
    [self presentViewController:noATMsFoundAlert animated:YES completion:nil];
}


@end
