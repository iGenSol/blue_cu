//
//  DemographicsViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 12/8/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import "AppDelegate.h"
#import "DemographicsViewController.h"
#import "ExtendedInfoTabBarContoller.h"
#import "FS220D+CoreDataProperties.h"

@interface DemographicsViewController ()

@end

@implementation DemographicsViewController

#define debug 1

NSString *demographicsCUNumber = @"";

#pragma mark - VIEW
-(void)refreshInterface {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    // Blank out in case no record found
    self.africanAmericanMemberLabelField.text = @"";
    self.asianAmericanMemberLabelField.text = @"";
    self.hispanicAmericanMemberLabelField.text = @"";
    self.nativeAmericanMemberLabelField.text = @"";
    self.minorityStatusMemberLabelField.text = @"";
    
    // Load views with passed variables from FOICUViewController via ExtendedInfoTabBarController
    ExtendedInfoTabBarContoller *tabbar = (ExtendedInfoTabBarContoller *)self.tabBarController;
    demographicsCUNumber = [tabbar.receivedCUNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *demographicsInfoFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220D"];
    NSSortDescriptor *demographicsInfoSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [demographicsInfoFetchRequest setSortDescriptors:[NSArray arrayWithObject:demographicsInfoSortDescriptor]];
    NSPredicate *demographicsInfoFilter = [NSPredicate predicateWithFormat:@"cu_number == %@", demographicsCUNumber];
    [demographicsInfoFetchRequest setPredicate:demographicsInfoFilter];
    NSArray *fetchedDemographicsInfo = [cdh.context executeFetchRequest:demographicsInfoFetchRequest error:nil];
    
    for (FS220D *fs220d in fetchedDemographicsInfo) {
        
        self.africanAmericanMemberLabelField.text = [self isStatusCodeYesOrNo:fs220d.member_african_american];
        self.asianAmericanMemberLabelField.text = [self isStatusCodeYesOrNo:fs220d.member_asian_american];
        self.hispanicAmericanMemberLabelField.text = [self isStatusCodeYesOrNo:fs220d.member_hispanic_american];
        self.nativeAmericanMemberLabelField.text = [self isStatusCodeYesOrNo:fs220d.member_native_american];
        self.minorityStatusMemberLabelField.text = [self isStatusCodeYesOrNo:fs220d.eligible_minority_status];
    }
}

-(void)viewDidLoad {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [super viewDidLoad];
    
    self.tabBarController.delegate = self;
    self.tabBarController.title = @"Demographics Info";
}

-(void)viewWillAppear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [self refreshInterface];
}

-(void)viewDidDisappear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
}

#pragma mark - YES/NO Member and Eligible METHOD
- (NSString*)isStatusCodeYesOrNo:(NSString *)statusCode {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    NSString *statusCodeYN = @"";
    statusCode = [statusCode stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if ([statusCode isEqualToString:@"0"]) {
        statusCodeYN = @"No";
    } else if ([statusCode isEqualToString:@"1"]) {
        statusCodeYN = @"Yes";
    }
    
    return statusCodeYN;
}

@end
