//
//  FOICUTableViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 10/29/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import "FOICUTableViewController.h"
#import "FOICUViewController.h"
#import "FOICU+CoreDataClass.h"
#import "AppDelegate.h"

@interface FOICUTableViewController ()

@end

@implementation FOICUTableViewController

#define debug 1

BOOL didUserPassState = NO;

#pragma mark - DATA
-(void)configureFetch{
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    if ([self.passedCUNameSearch isEqualToString:@"*"] && (!didUserPassState)) {
        
        NSFetchRequest *request = [[[cdh model] fetchRequestTemplateForName:@"selectFOICUListed"] copy];
        
        request.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"cu_name" ascending:YES], nil];
        
        [request setFetchBatchSize:50];
        
        self.frc = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:cdh.context sectionNameKeyPath:@"cu_name" cacheName:nil];
        self.frc.delegate = self;

    } else {
        
        if (![self.passedCUNameSearch isEqualToString:@"*"] && ![self.passedCUNameSearch isEqualToString:@""] && (!didUserPassState)) {

            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"FOICU"];
    
            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"cu_name" ascending:YES];
            [request setSortDescriptors:[NSArray arrayWithObject:sort]];
    
            NSPredicate *filter = [NSPredicate predicateWithFormat:@"cu_name CONTAINS[c] %@", self.passedCUNameSearch];
            [request setPredicate:filter];
    
            [request setFetchBatchSize:50];

            self.frc = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:cdh.context sectionNameKeyPath:@"cu_name" cacheName:nil];
            self.frc.delegate = self;
        
        } else {
            
            if (didUserPassState) { // State search takes priority
                
                NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"FOICU"];
                
                NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"cu_name" ascending:YES];
                [request setSortDescriptors:[NSArray arrayWithObject:sort]];
                
                NSPredicate *filter = [NSPredicate predicateWithFormat:@"state CONTAINS[c] %@", self.passedCUStateSearch];
                [request setPredicate:filter];
                
                [request setFetchBatchSize:50];
                
                self.frc = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:cdh.context sectionNameKeyPath:@"cu_name" cacheName:nil];
                self.frc.delegate = self;
            }
        }
    }
}


#pragma mark - VIEW
-(void)viewDidLoad {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    didUserPassState = NO;
    
    if (![self.passedCUStateSearch isEqualToString:@""]) {
        didUserPassState = YES;
    }
    
    /*
    // if user hasn't yet agreed to terms, then display the terms alert. Continue to display until they do, else they will be forced to quit the app
    NSNumber *userAgreedToTermsYN =
    [[NSUserDefaults standardUserDefaults] objectForKey:@"userAgreedToTermsYN"];
    
    if ([userAgreedToTermsYN isEqual:@0]) {
        [self showUserTerms];
    }
    */
     
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    
    [self.foicuActivityIndicator startAnimating];
    self.foicuActivityIndicator.hidden = NO;
    
    [self configureFetch];
    //[self performSelectorInBackground:@selector(performFetch) withObject:self]; // load in background thread due to large volume
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ // load on background thread
        [self performFetch];
    
        dispatch_async(dispatch_get_main_queue(), ^{ // return to main thread
        
        if ([self.foundTableDataYN isEqualToString:@"N"]) {
            [self.foicuActivityIndicator stopAnimating];
            self.foicuActivityIndicator.hidden = YES;
            [self showNoCreditUnionsFoundAlert];
        }
        });
    });
}

-(void)viewWillAppear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    static NSString *cellIdentifier = @"FOICU Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryDetailButton;
    
    FOICU *foicu = [self.frc objectAtIndexPath:indexPath];
    
    NSMutableString *title = [NSMutableString stringWithFormat:@"%@ \n   %@ %@", foicu.cu_name, foicu.city, foicu.state];
    [title replaceOccurrencesOfString:@"(null)" withString:@"" options:0 range:NSMakeRange(0, [title length])];
    
    cell.textLabel.numberOfLines = 0; // removes number of lines limitation, allows city, state on second line
    cell.textLabel.text = title;
    
    // make selected items white
    if (foicu.listed) {
        [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica Neue" size:14]];
        [cell.textLabel setTextColor:[UIColor whiteColor]];
    } else {
        [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica Neue" size:14]];
        [cell.textLabel setTextColor:[UIColor blueColor]];
    }
    
    // Now that we have data, turn off the activity indicator
    [self.foicuActivityIndicator stopAnimating];
    self.foicuActivityIndicator.hidden = YES;
    
    return cell;
}

-(NSArray*)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    return nil; // We don't want a section index
}

// Override titleForHeaderInSection from CoreDataTVC as we don't want section titles
- (NSString *)tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    return @""; // No Title
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleNone;
}

#pragma mark - SEGUE
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    FOICUViewController *foicuVC = segue.destinationViewController;
    if ([segue.identifier isEqualToString:@"Show Credit Union Object Segue"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        foicuVC.selectedCUID = [[self.frc objectAtIndexPath:indexPath] objectID];
        
    }
    else {
        NSLog(@"Unidentified Segue Attempted");
    }
}

-(void) tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    FOICUViewController *foicuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FOICUViewController"];
    foicuVC.selectedCUID = [[self.frc objectAtIndexPath:indexPath] objectID];
    [self.navigationController pushViewController:foicuVC animated:YES];
}


#pragma mark - ALERT PROCESSING

- (void)showNoCreditUnionsFoundAlert {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    UIAlertController *noCUsFoundAlert = [UIAlertController alertControllerWithTitle:@"No Credit Unions Found"
                                                                          message:[NSString stringWithFormat:@"No Federally Insured Credit Unions Found That Match Your Selection Criteria  \n"]
                                                                   preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [noCUsFoundAlert addAction:defaultAction];
    [self presentViewController:noCUsFoundAlert animated:YES completion:nil];
}



@end
