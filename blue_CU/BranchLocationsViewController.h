//
//  BranchLocationsViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 11/28/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataHelper.h"

@interface BranchLocationsViewController : UIViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSManagedObjectID *selectedBranchID;
@property (strong, nonatomic) IBOutlet UILabel *site_nameLabelField;
@property (strong, nonatomic) IBOutlet UILabel *site_typeLabelField;
@property (strong, nonatomic) IBOutlet UILabel *phys_adr_1LabelField;
@property (strong, nonatomic) IBOutlet UILabel *phys_adr_2LabelField;
@property (strong, nonatomic) IBOutlet UILabel *phys_adr_cityLabelField;
@property (strong, nonatomic) IBOutlet UILabel *phys_adr_state_codeLabelField;
@property (strong, nonatomic) IBOutlet UILabel *phys_adr_postal_codeLabelField;
@property (strong, nonatomic) IBOutlet UILabel *phys_adr_county_nameLabelField;
@property (strong, nonatomic) IBOutlet UILabel *phys_adr_countryLabelField;
@property (strong, nonatomic) IBOutlet UIButton *branchCallPhone;

@end
