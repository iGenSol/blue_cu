//
//  AssetLiabilityViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 1/14/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssetLiabilityViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *longTermAssetsTLAssetsLabelField;
@property (strong, nonatomic) IBOutlet UILabel *tlLoansTLSharesLabelField;
@property (strong, nonatomic) IBOutlet UILabel *cashInvAssetsLabelField;
@property (strong, nonatomic) IBOutlet UILabel *sharesTLSharesLabelField;
@property (strong, nonatomic) IBOutlet UILabel *superIntRateRiskNetWorthLabelField;
@property (strong, nonatomic) IBOutlet UILabel *sharesTLSharesBorrowLabelField;
@property (strong, nonatomic) IBOutlet UILabel *tlLoansTLAssetsLabelField;
@property (strong, nonatomic) IBOutlet UILabel *sharesDepBorrsAssetsLabelField;
@property (strong, nonatomic) IBOutlet UILabel *borrsSharesNetWorthLabelField;

@end
