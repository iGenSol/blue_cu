//
//  BranchTableViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 11/28/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataTVC.h"

@interface BranchTableViewController : CoreDataTVC

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *branchActivityIndicator;

@end
