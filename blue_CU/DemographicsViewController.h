//
//  DemographicsViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 12/8/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DemographicsViewController : UIViewController <UITabBarControllerDelegate>

@property (strong, nonatomic) IBOutlet UILabel *africanAmericanMemberLabelField;
@property (strong, nonatomic) IBOutlet UILabel *asianAmericanMemberLabelField;
@property (strong, nonatomic) IBOutlet UILabel *hispanicAmericanMemberLabelField;
@property (strong, nonatomic) IBOutlet UILabel *nativeAmericanMemberLabelField;
@property (strong, nonatomic) IBOutlet UILabel *minorityStatusMemberLabelField;

@end
