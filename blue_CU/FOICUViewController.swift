//
//  FOICUViewController.swift
//  blue_CU
//
//  Created by Timothy Milz on 7/24/19.
//  Copyright © 2019 iGenerateSolutions Inc. All rights reserved.
//
//  Converted to Swift 5 by Swiftify v5.0.7505 - https://objectivec2swift.com/
//
//  FOICUViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 10/30/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

//import Foundation

let debug = 1

// Variables for Extended Info View
var foicuCharterState = ""
var foicuCongDist = ""
var foicuCountyCode = ""
var foicuCUType = ""
var foicuNCUADistrict = ""
var foicuCharterInsuranceEffDate = ""
var foicuLimitedIncomeCode = ""
var foicuPeerGroup = ""
var foicuQuarterFlag = ""
var foicuNCUARegion = ""
var foicuRSSD = ""
var foicuSMSA = ""
var foicuCUNumber = ""

class FOICUViewController {
    private var extendedInfoVC: ExtendedInfoViewController?
    
    func hideKeyBoardWhenBackgroundIsTapped() {
        
        if debug == 1 {
            print("Running \(FOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        let tgr = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tgr.cancelsTouchesInView = false
        view.addGestureRecognizer(tgr)
    }
    
    @objc func hideKeyboard() {
        
        if debug == 1 {
            print("Running \(FOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        view.endEditing(true)
    }
    
    // MARK: - VIEW
    func refreshInterface() {
        
        if debug == 1 {
            print("Running \(FOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        if selectedCUID {
            let cdh = (UIApplication.shared.delegate as? AppDelegate)?.cdh()
            var foicu: FOICU? = nil
            do {
                foicu = try cdh?.context.existingObject(with: selectedCUID) as? FOICU
            } catch {
            }
            
            cu_nameLabelField.text = foicu?.cu_name
            cityLabelField.text = foicu?.city
            streetLabelField.text = foicu?.street
            state_codeLabelField.text = foicu?.state
            zip_codeLabelField.text = foicu?.zip_code
            year_openedLabelField.text = foicu?.year_opened
            
            let fullString = foicu?.cycle_date
            var prefix: String? = nil
            
            if (fullString?.count ?? 0) >= 10 {
                
                let length = fullString?.count ?? 0
                let buffer = [unichar](repeating: 0, count: length + 1)
                (fullString as NSString?)?.getCharacters(&buffer, range: NSRange(location: 0, length: length))
                
                for i in 0..<length {
                    if buffer[i] == 32 {
                        //if string is a space (uniChar 32), stop iteration
                        prefix = (fullString as? NSString)?.substring(to: i)
                        i = length + 1
                    }
                }
            } else {
                
                prefix = fullString
            }
            cycle_dateLabelField.text = prefix
            
            // Get FS220 Data
            let fs220FetchRequest = NSFetchRequest(entityName: "FS220")
            let fs220SortDescriptor = NSSortDescriptor(key: "cu_number", ascending: true)
            fs220FetchRequest.sortDescriptors = [fs220SortDescriptor]
            var fs220Filter: NSPredicate? = nil
            if let cu_number = foicu?.cu_number {
                fs220Filter = NSPredicate(format: "cu_number == %@", cu_number)
            }
            fs220FetchRequest.predicate = fs220Filter
            var fetchedFS220: [Any]? = nil
            do {
                fetchedFS220 = try cdh?.context.fetch(fs220FetchRequest)
            } catch {
            }
            
            for fs220 in fetchedFS220 ?? [] {
                guard let fs220 = fs220 as? FS220 else {
                    continue
                }
                
                // Convert Total Assets to Currency String
                let returnedNumberForTotalAssetsString = convertStringToNumber(forTextFields: fs220.acct_010)
                let returnedCurrencyStringForTotalAssets = convertNumberToCurrencyString(forLabelFields: returnedNumberForTotalAssetsString)
                totalAssetsLabelField.text = returnedCurrencyStringForTotalAssets
                
                // Convert Current Members to Decimal String
                let returnedNumberForCurrentMembersString = convertStringToNumber(forTextFields: fs220.current_members)
                let returnedDecimalStringForCurrentMembers = convertNumberToDecimalString(forLabelFields: returnedNumberForCurrentMembersString)
                currentMembersLabelField.text = returnedDecimalStringForCurrentMembers
                
                // Convert Potential Members to Decimal String
                let returnedNumberForPotentialMembersString = convertStringToNumber(forTextFields: fs220.potential_members)
                let returnedDecimalStringForPotentialMembers = convertNumberToDecimalString(forLabelFields: returnedNumberForPotentialMembersString)
                potentialMembersLabelField.text = returnedDecimalStringForPotentialMembers
            }
            
            // Setup variables for extended info
            foicuCharterState = foicu?.charter_state ?? ""
            foicuCongDist = foicu?.cong_dist ?? ""
            foicuCountyCode = foicu?.county_code ?? ""
            foicuCUType = foicu?.cu_type ?? ""
            foicuNCUADistrict = foicu?.district ?? ""
            foicuCharterInsuranceEffDate = foicu?.issue_date ?? ""
            foicuLimitedIncomeCode = foicu?.limited_inc ?? ""
            foicuPeerGroup = foicu?.peer_group ?? ""
            foicuQuarterFlag = foicu?.quarter_flag ?? ""
            foicuNCUARegion = foicu?.region ?? ""
            foicuRSSD = foicu?.rssd ?? ""
            foicuSMSA = foicu?.smsa ?? ""
            foicuCUNumber = foicu?.cu_number ?? ""
        }
    }
    
    func viewDidLoad() {
        
        if debug == 1 {
            print("Running \(FOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        super.viewDidLoad()
        hideKeyBoardWhenBackgroundIsTapped()
    }
    
    func viewWillAppear(_ animated: Bool) {
        
        if debug == 1 {
            print("Running \(FOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        refreshInterface()
    }
    
    func viewDidDisappear(_ animated: Bool) {
        
        if debug == 1 {
            print("Running \(FOICUViewController) '\(NSStringFromSelector(#function))'")
        }
    }
    
    // MARK: - SEGUE
    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if debug == 1 {
            print("Running \(FOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        // Setup Extended Info View data to pass
        if (segue.identifier == "Show Extended Info Segue") {
            let extendedInfoTBC = segue.destination as? ExtendedInfoTabBarContoller
            
            // For ExtendedInfoViewController:
            extendedInfoTBC?.receivedCharterState = foicuCharterState
            extendedInfoTBC?.receivedCongDist = foicuCongDist
            extendedInfoTBC?.receivedCountyCode = foicuCountyCode
            extendedInfoTBC?.receivedCUType = foicuCUType
            extendedInfoTBC?.receivedDistrict = foicuNCUADistrict
            extendedInfoTBC?.receivedIssueDate = foicuCharterInsuranceEffDate
            extendedInfoTBC?.receivedLimitedInc = foicuLimitedIncomeCode
            extendedInfoTBC?.receivedPeerGroup = foicuPeerGroup
            extendedInfoTBC?.receivedQuarterFlag = foicuQuarterFlag
            extendedInfoTBC?.receivedRegion = foicuNCUARegion
            extendedInfoTBC?.receivedRSSD = foicuRSSD
            extendedInfoTBC?.receivedSMSA = foicuSMSA
            extendedInfoTBC?.receivedCUNumber = foicuCUNumber
        } else if (segue.identifier == "Show Ratios Segue") {
            let ratioTBC = segue.destination as? RatioTabBarController
            
            // For RatioTabViewController:
            ratioTBC?.receivedCUNumber = foicuCUNumber
        } else {
            
            print("Unidentified Segue Attempted")
        }
    }
    
    // MARK: - STRING CONVERSION METHOD
    func convertStringToNumber(forTextFields passedString: String?) -> NSNumber? {
        
        if debug == 1 {
            print("Running \(FOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        var convertedNumber: Float = 0
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        convertedNumber = (numberFormatter.number(from: passedString ?? ""))?.floatValue ?? 0.0
        let returnedNumber = NSNumber(value: convertedNumber)
        
        return returnedNumber
    }
    
    // MARK: - STRING TO CURRENCY CONVERSION METHOD
    func convertNumberToCurrencyString(forLabelFields passedCurrencyNumber: NSNumber?) -> String? {
        
        if debug == 1 {
            print("Running \(FOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.numberStyle = .currency
        let currencyGroupingSeparator = NSLocale.current[NSLocale.Key.groupingSeparator] as? String
        currencyFormatter.groupingSeparator = currencyGroupingSeparator
        currencyFormatter.groupingSize = 3
        currencyFormatter.alwaysShowsDecimalSeparator = false
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.maximumFractionDigits = 0
        
        var formattedCurrencyString: String? = nil
        if let passedCurrencyNumber = passedCurrencyNumber {
            formattedCurrencyString = currencyFormatter.string(from: passedCurrencyNumber)
        }
        
        return formattedCurrencyString
        
    }
    
    // MARK: - STRING TO DECIMAL CONVERSION METHOD
    func convertNumberToDecimalString(forLabelFields passedDecimalNumber: NSNumber?) -> String? {
        
        if debug == 1 {
            print("Running \(FOICUViewController) '\(NSStringFromSelector(#function))'")
        }
        
        let decimalFormatter = NumberFormatter()
        decimalFormatter.numberStyle = .decimal
        let decimalGroupingSeparator = NSLocale.current[NSLocale.Key.groupingSeparator] as? String
        decimalFormatter.groupingSeparator = decimalGroupingSeparator
        decimalFormatter.groupingSize = 3
        decimalFormatter.alwaysShowsDecimalSeparator = false
        decimalFormatter.usesGroupingSeparator = true
        decimalFormatter.maximumFractionDigits = 0
        
        var formattedDecimalString: String? = nil
        if let passedDecimalNumber = passedDecimalNumber {
            formattedDecimalString = decimalFormatter.string(from: passedDecimalNumber)
        }
        
        return formattedDecimalString
        
    }
}
