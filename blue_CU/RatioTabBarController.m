//
//  RatioTabBarController.m
//  blue_CU
//
//  Created by Timothy Milz on 3/13/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "RatioTabBarController.h"

@interface RatioTabBarController ()

@end

@implementation RatioTabBarController

#define debug 0

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationItem setTitle:@"Capital Adequacy"];
    self.tabBarController.delegate = self;
    //[self.navigationItem setHidesBackButton:YES];
}


-(void)tabBar:(UITabBar *)theTabBar didSelectItem:(UIViewController *)viewController
{
    NSLog(@"Tab index = %@ ", theTabBar.selectedItem);
    for(int i = 0; i < theTabBar.items.count; i++)
    {
        if(theTabBar.selectedItem == theTabBar.items[i])
        {
            switch (i) {
                case 0:
                    [self.navigationItem setTitle:@"Capital Adequacy"];
                    break;
                case 1:
                    [self.navigationItem setTitle:@"Asset Quality"];
                    break;
                case 2:
                    [self.navigationItem setTitle:@"Earnings"];
                    break;
                case 3:
                    [self.navigationItem setTitle:@"Asset Liability Mgt"];
                    break;
                case 4: // custom "more" handling
                    [self.navigationItem setTitle:@"Productivity"];
                    break;
                default:
                    [self.navigationItem setTitle:@"Ratios"];
                    break;
            }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
