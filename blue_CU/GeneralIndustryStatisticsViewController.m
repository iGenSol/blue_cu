//
//  GeneralIndustryStatisticsViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 3/20/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "AppDelegate.h"
#import "GeneralIndustryStatisticsViewController.h"
#import "GeneralLoanStats+CoreDataClass.h"

@interface GeneralIndustryStatisticsViewController ()

@end

@implementation GeneralIndustryStatisticsViewController

#define debug 1



#pragma mark - VIEW
-(void)refreshInterface {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }

    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *generalStatsFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"GeneralLoanStats"];
    NSArray *fetchedGeneralStats = [cdh.context executeFetchRequest:generalStatsFetchRequest error:nil];
    
    for (GeneralLoanStats *generalStats in fetchedGeneralStats) {
        
        int zeroDecimals = 0;
        int oneDecimals = 1;
        int twoDecimals = 2;
        
        NSNumber *zeroDecimalNumber = @(zeroDecimals);
        NSNumber *oneDecimalNumber = @(oneDecimals);
        NSNumber *twoDecimalNumber = @(twoDecimals);
        
        NSNumber *federallyInsuredCUCountNSNumber = @(generalStats.federallyInsuredCUCount);
        self.federallyInsuredCUCountLabelField.text = [self convertNumberToFormattedStringForTextFields:federallyInsuredCUCountNSNumber];
        
        NSNumber *memberCountNSNumber = @(generalStats.memberCount);
        self.totalMembersLabelField.text = [self convertNumberToFormattedStringForTextFields:memberCountNSNumber];
        
        NSNumber *totalAssetsNSNumber = @(generalStats.totalAssets);
        NSString *returnedCurrencyStringForTotalAssets = [self convertNumberToCurrencyStringForLabelFields:totalAssetsNSNumber passedDecimalPlaces:twoDecimalNumber];
        self.totalAssetsLabelField.text = returnedCurrencyStringForTotalAssets;

        NSNumber *averageCUAssetsNSNumber = @(generalStats.averageCUAssets);
        NSString *returnedCurrencyStringForAverageCUAssets = [self convertNumberToCurrencyStringForLabelFields:averageCUAssetsNSNumber passedDecimalPlaces:zeroDecimalNumber];
        self.averageCUAssetsLabelField.text = returnedCurrencyStringForAverageCUAssets;

        NSNumber *returnOnAssetsNSNumber = @(generalStats.returnOnAverageAssets);
        self.returnOnAverageAssetsLabelField.text = [self convertNumberToFormattedStringForTextFields:returnOnAssetsNSNumber];
        
        NSNumber *totalInsuredSharesDepositsNSNumber = @(generalStats.totalInsuredSharesAndDeposits);
        NSString *returnedCurrencyStringForTotalInsuredSharesDeposits = [self convertNumberToCurrencyStringForLabelFields:totalInsuredSharesDepositsNSNumber passedDecimalPlaces:oneDecimalNumber];
        self.totalInsuredSharesDepositsLabelField.text = returnedCurrencyStringForTotalInsuredSharesDeposits;

        NSNumber *netIncomeNSNumber = @(generalStats.netIncome);
        NSString *returnedCurrencyStringForNetIncome = [self convertNumberToCurrencyStringForLabelFields:netIncomeNSNumber passedDecimalPlaces:oneDecimalNumber];
        self.netIncomeLabelField.text = returnedCurrencyStringForNetIncome;
  
        NSNumber *netWorthRatioNSNumber = @(generalStats.netWorthRatio);
        self.netWorthRatioLabelField.text = [self convertNumberToFormattedStringForTextFields:netWorthRatioNSNumber];
        
        NSNumber *averageSharesMemberNSNumber = @(generalStats.averageSharesMember);
        NSString *returnedCurrencyStringForAverageSharesMember = [self convertNumberToCurrencyStringForLabelFields:averageSharesMemberNSNumber passedDecimalPlaces:zeroDecimalNumber];
        self.averageSharesPerMemberLabelField.text = returnedCurrencyStringForAverageSharesMember;
        
      }
}

-(void)viewDidLoad {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [self refreshInterface];
}

-(void)viewDidDisappear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
}


#pragma mark - STRING CONVERSION METHOD
-(NSString*)convertNumberToFormattedStringForTextFields:(NSNumber*)passedNumber {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
    
    NSString *formattedString = [formatter stringFromNumber:passedNumber];
    
    return formattedString;

}

#pragma mark - STRING TO CURRENCY CONVERSION METHOD
-(NSString*)convertNumberToCurrencyStringForLabelFields:(NSNumber*) currencyNumber
                                                        passedDecimalPlaces :(NSNumber*) passedDecimalPlaces {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    NSInteger requiredDecimalPlaces = [passedDecimalPlaces integerValue];
    
    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSString *currencyGroupingSeparator = [[NSLocale currentLocale] objectForKey:NSLocaleGroupingSeparator];
    [currencyFormatter setGroupingSeparator:currencyGroupingSeparator];
    [currencyFormatter setGroupingSize:3];
    [currencyFormatter setAlwaysShowsDecimalSeparator:NO];
    [currencyFormatter setUsesGroupingSeparator:YES];
    [currencyFormatter setMaximumFractionDigits:requiredDecimalPlaces];
    
    NSString *formattedCurrencyString = [currencyFormatter stringFromNumber:currencyNumber];
    
    return formattedCurrencyString;
    
}



@end
