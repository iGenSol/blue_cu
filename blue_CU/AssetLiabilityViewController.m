//
//  AssetLiabilityViewController.m
//  blue_CU
//
//  Created by Timothy Milz on 1/14/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "AssetLiabilityViewController.h"
#import "AppDelegate.h"
#import "FS220+CoreDataClass.h"
#import "FS220A+CoreDataClass.h"
#import "FS220B+CoreDataClass.h"
#import "FS220H+CoreDataClass.h"
#import "RatioTabBarController.h"

@interface AssetLiabilityViewController ()

@end

@implementation AssetLiabilityViewController

#define debug 1

NSString *assetLiabilityDataCUNumber = @"";

NSString *assetLiabilityfs220_007 = @"";
NSString *assetLiabilityfs220_008 = @"";
NSString *assetLiabilityfs220_010 = @"";
NSString *assetLiabilityfs220_018 = @"";
NSString *assetLiabilityfs220_025B = @"";
NSString *assetLiabilityfs220_386 = @"";
NSString *assetLiabilityfs220_657 = @"";
NSString *assetLiabilityfs220_703 = @"";
NSString *assetLiabilityfs220_712 = @"";
NSString *assetLiabilityfs220_794 = @"";
NSString *assetLiabilityfs220_799A1 = @"";
NSString *assetLiabilityfs220_799C1 = @"";
NSString *assetLiabilityfs220_799C2 = @"";
NSString *assetLiabilityfs220_860C = @"";
NSString *assetLiabilityfs220_902 = @"";

NSString *assetLiabilityfs220a_718A =@"";
NSString *assetLiabilityfs220a_730A =@"";
NSString *assetLiabilityfs220a_730B =@"";
NSString *assetLiabilityfs220a_730C =@"";
NSString *assetLiabilityfs220a_799I =@"";
NSString *assetLiabilityfs220a_997 =@"";

NSString *assetLiabilityfs220b_781 =@"";
NSString *assetLiabilityfs220b_799D =@"";

NSString *assetLiabilityfs220h_400T =@"";
NSString *assetLiabilityfs220h_814E = @"";


#pragma mark - VIEW
-(void)refreshInterface {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    // Blank out in case no record found
    self.longTermAssetsTLAssetsLabelField.text = @"0.00";
    self.tlLoansTLSharesLabelField.text = @"0.00";
    self.cashInvAssetsLabelField.text = @"0.00";
    self.sharesTLSharesLabelField.text = @"0.00";
    self.superIntRateRiskNetWorthLabelField.text = @"0.00";
    self.sharesTLSharesBorrowLabelField.text = @"0.00";
    self.tlLoansTLAssetsLabelField.text = @"0.00";
    self.sharesDepBorrsAssetsLabelField.text = @"0.00";
    self.borrsSharesNetWorthLabelField.text = @"0.00";
    
    // Get credit union number for fetch from ExtendedInfoTabBarController
    RatioTabBarController *tabbar = (RatioTabBarController *)self.tabBarController;
    assetLiabilityDataCUNumber = [tabbar.receivedCUNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    // Get FS220 Data
    NSFetchRequest *fs220FetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220"];
    NSSortDescriptor *fs220SortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220FetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220SortDescriptor]];
    NSPredicate *fs220Filter = [NSPredicate predicateWithFormat:@"cu_number == %@", assetLiabilityDataCUNumber];
    [fs220FetchRequest setPredicate:fs220Filter];
    NSArray *fetchedFS220 = [cdh.context executeFetchRequest:fs220FetchRequest error:nil];
    
    for (FS220 *fs220 in fetchedFS220) {
        assetLiabilityfs220_007 = fs220.acct_007;
        assetLiabilityfs220_008 = fs220.acct_008;
        assetLiabilityfs220_010 = fs220.acct_010;
        assetLiabilityfs220_018 = fs220.acct_018;
        assetLiabilityfs220_025B = fs220.acct_025B;
        assetLiabilityfs220_386 = fs220.acct_386;
        assetLiabilityfs220_657 = fs220.acct_657;
        assetLiabilityfs220_703 = fs220.acct_703;
        assetLiabilityfs220_712 = fs220.acct_712;
        assetLiabilityfs220_794 = fs220.acct_794;
        assetLiabilityfs220_799A1 = fs220.acct_799A1;
        assetLiabilityfs220_799C1 = fs220.acct_799C1;
        assetLiabilityfs220_799C2 = fs220.acct_799C2;
        assetLiabilityfs220_860C = fs220.acct_860C;
        assetLiabilityfs220_902 = fs220.acct_902;
    }
    
    // Get FS220A Data
    NSFetchRequest *fs220aFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220A"];
    NSSortDescriptor *fs220aSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220aFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220aSortDescriptor]];
    NSPredicate *fs220aFilter = [NSPredicate predicateWithFormat:@"cu_number == %@", assetLiabilityDataCUNumber];
    [fs220aFetchRequest setPredicate:fs220aFilter];
    NSArray *fetchedFS220a = [cdh.context executeFetchRequest:fs220aFetchRequest error:nil];
    
    for (FS220A *fs220a in fetchedFS220a) {

        assetLiabilityfs220a_718A = fs220a.acct_718A;
        assetLiabilityfs220a_730A = fs220a.acct_730A;
        assetLiabilityfs220a_730B = fs220a.acct_730B;
        assetLiabilityfs220a_730C = fs220a.acct_730C;
        assetLiabilityfs220a_799I = fs220a.acct_799I;
        assetLiabilityfs220a_997 = fs220a.acct_997;
    }
    
    // Get FS220b Data
    NSFetchRequest *fs220bFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220B"];
    NSSortDescriptor *fs220bSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220bFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220bSortDescriptor]];
    NSPredicate *fs220bFilter = [NSPredicate predicateWithFormat:@"cu_number == %@", assetLiabilityDataCUNumber];
    [fs220bFetchRequest setPredicate:fs220bFilter];
    NSArray *fetchedFS220b = [cdh.context executeFetchRequest:fs220bFetchRequest error:nil];
    
    for (FS220B *fs220b in fetchedFS220b) {
        
        assetLiabilityfs220b_781 = fs220b.acct_781;
        assetLiabilityfs220b_799D = fs220b.acct_799D;
    }
    
    // Get FS220h Data
    NSFetchRequest *fs220hFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220H"];
    NSSortDescriptor *fs220hSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220hFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220hSortDescriptor]];
    NSPredicate *fs220hFilter = [NSPredicate predicateWithFormat:@"cu_number == %@", assetLiabilityDataCUNumber];
    [fs220hFetchRequest setPredicate:fs220hFilter];
    NSArray *fetchedFS220h = [cdh.context executeFetchRequest:fs220hFetchRequest error:nil];
    
    for (FS220H *fs220h in fetchedFS220h) {
     
        assetLiabilityfs220h_400T = fs220h.acct_400T;
        assetLiabilityfs220h_814E = fs220h.acct_814E;
    }
    
    [self calculateRatios];
}

-(void)viewDidLoad {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [self refreshInterface];
}

-(void)viewDidDisappear:(BOOL)animated {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
}

#pragma mark - STRING CONVERSION METHOD
-(NSNumber*)convertStringToNumberForTextFields:(NSString*)passedString {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    float convertedNumber = 0;
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    convertedNumber = [[numberFormatter numberFromString: passedString] floatValue];
    NSNumber *returnedNumber = [NSNumber numberWithFloat:convertedNumber];
    
    return returnedNumber;
}


#pragma mark - CALCULATE RATIOS
-(void)calculateRatios {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }

    // Setup variables
    NSNumber *returnedNumberForfs220_007 = [self convertStringToNumberForTextFields:assetLiabilityfs220_007];
    NSNumber *returnedNumberForfs220_008 = [self convertStringToNumberForTextFields:assetLiabilityfs220_008];
    NSNumber *returnedNumberForfs220_010 = [self convertStringToNumberForTextFields:assetLiabilityfs220_010];
    NSNumber *returnedNumberForfs220_018 = [self convertStringToNumberForTextFields:assetLiabilityfs220_018];
    NSNumber *returnedNumberForfs220_025B = [self convertStringToNumberForTextFields:assetLiabilityfs220_025B];
    NSNumber *returnedNumberForfs220_386 = [self convertStringToNumberForTextFields:assetLiabilityfs220_386];
    NSNumber *returnedNumberForfs220_657 = [self convertStringToNumberForTextFields:assetLiabilityfs220_657];
    NSNumber *returnedNumberForfs220_703 = [self convertStringToNumberForTextFields:assetLiabilityfs220_703];
    NSNumber *returnedNumberForfs220_712 = [self convertStringToNumberForTextFields:assetLiabilityfs220_712];
    NSNumber *returnedNumberForfs220_794 = [self convertStringToNumberForTextFields:assetLiabilityfs220_794];
    NSNumber *returnedNumberForfs220_799A1 = [self convertStringToNumberForTextFields:assetLiabilityfs220_799A1];
    NSNumber *returnedNumberForfs220_799C1 = [self convertStringToNumberForTextFields:assetLiabilityfs220_799C1];
    NSNumber *returnedNumberForfs220_799C2 = [self convertStringToNumberForTextFields:assetLiabilityfs220_799C2];
    NSNumber *returnedNumberForfs220_860C = [self convertStringToNumberForTextFields:assetLiabilityfs220_860C];
    NSNumber *returnedNumberForfs220_902 = [self convertStringToNumberForTextFields:assetLiabilityfs220_902];
    
    NSNumber *returnedNumberForfs220a_718A = [self convertStringToNumberForTextFields:assetLiabilityfs220a_718A];
    NSNumber *returnedNumberForfs220a_730A = [self convertStringToNumberForTextFields:assetLiabilityfs220a_730A];
    NSNumber *returnedNumberForfs220a_730B = [self convertStringToNumberForTextFields:assetLiabilityfs220a_730B];
    NSNumber *returnedNumberForfs220a_730C = [self convertStringToNumberForTextFields:assetLiabilityfs220a_730C];
    NSNumber *returnedNumberForfs220a_799I = [self convertStringToNumberForTextFields:assetLiabilityfs220a_799I];
    NSNumber *returnedNumberForfs220a_997 = [self convertStringToNumberForTextFields:assetLiabilityfs220a_997];
    
    NSNumber *returnedNumberForfs220b_781 = [self convertStringToNumberForTextFields:assetLiabilityfs220b_781];
    NSNumber *returnedNumberForfs220b_799D = [self convertStringToNumberForTextFields:assetLiabilityfs220b_799D];
    
    NSNumber *returnedNumberForfs220h_400T = [self convertStringToNumberForTextFields:assetLiabilityfs220h_400T];
    NSNumber *returnedNumberForfs220h_814E = [self convertStringToNumberForTextFields:assetLiabilityfs220h_814E];
    
    float f007 = [returnedNumberForfs220_007 floatValue];
    float f008 = [returnedNumberForfs220_008 floatValue];
    float f010 = [returnedNumberForfs220_010 floatValue];
    float f018 = [returnedNumberForfs220_018 floatValue];
    float f025B = [returnedNumberForfs220_025B floatValue];
    float f386 = [returnedNumberForfs220_386 floatValue];
    float f657 = [returnedNumberForfs220_657 floatValue];
    float f703 = [returnedNumberForfs220_703 floatValue];
    float f712 = [returnedNumberForfs220_712 floatValue];
    float f794 = [returnedNumberForfs220_794 floatValue];
    float f799A1 = [returnedNumberForfs220_799A1 floatValue];
    float f799C1 = [returnedNumberForfs220_799C1 floatValue];
    float f799C2 = [returnedNumberForfs220_799C2 floatValue];
    float f860C = [returnedNumberForfs220_860C floatValue];
    float f902 = [returnedNumberForfs220_902 floatValue];
    
    float f718A = [returnedNumberForfs220a_718A floatValue];
    float f730A = [returnedNumberForfs220a_730A floatValue];
    float f730B = [returnedNumberForfs220a_730B floatValue];
    float f730C = [returnedNumberForfs220a_730C floatValue];
    float f799I = [returnedNumberForfs220a_799I floatValue];
    float f997 = [returnedNumberForfs220a_997 floatValue];
    
    float f781 = [returnedNumberForfs220b_781 floatValue];
    float f799D = [returnedNumberForfs220b_799D floatValue];
    
    float f400T = [returnedNumberForfs220h_400T floatValue];
    float f814E = [returnedNumberForfs220h_814E floatValue];
    
    // Calculate Net Long Term Assets / Total Assets (display rounded)
    if (f010 != 0) {
        float longTermAssetsTLAssetsRatio = ((f703 + f386 - f712 + f400T - f814E + f799C1 + f799C2 + f799D + f007 + f008 - f718A + f794) / f010) * 100;
        NSString *formattedLongTermAssetsTLAssetsRatio = [NSString stringWithFormat:@"%.02f", longTermAssetsTLAssetsRatio];
        self.longTermAssetsTLAssetsLabelField.text = formattedLongTermAssetsTLAssetsRatio;
    } else {
        self.longTermAssetsTLAssetsLabelField.text = @"0.00";
    }
    
    // Calculate Total Loans / Total Shares (display rounded)
    if (f018 != 0) {
        float tLLoansTLSharesRatio = (f025B / f018) * 100;
        NSString* formattedTLLoansTLSharesRatio = [NSString stringWithFormat:@"%.02f", tLLoansTLSharesRatio];
        self.tlLoansTLSharesLabelField.text = formattedTLLoansTLSharesRatio;
    } else {
        self.tlLoansTLSharesLabelField.text = @"0.00";
    }
    
    // Calculate Cash Investments / Assets (display rounded)
    if (f010 != 0) {
        float cashInvAssetsRatio = ((f730A + f730B + f730C + f799A1) / f010) * 100;
        NSString* formattedCashInvAssetsRatio = [NSString stringWithFormat:@"%.02f", cashInvAssetsRatio];
        self.cashInvAssetsLabelField.text = formattedCashInvAssetsRatio;
    } else {
        self.cashInvAssetsLabelField.text = @"0.00";
    }
    
    // Calculate Regular Shares / Total Shares & Borrs (display rounded)
    if ((f018 + f860C - f781) != 0) {
        float regSharesTLSharesRatio = (f657 / (f018 + f860C - f781)) * 100;
        NSString* formattedRegSharesTLSharesRatio = [NSString stringWithFormat:@"%.02f", regSharesTLSharesRatio];
        self.sharesTLSharesBorrowLabelField.text = formattedRegSharesTLSharesRatio;
    } else {
        self.sharesTLSharesBorrowLabelField.text = @"0.00";
    }
    
    // Calculate Supervisory Int Rate Risk / Net Worth (display rounded)
    if (f997 != 0) {
        float superIntRateNetWorthRatio = ((f703 + f799C2 + f799D) / f997) * 100;
        NSString* formattedSuperIntRateNetWorthRatio = [NSString stringWithFormat:@"%.02f", superIntRateNetWorthRatio];
        self.superIntRateRiskNetWorthLabelField.text = formattedSuperIntRateNetWorthRatio;
    } else {
        self.superIntRateRiskNetWorthLabelField.text = @"0.00";
    }
    
    // Calculate Shares / TL Shares & Borrs (display rounded)
    if (f018 + f860C - f781 != 0) {
        float sharesTLSharesBorrsRatio = ((f902 + f657) / (f018 + f860C - f781)) * 100;
        NSString* formattedSharesTLSharesBorrsRatio = [NSString stringWithFormat:@"%.02f", sharesTLSharesBorrsRatio];
        self.sharesTLSharesLabelField.text = formattedSharesTLSharesBorrsRatio;
    } else {
        self.sharesTLSharesLabelField.text = @"0.00";
    }
    
    // Calculate Total Loans / Total Assets (display rounded)
    if (f010 != 0) {
        float tLLoansTLAssetsRatio = (f025B / f010) * 100;
        NSString* formattedTLLoansTLAssetsRatio = [NSString stringWithFormat:@"%.02f", tLLoansTLAssetsRatio];
        self.tlLoansTLAssetsLabelField.text = formattedTLLoansTLAssetsRatio;
    } else {
        self.tlLoansTLAssetsLabelField.text = @"0.00";
    }
    
    // Calculate Total Shares, Deposits, Borrs / Earning Assets (display rounded)
    if (f025B + f799I + f730B + f730C - f781 != 0) {
        float tlSharesDepBorrRatio = ((f018 + f860C - f781) / (f025B + f799I + f730B + f730C - f781)) * 100;
        NSString* formattedTLSharesDepBorrRatio = [NSString stringWithFormat:@"%.02f", tlSharesDepBorrRatio];
        self.sharesDepBorrsAssetsLabelField.text = formattedTLSharesDepBorrRatio;
    } else {
        self.sharesDepBorrsAssetsLabelField.text = @"0.00";
    }
    
    // Calculate Borrowings / TL Shares & Net Worth (display rounded)
    if (f018 + f997 != 0) {
        float borrsTLSharesNetWorthRatio = ((f860C - f781) / (f018 + f997)) * 100;
        NSString* formattedBorrsTLSharesNetWorthRatio = [NSString stringWithFormat:@"%.02f", borrsTLSharesNetWorthRatio];
        self.borrsSharesNetWorthLabelField.text = formattedBorrsTLSharesNetWorthRatio;
    } else {
        self.borrsSharesNetWorthLabelField.text = @"0.00";
    }
}


@end
