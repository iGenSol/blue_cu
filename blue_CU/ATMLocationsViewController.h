//
//  ATMLocationsViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 11/10/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataHelper.h"

@interface ATMLocationsViewController : UIViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSManagedObjectID *selectedATMID;
@property (strong, nonatomic) IBOutlet UILabel *site_NameLabelField;
@property (strong, nonatomic) IBOutlet UILabel *site_type_nameLabelField;
@property (strong, nonatomic) IBOutlet UILabel *phys_adr_1LabelField;
@property (strong, nonatomic) IBOutlet UILabel *phy_adr_2LabelField;
@property (strong, nonatomic) IBOutlet UILabel *phys_adr_cityLabelField;
@property (strong, nonatomic) IBOutlet UILabel *phys_adr_countyLabelField;
@property (strong, nonatomic) IBOutlet UILabel *phys_adr_state_codeLabelField;
@property (strong, nonatomic) IBOutlet UILabel *phys_adr_postal_codeLabelField;

@end
