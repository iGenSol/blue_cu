//
//  AssetQualityViewController.h
//  blue_CU
//
//  Created by Timothy Milz on 1/13/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssetQualityViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *DelinqLoansTotalLoansLabelField;
@property (strong, nonatomic) IBOutlet UILabel *netChargeOffsAvgLoansLabelField;
@property (strong, nonatomic) IBOutlet UILabel *fairMarketBookValueLabelField;
@property (strong, nonatomic) IBOutlet UILabel *AccumulatedGainLossLabelField;
@property (strong, nonatomic) IBOutlet UILabel *DelinqLoansAssetsLabelField;

@end
