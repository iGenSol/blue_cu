//
//  FS220A+CoreDataProperties.m
//  blue_CU
//
//  Created by Timothy Milz on 2/10/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "FS220A+CoreDataProperties.h"

@implementation FS220A (CoreDataProperties)

+ (NSFetchRequest<FS220A *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"FS220A"];
}

@dynamic acct_110;
@dynamic acct_115;
@dynamic acct_119;
@dynamic acct_120;
@dynamic acct_124;
@dynamic acct_131;
@dynamic acct_350;
@dynamic acct_381;
@dynamic acct_661A;
@dynamic acct_730B;
@dynamic acct_730C;
@dynamic acct_798A;
@dynamic acct_799I;
@dynamic acct_820A;
@dynamic acct_925A;
@dynamic acct_997;
@dynamic cu_number;
@dynamic acct_718A;
@dynamic acct_730A;
@dynamic acct_564A;
@dynamic acct_564B;
@dynamic acct_210;

@end
