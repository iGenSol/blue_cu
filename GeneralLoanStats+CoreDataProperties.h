//
//  GeneralLoanStats+CoreDataProperties.h
//  blue_CU
//
//  Created by Timothy Milz on 3/20/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "GeneralLoanStats+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface GeneralLoanStats (CoreDataProperties)

+ (NSFetchRequest<GeneralLoanStats *> *)fetchRequest;

@property (nonatomic) int16_t federallyInsuredCUCount;
@property (nonatomic) float memberCount;
@property (nonatomic) float totalAssets;
@property (nonatomic) float averageCUAssets;
@property (nonatomic) float totalInsuredSharesAndDeposits;
@property (nonatomic) float returnOnAverageAssets;
@property (nonatomic) float netIncome;
@property (nonatomic) float netWorthRatio;
@property (nonatomic) float averageSharesMember;
@property (nonatomic) float totalLoans;
@property (nonatomic) float averageLoanBalance;
@property (nonatomic) float loanToShareRatio;
@property (nonatomic) float mortgagesRealEstate;
@property (nonatomic) float autoLoans;
@property (nonatomic) float unsecuredCreditCards;
@property (nonatomic) float other;
@property (nonatomic) float delinquencyRatio;

@end

NS_ASSUME_NONNULL_END
