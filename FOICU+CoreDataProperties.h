//
//  FOICU+CoreDataProperties.h
//  blue_CU
//
//  Created by Timothy Milz on 11/14/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import "FOICU+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface FOICU (CoreDataProperties)

+ (NSFetchRequest<FOICU *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *attention_of;
@property (nullable, nonatomic, copy) NSString *charter_state;
@property (nullable, nonatomic, copy) NSString *city;
@property (nullable, nonatomic, copy) NSString *cong_dist;
@property (nullable, nonatomic, copy) NSString *county_code;
@property (nullable, nonatomic, copy) NSString *cu_name;
@property (nullable, nonatomic, copy) NSString *cu_number;
@property (nullable, nonatomic, copy) NSString *cu_type;
@property (nullable, nonatomic, copy) NSString *cycle_date;
@property (nullable, nonatomic, copy) NSString *district;
@property (nullable, nonatomic, copy) NSString *issue_date;
@property (nullable, nonatomic, copy) NSString *join_number;
@property (nullable, nonatomic, copy) NSString *limited_inc;
@property (nonatomic) BOOL listed;
@property (nullable, nonatomic, copy) NSString *peer_group;
@property (nullable, nonatomic, copy) NSString *quarter_flag;
@property (nullable, nonatomic, copy) NSString *region;
@property (nullable, nonatomic, copy) NSString *rssd;
@property (nullable, nonatomic, copy) NSString *se;
@property (nullable, nonatomic, copy) NSString *smsa;
@property (nullable, nonatomic, copy) NSString *state;
@property (nullable, nonatomic, copy) NSString *state_code;
@property (nullable, nonatomic, copy) NSString *street;
@property (nullable, nonatomic, copy) NSString *tom_code;
@property (nullable, nonatomic, copy) NSString *year_opened;
@property (nullable, nonatomic, copy) NSString *zip_code;

@end

NS_ASSUME_NONNULL_END
