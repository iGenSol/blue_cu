//
//  FS220D+CoreDataProperties.m
//  blue_CU
//
//  Created by Timothy Milz on 2/18/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "FS220D+CoreDataProperties.h"

@implementation FS220D (CoreDataProperties)

+ (NSFetchRequest<FS220D *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"FS220D"];
}

@dynamic board_pres_first_name;
@dynamic board_pres_last_name;
@dynamic board_pres_middle_initial;
@dynamic bond_provider;
@dynamic ceo_first_name;
@dynamic ceo_last_name;
@dynamic ceo_middle_initial;
@dynamic certified_first_name;
@dynamic certified_last_name;
@dynamic certified_middle_initial;
@dynamic cu_number;
@dynamic eligible_african_american;
@dynamic eligible_asian_american;
@dynamic eligible_hispanic_american;
@dynamic eligible_minority_status;
@dynamic eligible_native_american;
@dynamic member_african_american;
@dynamic member_asian_american;
@dynamic member_hispanic_american;
@dynamic member_native_american;
@dynamic net_worth_new_CU;
@dynamic net_worth_not_new_CU;
@dynamic phone_extension;
@dynamic phone_number;
@dynamic preparer_first_name;
@dynamic preparer_last_name;
@dynamic preparer_middle_initial;
@dynamic website_address;

@end
