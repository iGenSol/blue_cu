//
//  FS220A_PYE+CoreDataProperties.m
//  blue_CU
//
//  Created by Timothy Milz on 2/10/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "FS220A_PYE+CoreDataProperties.h"

@implementation FS220A_PYE (CoreDataProperties)

+ (NSFetchRequest<FS220A_PYE *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"FS220A_PYE"];
}

@dynamic cu_number;
@dynamic acct_799I;
@dynamic acct_730B;
@dynamic acct_730C;
@dynamic acct_997;

@end
