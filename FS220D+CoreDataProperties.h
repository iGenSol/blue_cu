//
//  FS220D+CoreDataProperties.h
//  blue_CU
//
//  Created by Timothy Milz on 2/18/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "FS220D+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface FS220D (CoreDataProperties)

+ (NSFetchRequest<FS220D *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *board_pres_first_name;
@property (nullable, nonatomic, copy) NSString *board_pres_last_name;
@property (nullable, nonatomic, copy) NSString *board_pres_middle_initial;
@property (nullable, nonatomic, copy) NSString *bond_provider;
@property (nullable, nonatomic, copy) NSString *ceo_first_name;
@property (nullable, nonatomic, copy) NSString *ceo_last_name;
@property (nullable, nonatomic, copy) NSString *ceo_middle_initial;
@property (nullable, nonatomic, copy) NSString *certified_first_name;
@property (nullable, nonatomic, copy) NSString *certified_last_name;
@property (nullable, nonatomic, copy) NSString *certified_middle_initial;
@property (nullable, nonatomic, copy) NSString *cu_number;
@property (nullable, nonatomic, copy) NSString *eligible_african_american;
@property (nullable, nonatomic, copy) NSString *eligible_asian_american;
@property (nullable, nonatomic, copy) NSString *eligible_hispanic_american;
@property (nullable, nonatomic, copy) NSString *eligible_minority_status;
@property (nullable, nonatomic, copy) NSString *eligible_native_american;
@property (nullable, nonatomic, copy) NSString *member_african_american;
@property (nullable, nonatomic, copy) NSString *member_asian_american;
@property (nullable, nonatomic, copy) NSString *member_hispanic_american;
@property (nullable, nonatomic, copy) NSString *member_native_american;
@property (nullable, nonatomic, copy) NSString *net_worth_new_CU;
@property (nullable, nonatomic, copy) NSString *net_worth_not_new_CU;
@property (nullable, nonatomic, copy) NSString *phone_extension;
@property (nullable, nonatomic, copy) NSString *phone_number;
@property (nullable, nonatomic, copy) NSString *preparer_first_name;
@property (nullable, nonatomic, copy) NSString *preparer_last_name;
@property (nullable, nonatomic, copy) NSString *preparer_middle_initial;
@property (nullable, nonatomic, copy) NSString *website_address;

@end

NS_ASSUME_NONNULL_END
