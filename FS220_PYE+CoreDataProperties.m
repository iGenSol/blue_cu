//
//  FS220_PYE+CoreDataProperties.m
//  blue_CU
//
//  Created by Timothy Milz on 2/10/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "FS220_PYE+CoreDataProperties.h"

@implementation FS220_PYE (CoreDataProperties)

+ (NSFetchRequest<FS220_PYE *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"FS220_PYE"];
}

@dynamic cu_number;
@dynamic acct_025B;
@dynamic acct_010;
@dynamic acct_018;
@dynamic acct_083;

@end
