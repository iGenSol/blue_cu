//
//  FS220G+CoreDataProperties.h
//  blue_CU
//
//  Created by Timothy Milz on 2/10/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "FS220G+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface FS220G (CoreDataProperties)

+ (NSFetchRequest<FS220G *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *acct_440A;
@property (nullable, nonatomic, copy) NSString *acct_660A;
@property (nullable, nonatomic, copy) NSString *cu_number;

@end

NS_ASSUME_NONNULL_END
