//
//  FS220+CoreDataProperties.m
//  blue_CU
//
//  Created by Timothy Milz on 2/10/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "FS220+CoreDataProperties.h"

@implementation FS220 (CoreDataProperties)

+ (NSFetchRequest<FS220 *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"FS220"];
}

@dynamic acct_007;
@dynamic acct_008;
@dynamic acct_010;
@dynamic acct_018;
@dynamic acct_025B;
@dynamic acct_041B;
@dynamic acct_300;
@dynamic acct_340;
@dynamic acct_380;
@dynamic acct_550;
@dynamic acct_551;
@dynamic acct_668;
@dynamic acct_671;
@dynamic acct_719;
@dynamic acct_825;
@dynamic acct_860C;
@dynamic acct_980;
@dynamic cu_number;
@dynamic current_members;
@dynamic potential_members;
@dynamic acct_703;
@dynamic acct_386;
@dynamic acct_712;
@dynamic acct_799C1;
@dynamic acct_799C2;
@dynamic acct_794;
@dynamic acct_657;
@dynamic acct_799A1;
@dynamic acct_902;
@dynamic acct_083;
@dynamic acct_084;
@dynamic acct_025A;

@end
