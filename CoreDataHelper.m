//
//  CoreDataHelper.m
//  v2.0
//
//  Created by Tim Roadley on 09/09/13.
//  Copyright (c) 2013 Tim Roadley. All rights reserved.
//
//  This class is free to use in production applications for owners of "Learning Core Data for iOS" by Tim Roadley
//

#import "CoreDataHelper.h"
#import "CoreDataImporter.h"

@class UIViewController;

@implementation CoreDataHelper
#define debug 1

#pragma mark - FILES
//NSString *storeFilename = @"blue_CU.sqlite";
NSString *storeFilename = @"201712blue_CU.sqlite";


#pragma mark - PATHS
- (NSString *)applicationDocumentsDirectory {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class,NSStringFromSelector(_cmd));
    }
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES) lastObject];
}
- (NSURL *)applicationStoresDirectory {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    NSURL *storesDirectory =
    [[NSURL fileURLWithPath:[self applicationDocumentsDirectory]]
     URLByAppendingPathComponent:@"Stores"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:[storesDirectory path]]) {
        NSError *error = nil;
        if ([fileManager createDirectoryAtURL:storesDirectory
                  withIntermediateDirectories:YES
                                   attributes:nil
                                        error:&error]) {
            if (debug==1) {
                NSLog(@"Successfully created Stores directory");}
        }
        else {NSLog(@"FAILED to create Stores directory: %@", error);}
    }
    return storesDirectory;
}
- (NSURL *)storeURL {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    return [[self applicationStoresDirectory]
            URLByAppendingPathComponent:storeFilename];
}


#pragma mark - SETUP
- (id)init {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    self = [super init];
    
    if (!self) {return nil;}
    
    _model = [NSManagedObjectModel mergedModelFromBundles:nil];
    _coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:_model];
    
    _parentContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_parentContext performBlockAndWait:^{
        [_parentContext setPersistentStoreCoordinator:_coordinator];
        [_parentContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    }];
    
    _context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_context setParentContext:_parentContext];
    [_context setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    
    _importContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_importContext performBlockAndWait:^{
        [_importContext setParentContext:_context];
        [_importContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
        [_importContext setUndoManager:nil]; // the default on iOS
    }];
    
    _sourceCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:_model];
    _sourceContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_sourceContext performBlockAndWait:^{
        [_sourceContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
        [_sourceContext setParentContext:_context];
        [_sourceContext setUndoManager:nil]; // the default on iOS
    }];
    return self;
}

- (void)loadStore {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    if (_store) {return;} // Don’t load store if it’s already loaded
    
    BOOL useMigrationManager = NO;
    if (useMigrationManager &&
        [self isMigrationNecessaryForStore:[self storeURL]]) {
        [self performBackgroundManagedMigrationForStore:[self storeURL]];
    } else {
        NSDictionary *options =
        @{
          NSMigratePersistentStoresAutomaticallyOption:@YES
          ,NSInferMappingModelAutomaticallyOption:@YES
          ,NSSQLitePragmasOption: @{@"journal_mode": @"DELETE"} // Uncomment to disable WAL journal mode
          };
        NSError *error = nil;
        _store = [_coordinator addPersistentStoreWithType:NSSQLiteStoreType
                                            configuration:nil
                                                      URL:[self storeURL]
                                                  options:options
                                                    error:&error];
        if (!_store) {
            NSLog(@"Failed to add store. Error: %@", error);abort();
        }
        else         {NSLog(@"Successfully added store: %@", _store);}
    }
    
}
- (void)setupCoreData {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    [self setDefaultDataStoreAsInitialStore];
    [self loadStore];
    [self checkIfDefaultDataNeedsImporting];
}

#pragma mark - SAVING
- (void)saveContext {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    if ([_context hasChanges]) {
        NSError *error = nil;
        if ([_context save:&error]) {
            NSLog(@"_context SAVED changes to persistent store");
        } else {
            NSLog(@"Failed to save _context: %@", error);
            [self showValidationError:error];
        }
    } else {
        NSLog(@"SKIPPED _context save, there are no changes!");
    }
}

- (void)backgroundSaveContext {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    // First, save the child context in the foreground (fast, all in memory)
    [self saveContext];
    
    // Then, save the parent context.
    [_parentContext performBlock:^{
        if ([_parentContext hasChanges]) {
            NSError *error = nil;
            if ([_parentContext save:&error]) {
                NSLog(@"_parentContext SAVED changes to persistent store");
            }
            else {
                NSLog(@"_parentContext FAILED to save: %@", error);
                [self showValidationError:error];
            }
        }
        else {
            NSLog(@"_parentContext SKIPPED saving as there are no changes");
        }
    }];
    
}


#pragma mark - MIGRATION MANAGER
- (BOOL)isMigrationNecessaryForStore:(NSURL*)storeUrl {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    if (![[NSFileManager defaultManager] fileExistsAtPath:[self storeURL].path]) {
        if (debug==1) {NSLog(@"SKIPPED MIGRATION: Source database missing.");}
        return NO;
    }
    NSError *error = nil;
    NSDictionary *sourceMetadata =
    [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType
                                                               URL:storeUrl
                                                                options:nil
                                                                error:&error];
    
    NSManagedObjectModel *destinationModel = _coordinator.managedObjectModel;
    if ([destinationModel isConfiguration:nil
              compatibleWithStoreMetadata:sourceMetadata]) {
        if (debug==1) {
            NSLog(@"SKIPPED MIGRATION: Source is already compatible");}
        return NO;
    }
    return YES;
}
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    
    if ([keyPath isEqualToString:@"migrationProgress"]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            float progress =
            [[change objectForKey:NSKeyValueChangeNewKey] floatValue];
            self.migrationVC.progressView.progress = progress;
            int percentage = progress * 100;
            NSString *string =
            [NSString stringWithFormat:@"Migration Progress: %i%%",
             percentage];
            NSLog(@"%@",string);
            self.migrationVC.label.text = string;
        });
    }
}
- (BOOL)replaceStore:(NSURL*)old withStore:(NSURL*)new {
    
    BOOL success = NO;
    NSError *Error = nil;
    if ([[NSFileManager defaultManager]
         removeItemAtURL:old error:&Error]) {
        
        Error = nil;
        if ([[NSFileManager defaultManager]
             moveItemAtURL:new toURL:old error:&Error]) {
            success = YES;
        }
        else {
            if (debug==1) {NSLog(@"FAILED to re-home new store %@", Error);}
        }
    }
    else {
        if (debug==1) {
            NSLog(@"FAILED to remove old store %@: Error:%@", old, Error);
        }
    }
    return success;
}
- (BOOL)migrateStore:(NSURL*)sourceStore {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    BOOL success = NO;
    NSError *error = nil;
    
    // STEP 1 - Gather the Source, Destination and Mapping Model
    NSDictionary *sourceMetadata = [NSPersistentStoreCoordinator
                                    metadataForPersistentStoreOfType:NSSQLiteStoreType
                                    URL:sourceStore
                                    options:nil
                                    error:&error];
    
    NSManagedObjectModel *sourceModel =
    [NSManagedObjectModel mergedModelFromBundles:nil
                                forStoreMetadata:sourceMetadata];
    
    NSManagedObjectModel *destinModel = _model;
    
    NSMappingModel *mappingModel =
    [NSMappingModel mappingModelFromBundles:nil
                             forSourceModel:sourceModel
                           destinationModel:destinModel];
    
    // STEP 2 - Perform migration, assuming the mapping model isn't null
    if (mappingModel) {
        NSError *error = nil;
        NSMigrationManager *migrationManager =
        [[NSMigrationManager alloc] initWithSourceModel:sourceModel
                                       destinationModel:destinModel];
        [migrationManager addObserver:self
                           forKeyPath:@"migrationProgress"
                              options:NSKeyValueObservingOptionNew
                              context:NULL];
        
        NSURL *destinStore =
        [[self applicationStoresDirectory]
         URLByAppendingPathComponent:@"Temp.sqlite"];
        
        success =
        [migrationManager migrateStoreFromURL:sourceStore
                                         type:NSSQLiteStoreType options:nil
                             withMappingModel:mappingModel
                             toDestinationURL:destinStore
                              destinationType:NSSQLiteStoreType
                           destinationOptions:nil
                                        error:&error];
        if (success) {
            // STEP 3 - Replace the old store with the new migrated store
            if ([self replaceStore:sourceStore withStore:destinStore]) {
                if (debug==1) {
                    NSLog(@"SUCCESSFULLY MIGRATED %@ to the Current Model",
                          sourceStore.path);}
                [migrationManager removeObserver:self
                                      forKeyPath:@"migrationProgress"];
            }
        }
        else {
            if (debug==1) {NSLog(@"FAILED MIGRATION: %@",error);}
        }
    }
    else {
        if (debug==1) {NSLog(@"FAILED MIGRATION: Mapping Model is null");}
    }
    return YES; // indicates migration has finished, regardless of outcome
}
- (void)performBackgroundManagedMigrationForStore:(NSURL*)storeURL {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    // Show migration progress view preventing the user from using the app
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.migrationVC =
    [sb instantiateViewControllerWithIdentifier:@"migration"];
    UIApplication *sa = [UIApplication sharedApplication];
    UINavigationController *nc =
    (UINavigationController*)sa.keyWindow.rootViewController;
    [nc presentViewController:self.migrationVC animated:NO completion:nil];
    
    // Perform migration in the background, so it doesn't freeze the UI.
    // This way progress can be shown to the user
    dispatch_async(
                   dispatch_get_global_queue(
                                             DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                       BOOL done = [self migrateStore:storeURL];
                       if(done) {
                           // When migration finishes, add the newly migrated store
                           dispatch_async(dispatch_get_main_queue(), ^{
                               NSError *error = nil;
                               _store =
                               [_coordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                          configuration:nil
                                                                    URL:[self storeURL]
                                                                options:nil
                                                                  error:&error];
                               if (!_store) {
                                   NSLog(@"Failed to add a migrated store. Error: %@",
                                         error);abort();}
                               else {
                                   NSLog(@"Successfully added a migrated store: %@",
                                         _store);}
                               [self.migrationVC dismissViewControllerAnimated:NO
                                                                    completion:nil];
                               self.migrationVC = nil;
                           });
                       }
                   });
}

#pragma mark - VALIDATION ERROR HANDLING
- (void)showValidationError:(NSError *)anError {
    
    if (anError && [anError.domain isEqualToString:@"NSCocoaErrorDomain"]) {
        NSArray *errors = nil;  // holds all errors
        NSString *txt = @""; // the error message text of the alert
        
        // Populate array with error(s)
        if (anError.code == NSValidationMultipleErrorsError) {
            errors = [anError.userInfo objectForKey:NSDetailedErrorsKey];
        } else {
            errors = [NSArray arrayWithObject:anError];
        }
        // Display the error(s)
        if (errors && errors.count > 0) {
            // Build error message text based on errors
            for (NSError * error in errors) {
                NSString *entity =
                [[[error.userInfo objectForKey:@"NSValidationErrorObject"]entity]name];
                
                NSString *property =
                [error.userInfo objectForKey:@"NSValidationErrorKey"];
                
                switch (error.code) {
                    case NSValidationRelationshipDeniedDeleteError:
                        txt = [txt stringByAppendingFormat:
                               @"%@ delete was denied because there are associated %@\n(Error Code %li)\n\n", entity, property, (long)error.code];
                        break;
                    case NSValidationRelationshipLacksMinimumCountError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' relationship count is too small (Code %li).", property, (long)error.code];
                        break;
                    case NSValidationRelationshipExceedsMaximumCountError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' relationship count is too large (Code %li).", property, (long)error.code];
                        break;
                    case NSValidationMissingMandatoryPropertyError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' property is missing (Code %li).", property, (long)error.code];
                        break;
                    case NSValidationNumberTooSmallError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' number is too small (Code %li).", property, (long)error.code];
                        break;
                    case NSValidationNumberTooLargeError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' number is too large (Code %li).", property, (long)error.code];
                        break;
                    case NSValidationDateTooSoonError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' date is too soon (Code %li).", property, (long)error.code];
                        break;
                    case NSValidationDateTooLateError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' date is too late (Code %li).", property, (long)error.code];
                        break;
                    case NSValidationInvalidDateError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' date is invalid (Code %li).", property, (long)error.code];
                        break;
                    case NSValidationStringTooLongError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' text is too long (Code %li).", property, (long)error.code];
                        break;
                    case NSValidationStringTooShortError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' text is too short (Code %li).", property, (long)error.code];
                        break;
                    case NSValidationStringPatternMatchingError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' text doesn't match the specified pattern (Code %li).", property, (long)error.code];
                        break;
                    case NSManagedObjectValidationError:
                        txt = [txt stringByAppendingFormat:
                               @"generated validation error (Code %li)", (long)error.code];
                        break;
                        
                    default:
                        txt = [txt stringByAppendingFormat:
                               @"Unhandled error code %li in showValidationError method", (long)error.code];
                        break;
                }
            }
            // display error message txt message
            /*
            UIAlertController *validationAlert = [UIAlertController alertControllerWithTitle:@"Caliber Alert"
                                                                                  message:[NSString stringWithFormat:@"%@Please double-tap the home button and close this application by swiping the application screenshot upwards \n", txt]
                                                                           preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [validationAlert addAction:defaultAction];
            [self presentViewController:validationAlert animated:YES completion:nil];
            */
        }
    }
}

#pragma mark – DATA IMPORT
- (BOOL)isDefaultDataAlreadyImportedForStoreWithURL:(NSURL*)url
                                             ofType:(NSString*)type {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    NSError *error;
    NSDictionary *dictionary =
    [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:type
                                                               URL:url
                                                           options:nil
                                                             error:&error];
    if (error) {
        NSLog(@"Error reading persistent store metadata: %@",
              error.localizedDescription);
    }
    else {
        NSNumber *defaultDataAlreadyImported =
        [dictionary valueForKey:@"DefaultDataImported"];
        if (![defaultDataAlreadyImported boolValue]) {
            NSLog(@"Default Data has NOT already been imported");
            return NO;
        }
    }
    if (debug==1) {NSLog(@"Default Data HAS already been imported");}
    return YES;
}

- (void)checkIfDefaultDataNeedsImporting {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    if (![self isDefaultDataAlreadyImportedForStoreWithURL:[self storeURL]
                                                    ofType:NSSQLiteStoreType]) {
        
        NSLog(@"Default Data has NOT been imported. This will now occur");
        [self triggerDefaultDataImport];
        
        /*
        self.importAlertView =
        [[UIAlertView alloc] initWithTitle:@"Import Default Data?"
                                   message:@"If you've never used Grocery Dude before then some default data might help you understand how to use it. Tap 'Import' to import default data. Tap 'Cancel' to skip the import, especially if you've done this before on other devices."
                                  delegate:self
                         cancelButtonTitle:@"Cancel"
                         otherButtonTitles:@"Import", nil];
        [self.importAlertView show];
        */
    }
}

- (void)importFromXML:(NSURL*)url {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    self.parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    self.parser.delegate = self;
    
    NSLog(@"**** START PARSE OF %@", url.path);
    [self.parser parse];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"SomethingChanged" object:nil];
    NSLog(@"***** END PARSE OF %@", url.path);
}

- (void)setDefaultDataAsImportedForStore:(NSPersistentStore*)aStore {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    // get metadata dictionary
    NSMutableDictionary *dictionary =
    [NSMutableDictionary dictionaryWithDictionary:[[aStore metadata] copy]];
    
    if (debug==1) {
        NSLog(@"__Store Metadata BEFORE changes__ \n %@", dictionary);
    }
    
    // edit metadata dictionary
    [dictionary setObject:@YES forKey:@"DefaultDataImported"];
    
    // set metadata dictionary
    [self.coordinator setMetadata:dictionary forPersistentStore:aStore];
    
    if (debug==1) {NSLog(@"__Store Metadata AFTER changes__ \n %@", dictionary);}
    
}

- (void)setDefaultDataStoreAsInitialStore {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:self.storeURL.path]) {
        NSURL *defaultDataURL =
        [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                pathForResource:@"201706DefaultData" ofType:@"sqlite"]];
        NSError *error;
        if (![fileManager copyItemAtURL:defaultDataURL
                                  toURL:self.storeURL
                                  error:&error]) {
            NSLog(@"DefaultData.sqlite copy FAIL: %@",
                  error.localizedDescription);
        }
        else {
            NSLog(@"A copy of DefaultData.sqlite was set as the initial store for %@",
                  self.storeURL.path);
        }
    }
}

#pragma mark - DELEGATE: TRIGGER DEFAULT DATA IMPORT
- (void)triggerDefaultDataImport {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    NSLog(@"Default Data Import Approved by User");
    [_importContext performBlock:^{
        // XML Import
        [self importFromXML:[[NSBundle mainBundle]
                                     URLForResource:@"export" withExtension:@"xml"]];
        //[self importFromXML:[[NSBundle mainBundle]
                             //URLForResource:@"exportATM" withExtension:@"xml"]];
        
        [self backgroundSaveContext]; //save context to primary store
        [self cleanBlueCUFirstData]; //clean up imported records
    }];
    // Set the data as imported regardless of the user's decision
    [self setDefaultDataAsImportedForStore:_store];
}


#pragma mark - DELEGATE: UIAlertView
/*
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    if (alertView == self.importAlertView) {
        if (buttonIndex == 1) { // The ‘Import’ button on the importAlertView
            
            NSLog(@"Default Data Import Approved by User");
            [_importContext performBlock:^{
                // XML Import
                [self importFromXML:[[NSBundle mainBundle]
                                     URLForResource:@"DefaultData" withExtension:@"xml"]];
            }];
        } else {
            NSLog(@"Default Data Import Cancelled by User");
        }
        // Set the data as imported regardless of the user's decision
        [self setDefaultDataAsImportedForStore:_store];
    }
}
*/

#pragma mark - UNIQUE ATTRIBUTE SELECTION
- (NSDictionary*)selectedUniqueAttributes {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    NSMutableArray *entities   = [NSMutableArray new];
    NSMutableArray *attributes = [NSMutableArray new];
    
    // Select an attribute in each entity for uniqueness
    [entities addObject:@"FOICU"];[attributes addObject:@"cu_number"];
    
    [entities addObject:@"FS220"];[attributes addObject:@"cu_number"];
    [entities addObject:@"FS220A"];[attributes addObject:@"cu_number"];
    [entities addObject:@"FS220B"];[attributes addObject:@"cu_number"];
    [entities addObject:@"FS220D"];[attributes addObject:@"cu_number"];
    [entities addObject:@"FS220G"];[attributes addObject:@"cu_number"];
    [entities addObject:@"FS220H"];[attributes addObject:@"cu_number"];
    
    [entities addObject:@"FS220_PYE"];[attributes addObject:@"cu_number"];
    [entities addObject:@"FS220A_PYE"];[attributes addObject:@"cu_number"];
    [entities addObject:@"FS220B_PYE"];[attributes addObject:@"cu_number"];
    
    [entities addObject:@"ATMLocations"];[attributes addObject:@"unique_key"];
    [entities addObject:@"BranchLocations"];[attributes addObject:@"unique_key"];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:attributes
                                                           forKeys:entities];
    return dictionary;
}

#pragma mark - DELEGATE: NSXMLParser
- (void)parser:(NSXMLParser *)parser
parseErrorOccurred:(NSError *)parseError {
    if (debug==1) {
        NSLog(@"Parser Error: %@", parseError.localizedDescription);
    }
}
- (void)parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
    attributes:(NSDictionary *)attributeDict {
    
    [self.importContext performBlockAndWait:^{
        
        // STEP 2: Prepare the Core Data Importer
        CoreDataImporter *importer =
        [[CoreDataImporter alloc] initWithUniqueAttributes:
         [self selectedUniqueAttributes]];
        
        // STEP 1: Process the 'foicu' element in the XML file
        if ([elementName isEqualToString:@"foicu"]) {
            
            // STEP 3a: Insert a unique 'foicu' object
            NSManagedObject *foicu =
            [importer insertBasicObjectInTargetEntity:@"FOICU"
                                targetEntityAttribute:@"cu_number"
                                   sourceXMLAttribute:@"cu_number"
                                        attributeDict:attributeDict
                                              context:_importContext];
            
            // Step 4: Manually add extra attribute values
            NSString *cycle_date = [attributeDict objectForKey:@"cycle_date"];
            [foicu setValue:cycle_date forKey:@"cycle_date"];
            
            NSString *join_number = [attributeDict objectForKey:@"join_number"];
            [foicu setValue:join_number forKey:@"join_number"];
            
            NSString *rssd = [attributeDict objectForKey:@"rssd"];
            [foicu setValue:rssd forKey:@"rssd"];
            
            NSString *cu_type = [attributeDict objectForKey:@"cu_type"];
            [foicu setValue:cu_type forKey:@"cu_type"];

            NSString *cu_name = [attributeDict objectForKey:@"cu_name"];
            [foicu setValue:cu_name forKey:@"cu_name"];
            
            NSString *city = [attributeDict objectForKey:@"city"];
            [foicu setValue:city forKey:@"city"];
            
            NSString *state = [attributeDict objectForKey:@"state"];
            [foicu setValue:state forKey:@"state"];
            
            NSString *charter_state = [attributeDict objectForKey:@"charter_state"];
            [foicu setValue:charter_state forKey:@"charter_state"];
            
            NSString *state_code = [attributeDict objectForKey:@"state_code"];
            [foicu setValue:state_code forKey:@"state_code"];
            
            NSString *zip_code = [attributeDict objectForKey:@"zip_code"];
            [foicu setValue:zip_code forKey:@"zip_code"];
            
            NSString *county_code = [attributeDict objectForKey:@"county_code"];
            [foicu setValue:county_code forKey:@"county_code"];
            
            NSString *cong_dist = [attributeDict objectForKey:@"cong_dist"];
            [foicu setValue:cong_dist forKey:@"cong_dist"];
            
            NSString *smsa = [attributeDict objectForKey:@"smsa"];
            [foicu setValue:smsa forKey:@"smsa"];
            
            NSString *attention_of = [attributeDict objectForKey:@"attention_of"];
            [foicu setValue:attention_of forKey:@"attention_of"];
            
            NSString *street = [attributeDict objectForKey:@"street"];
            [foicu setValue:street forKey:@"street"];
            
            NSString *region = [attributeDict objectForKey:@"region"];
            [foicu setValue:region forKey:@"region"];
            
            NSString *se = [attributeDict objectForKey:@"se"];
            [foicu setValue:se forKey:@"se"];
            
            NSString *district = [attributeDict objectForKey:@"district"];
            [foicu setValue:district forKey:@"district"];
            
            NSString *year_opened = [attributeDict objectForKey:@"year_opened"];
            [foicu setValue:year_opened forKey:@"year_opened"];
            
            NSString *tom_code = [attributeDict objectForKey:@"tom_code"];
            [foicu setValue:tom_code forKey:@"tom_code"];
            
            NSString *limited_inc = [attributeDict objectForKey:@"limited_inc"];
            [foicu setValue:limited_inc forKey:@"limited_inc"];
            
            NSString *issue_date = [attributeDict objectForKey:@"issue_date"];
            [foicu setValue:issue_date forKey:@"issue_date"];
            
            NSString *peer_group = [attributeDict objectForKey:@"peer_group"];
            [foicu setValue:peer_group forKey:@"peer_group"];
            
            NSString *quarter_flag = [attributeDict objectForKey:@"quarter_flag"];
            [foicu setValue:quarter_flag forKey:@"quarter_flag"];
            
            [foicu setValue:@YES forKey:@"listed"];
            
            // STEP 6: Save new objects to the persistent store.
            [CoreDataImporter saveContext:_importContext];
            
            // STEP 7: Turn objects into faults to save memory
            [_importContext refreshObject:foicu mergeChanges:NO];
            
        } // End FOICU Parse
        
        // STEP 1: Process the 'fs220' element in the XML file
        if ([elementName isEqualToString:@"fs220"]) {
            
            // STEP 3a: Insert a unique 'fs220' object
            NSManagedObject *fs220 =
            [importer insertBasicObjectInTargetEntity:@"FS220"
                                targetEntityAttribute:@"cu_number"
                                   sourceXMLAttribute:@"cu_number"
                                        attributeDict:attributeDict
                                              context:_importContext];
            
            // Step 4: Manually add extra attribute values
            NSString *acct_007 = [attributeDict objectForKey:@"acct_007"];
            [fs220 setValue:acct_007 forKey:@"acct_007"];
            
            NSString *acct_008 = [attributeDict objectForKey:@"acct_008"];
            [fs220 setValue:acct_008 forKey:@"acct_008"];
            
            NSString *acct_010 = [attributeDict objectForKey:@"acct_010"];
            [fs220 setValue:acct_010 forKey:@"acct_010"];
            
            NSString *acct_018 = [attributeDict objectForKey:@"acct_018"];
            [fs220 setValue:acct_018 forKey:@"acct_018"];
            
            NSString *acct_025A = [attributeDict objectForKey:@"acct_025A"];
            [fs220 setValue:acct_025A forKey:@"acct_025A"];
 
            NSString *acct_025B = [attributeDict objectForKey:@"acct_025B"];
            [fs220 setValue:acct_025B forKey:@"acct_025B"];
            
            NSString *acct_041B = [attributeDict objectForKey:@"acct_041B"];
            [fs220 setValue:acct_041B forKey:@"acct_041B"];
            
            NSString *acct_083 = [attributeDict objectForKey:@"acct_083"];
            [fs220 setValue:acct_083 forKey:@"current_members"];
            [fs220 setValue:acct_083 forKey:@"acct_083"];
            
            NSString *acct_084 = [attributeDict objectForKey:@"acct_084"];
            [fs220 setValue:acct_084 forKey:@"potential_members"];
            [fs220 setValue:acct_084 forKey:@"acct_084"];
            
            NSString *acct_300 = [attributeDict objectForKey:@"acct_300"];
            [fs220 setValue:acct_300 forKey:@"acct_300"];

            NSString *acct_340 = [attributeDict objectForKey:@"acct_340"];
            [fs220 setValue:acct_340 forKey:@"acct_340"];

            NSString *acct_380 = [attributeDict objectForKey:@"acct_380"];
            [fs220 setValue:acct_380 forKey:@"acct_380"];
            
            NSString *acct_386 = [attributeDict objectForKey:@"acct_386"];
            [fs220 setValue:acct_386 forKey:@"acct_386"];
            
            NSString *acct_550 = [attributeDict objectForKey:@"acct_550"];
            [fs220 setValue:acct_550 forKey:@"acct_550"];
            
            NSString *acct_551 = [attributeDict objectForKey:@"acct_551"];
            [fs220 setValue:acct_551 forKey:@"acct_551"];
            
            NSString *acct_657 = [attributeDict objectForKey:@"acct_657"];
            [fs220 setValue:acct_657 forKey:@"acct_657"];
            
            NSString *acct_668 = [attributeDict objectForKey:@"acct_668"];
            [fs220 setValue:acct_668 forKey:@"acct_668"];
            
            NSString *acct_671 = [attributeDict objectForKey:@"acct_671"];
            [fs220 setValue:acct_671 forKey:@"acct_671"];
            
            NSString *acct_703 = [attributeDict objectForKey:@"acct_703"];
            [fs220 setValue:acct_703 forKey:@"acct_703"];
            
            NSString *acct_712 = [attributeDict objectForKey:@"acct_712"];
            [fs220 setValue:acct_712 forKey:@"acct_712"];
            
            NSString *acct_719 = [attributeDict objectForKey:@"acct_719"];
            [fs220 setValue:acct_719 forKey:@"acct_719"];
            
            NSString *acct_794 = [attributeDict objectForKey:@"acct_794"];
            [fs220 setValue:acct_794 forKey:@"acct_794"];
            
            NSString *acct_799A1 = [attributeDict objectForKey:@"acct_799A1"];
            [fs220 setValue:acct_799A1 forKey:@"acct_799A1"];
            
            NSString *acct_799C1 = [attributeDict objectForKey:@"acct_799C1"];
            [fs220 setValue:acct_799C1 forKey:@"acct_799C1"];
            
            NSString *acct_799C2 = [attributeDict objectForKey:@"acct_799C2"];
            [fs220 setValue:acct_799C2 forKey:@"acct_799C2"];
            
            NSString *acct_825 = [attributeDict objectForKey:@"acct_825"];
            [fs220 setValue:acct_825 forKey:@"acct_825"];
            
            NSString *acct_860C = [attributeDict objectForKey:@"acct_860C"];
            [fs220 setValue:acct_860C forKey:@"acct_860C"];
            
            NSString *acct_902 = [attributeDict objectForKey:@"acct_902"];
            [fs220 setValue:acct_902 forKey:@"acct_902"];
            
            NSString *acct_980 = [attributeDict objectForKey:@"acct_980"];
            [fs220 setValue:acct_980 forKey:@"acct_980"];
            
            // STEP 6: Save new objects to the persistent store.
            [CoreDataImporter saveContext:_importContext];
            
            // STEP 7: Turn objects into faults to save memory
            [_importContext refreshObject:fs220 mergeChanges:NO];
            
        } // End fs220 Parse
        
        // STEP 1: Process the 'fs220a' element in the XML file
        if ([elementName isEqualToString:@"fs220a"]) {
            
            // STEP 3a: Insert a unique 'fs220a' object
            NSManagedObject *fs220a =
            [importer insertBasicObjectInTargetEntity:@"FS220A"
                                targetEntityAttribute:@"cu_number"
                                   sourceXMLAttribute:@"cu_number"
                                        attributeDict:attributeDict
                                              context:_importContext];
            
            // Step 4: Manually add extra attribute values
            NSString *acct_110 = [attributeDict objectForKey:@"acct_110"];
            [fs220a setValue:acct_110 forKey:@"acct_110"];
            
            NSString *acct_115 = [attributeDict objectForKey:@"acct_115"];
            [fs220a setValue:acct_115 forKey:@"acct_115"];
            
            NSString *acct_119 = [attributeDict objectForKey:@"acct_119"];
            [fs220a setValue:acct_119 forKey:@"acct_119"];
            
            NSString *acct_120 = [attributeDict objectForKey:@"acct_120"];
            [fs220a setValue:acct_120 forKey:@"acct_120"];
            
            NSString *acct_131 = [attributeDict objectForKey:@"acct_131"];
            [fs220a setValue:acct_131 forKey:@"acct_131"];
            
            NSString *acct_210 = [attributeDict objectForKey:@"acct_210"];
            [fs220a setValue:acct_210 forKey:@"acct_210"];
            
            NSString *acct_350 = [attributeDict objectForKey:@"acct_350"];
            [fs220a setValue:acct_350 forKey:@"acct_350"];
            
            NSString *acct_381 = [attributeDict objectForKey:@"acct_381"];
            [fs220a setValue:acct_381 forKey:@"acct_381"];
            
            NSString *acct_564A = [attributeDict objectForKey:@"acct_564A"];
            [fs220a setValue:acct_564A forKey:@"acct_564A"];
            
            NSString *acct_564B = [attributeDict objectForKey:@"acct_564B"];
            [fs220a setValue:acct_564B forKey:@"acct_564B"];
            
            NSString *acct_661A = [attributeDict objectForKey:@"acct_661A"];
            [fs220a setValue:acct_661A forKey:@"acct_661A"];
            
            NSString *acct_718A = [attributeDict objectForKey:@"acct_718A"];
            [fs220a setValue:acct_718A forKey:@"acct_718A"];
            
            NSString *acct_730A = [attributeDict objectForKey:@"acct_730A"];
            [fs220a setValue:acct_730A forKey:@"acct_730A"];
            
            NSString *acct_730B = [attributeDict objectForKey:@"acct_730B"];
            [fs220a setValue:acct_730B forKey:@"acct_730B"];
            
            NSString *acct_730C = [attributeDict objectForKey:@"acct_730C"];
            [fs220a setValue:acct_730C forKey:@"acct_730C"];
            
            NSString *acct_798A = [attributeDict objectForKey:@"acct_798A"];
            [fs220a setValue:acct_798A forKey:@"acct_798A"];
            
            NSString *acct_799I = [attributeDict objectForKey:@"acct_799I"];
            [fs220a setValue:acct_799I forKey:@"acct_799I"];
            
            NSString *acct_820A = [attributeDict objectForKey:@"acct_820A"];
            [fs220a setValue:acct_820A forKey:@"acct_820A"];
            
            NSString *acct_925A = [attributeDict objectForKey:@"acct_925A"];
            [fs220a setValue:acct_925A forKey:@"acct_925A"];
            
            NSString *acct_997 = [attributeDict objectForKey:@"acct_997"];
            [fs220a setValue:acct_997 forKey:@"acct_997"];
            
            // STEP 6: Save new objects to the persistent store.
            [CoreDataImporter saveContext:_importContext];
            
            // STEP 7: Turn objects into faults to save memory
            [_importContext refreshObject:fs220a mergeChanges:NO];
            
        } // End fs220a Parse
        
        // STEP 1: Process the 'fs220b' element in the XML file
        if ([elementName isEqualToString:@"fs220b"]) {
            
            // STEP 3a: Insert a unique 'fs220b' object
            NSManagedObject *fs220b =
            [importer insertBasicObjectInTargetEntity:@"FS220B"
                                targetEntityAttribute:@"cu_number"
                                   sourceXMLAttribute:@"cu_number"
                                        attributeDict:attributeDict
                                              context:_importContext];
            
            // Step 4: Manually add extra attribute values
            NSString *acct_659 = [attributeDict objectForKey:@"acct_659"];
            [fs220b setValue:acct_659 forKey:@"acct_659"];
            
            NSString *acct_781 = [attributeDict objectForKey:@"acct_781"];
            [fs220b setValue:acct_781 forKey:@"acct_781"];
            
            NSString *acct_796E = [attributeDict objectForKey:@"acct_796E"];
            [fs220b setValue:acct_796E forKey:@"acct_796E"];
            
            NSString *acct_797E = [attributeDict objectForKey:@"acct_797E"];
            [fs220b setValue:acct_797E forKey:@"acct_797E"];
            
            NSString *acct_799D = [attributeDict objectForKey:@"acct_799D"];
            [fs220b setValue:acct_799D forKey:@"acct_799D"];
            
            NSString *acct_801 = [attributeDict objectForKey:@"acct_801"];
            [fs220b setValue:acct_801 forKey:@"acct_801"];
            
            NSString *acct_945 = [attributeDict objectForKey:@"acct_945"];
            [fs220b setValue:acct_945 forKey:@"acct_945"];
            
            // STEP 6: Save new objects to the persistent store.
            [CoreDataImporter saveContext:_importContext];
            
            // STEP 7: Turn objects into faults to save memory
            [_importContext refreshObject:fs220b mergeChanges:NO];
            
        } // End fs220b Parse
        
        // STEP 1: Process the 'fs220d' element in the XML file
        if ([elementName isEqualToString:@"fs220d"]) {
            
            // STEP 3a: Insert a unique 'fs220d' object
            NSManagedObject *fs220d =
            [importer insertBasicObjectInTargetEntity:@"FS220D"
                                targetEntityAttribute:@"cu_number"
                                   sourceXMLAttribute:@"cu_number"
                                        attributeDict:attributeDict
                                              context:_importContext];
            
            // Step 4: Manually add extra attribute values
            NSString *ceo_l = [attributeDict objectForKey:@"ceo_l"];
            [fs220d setValue:ceo_l forKey:@"ceo_last_name"];
            
            NSString *phone_number = [attributeDict objectForKey:@"phone_number"];
            [fs220d setValue:phone_number forKey:@"phone_number"];
            
            NSString *preparer_ln = [attributeDict objectForKey:@"preparer_ln"];
            [fs220d setValue:preparer_ln forKey:@"preparer_last_name"];
            
            NSString *president_l = [attributeDict objectForKey:@"president_l"];
            [fs220d setValue:president_l forKey:@"board_pres_last_name"];
            
            NSString *ceo_f = [attributeDict objectForKey:@"ceo_f"];
            [fs220d setValue:ceo_f forKey:@"ceo_first_name"];
            
            NSString *ceo_m = [attributeDict objectForKey:@"ceo_m"];
            [fs220d setValue:ceo_m forKey:@"ceo_middle_initial"];
            
            NSString *preparer_f = [attributeDict objectForKey:@"preparer_f"];
            [fs220d setValue:preparer_f forKey:@"preparer_first_name"];
            
            NSString *preparer_m = [attributeDict objectForKey:@"preparer_m"];
            [fs220d setValue:preparer_m forKey:@"preparer_middle_initial"];
            
            NSString *president_f = [attributeDict objectForKey:@"president_f"];
            [fs220d setValue:president_f forKey:@"board_pres_first_name"];
            
            NSString *president_m = [attributeDict objectForKey:@"president_m"];
            [fs220d setValue:president_m forKey:@"board_pres_middle_initial"];
            
            NSString *website = [attributeDict objectForKey:@"website"];
            [fs220d setValue:website forKey:@"website_address"];
            
            NSString *acct_700 = [attributeDict objectForKey:@"acct_700"];
            [fs220d setValue:acct_700 forKey:@"net_worth_not_new_CU"];
            
            NSString *acct_701 = [attributeDict objectForKey:@"acct_701"];
            [fs220d setValue:acct_701 forKey:@"net_worth_new_CU"];
            
            NSString *certified_l = [attributeDict objectForKey:@"certified_l"];
            [fs220d setValue:certified_l forKey:@"certified_last_name"];
            
            NSString *certified_f = [attributeDict objectForKey:@"certified_f"];
            [fs220d setValue:certified_f forKey:@"certified_first_name"];
            
            NSString *certified_m = [attributeDict objectForKey:@"certified_m"];
            [fs220d setValue:certified_m forKey:@"certified_middle_initial"];
            
            NSString *extension = [attributeDict objectForKey:@"extension"];
            [fs220d setValue:extension forKey:@"phone_extension"];
            
            NSString *bond_provider = [attributeDict objectForKey:@"bond_provider"];
            [fs220d setValue:bond_provider forKey:@"bond_provider"];
            
            NSString *eligible_minority_status = [attributeDict objectForKey:@"eligible_minority_status"];
            [fs220d setValue:eligible_minority_status forKey:@"eligible_minority_status"];
            
            NSString *member_asian_american = [attributeDict objectForKey:@"member_asian_american"];
            [fs220d setValue:member_asian_american forKey:@"member_asian_american"];
            
            NSString *member_african_american = [attributeDict objectForKey:@"member_african_american"];
            [fs220d setValue:member_african_american forKey:@"member_african_american"];
            
            NSString *member_hispanic_american = [attributeDict objectForKey:@"member_hispanic_american"];
            [fs220d setValue:member_hispanic_american forKey:@"member_hispanic_american"];
            
            NSString *member_native_american = [attributeDict objectForKey:@"member_native_american"];
            [fs220d setValue:member_native_american forKey:@"member_native_american"];
            
            NSString *eligible_asian_american = [attributeDict objectForKey:@"eligible_asian_american"];
            [fs220d setValue:eligible_asian_american forKey:@"eligible_asian_american"];
            
            NSString *eligible_african_american = [attributeDict objectForKey:@"eligible_african_american"];
            [fs220d setValue:eligible_african_american forKey:@"eligible_african_american"];
            
            NSString *eligible_hispanic_american = [attributeDict objectForKey:@"eligible_hispanic_american"];
            [fs220d setValue:eligible_hispanic_american forKey:@"eligible_hispanic_american"];
            
            NSString *eligible_native_american = [attributeDict objectForKey:@"eligible_native_american"];
            [fs220d setValue:eligible_native_american forKey:@"eligible_native_american"];
            
            // STEP 6: Save new objects to the persistent store.
            [CoreDataImporter saveContext:_importContext];
            
            // STEP 7: Turn objects into faults to save memory
            [_importContext refreshObject:fs220d mergeChanges:NO];
            
        } // End fs220d Parse
        
        // STEP 1: Process the 'fs220g' element in the XML file
        if ([elementName isEqualToString:@"fs220g"]) {
            
            // STEP 3a: Insert a unique 'fs220g' object
            NSManagedObject *fs220g =
            [importer insertBasicObjectInTargetEntity:@"FS220G"
                                targetEntityAttribute:@"cu_number"
                                   sourceXMLAttribute:@"cu_number"
                                        attributeDict:attributeDict
                                              context:_importContext];
            
            // Step 4: Manually add extra attribute values
            NSString *acct_440A = [attributeDict objectForKey:@"acct_440A"];
            [fs220g setValue:acct_440A forKey:@"acct_440A"];

            NSString *acct_660A = [attributeDict objectForKey:@"acct_660A"];
            [fs220g setValue:acct_660A forKey:@"acct_660A"];
            
            // STEP 6: Save new objects to the persistent store.
            [CoreDataImporter saveContext:_importContext];
            
            // STEP 7: Turn objects into faults to save memory
            [_importContext refreshObject:fs220g mergeChanges:NO];
            
        } // End fs220g Parse

        // STEP 1: Process the 'fs220h' element in the XML file
        if ([elementName isEqualToString:@"fs220h"]) {
            
            // STEP 3a: Insert a unique 'fs220h' object
            NSManagedObject *fs220h =
            [importer insertBasicObjectInTargetEntity:@"FS220H"
                                targetEntityAttribute:@"cu_number"
                                   sourceXMLAttribute:@"cu_number"
                                        attributeDict:attributeDict
                                              context:_importContext];
            
            // Step 4: Manually add extra attribute values
            NSString *acct_400T = [attributeDict objectForKey:@"acct_400T"];
            [fs220h setValue:acct_400T forKey:@"acct_400T"];
            
            NSString *acct_814E = [attributeDict objectForKey:@"acct_814E"];
            [fs220h setValue:acct_814E forKey:@"acct_814E"];
            
            // STEP 6: Save new objects to the persistent store.
            [CoreDataImporter saveContext:_importContext];
            
            // STEP 7: Turn objects into faults to save memory
            [_importContext refreshObject:fs220h mergeChanges:NO];
            
        } // End fs220h Parse
        
        // STEP 1: Process the 'fs220PYE' element in the XML file
        if ([elementName isEqualToString:@"fs220PYE"]) {
            
            // STEP 3a: Insert a unique 'fs220PYE' object
            NSManagedObject *fs220PYE =
            [importer insertBasicObjectInTargetEntity:@"FS220_PYE"
                                targetEntityAttribute:@"cu_number"
                                   sourceXMLAttribute:@"cu_number"
                                        attributeDict:attributeDict
                                              context:_importContext];
            
            // Step 4: Manually add extra attribute values
            NSString *acct_fs220PYE_010 = [attributeDict objectForKey:@"acct_010_fs220PYE"];
            [fs220PYE setValue:acct_fs220PYE_010 forKey:@"acct_010"];
            
            NSString *acct_fs220PYE_018 = [attributeDict objectForKey:@"acct_018_fs220PYE"];
            [fs220PYE setValue:acct_fs220PYE_018 forKey:@"acct_018"];
            
            NSString *acct_fs220PYE_025B = [attributeDict objectForKey:@"acct_025B_fs220PYE"];
            [fs220PYE setValue:acct_fs220PYE_025B forKey:@"acct_025B"];
            
            NSString *acct_fs220PYE_083 = [attributeDict objectForKey:@"acct_083_fs220PYE"];
            [fs220PYE setValue:acct_fs220PYE_083 forKey:@"acct_083"];
            
            // STEP 6: Save new objects to the persistent store.
            [CoreDataImporter saveContext:_importContext];
            
            // STEP 7: Turn objects into faults to save memory
            [_importContext refreshObject:fs220PYE mergeChanges:NO];
            
        } // End fs220PYE Parse 

        // STEP 1: Process the 'fs220aPYE' element in the XML file
        if ([elementName isEqualToString:@"fs220aPYE"]) {
            
            // STEP 3a: Insert a unique 'fs220aPYE' object
            NSManagedObject *fs220aPYE =
            [importer insertBasicObjectInTargetEntity:@"FS220A_PYE"
                                targetEntityAttribute:@"cu_number"
                                   sourceXMLAttribute:@"cu_number"
                                        attributeDict:attributeDict
                                              context:_importContext];
            
            // Step 4: Manually add extra attribute values
            NSString *acct_fs220aPYE_730B = [attributeDict objectForKey:@"acct_730B_fs220aPYE"];
            [fs220aPYE setValue:acct_fs220aPYE_730B forKey:@"acct_730B"];
            
            NSString *acct_fs220aPYE_730C = [attributeDict objectForKey:@"acct_730C_fs220aPYE"];
            [fs220aPYE setValue:acct_fs220aPYE_730C forKey:@"acct_730C"];
            
            NSString *acct_fs220aPYE_799I = [attributeDict objectForKey:@"acct_799I_fs220aPYE"];
            [fs220aPYE setValue:acct_fs220aPYE_799I forKey:@"acct_799I"];
            
            NSString *acct_fs220aPYE_997 = [attributeDict objectForKey:@"acct_997_fs220aPYE"];
            [fs220aPYE setValue:acct_fs220aPYE_997 forKey:@"acct_997"];
            
            // STEP 6: Save new objects to the persistent store.
            [CoreDataImporter saveContext:_importContext];
            
            // STEP 7: Turn objects into faults to save memory
            [_importContext refreshObject:fs220aPYE mergeChanges:NO];
            
        } // End fs220aPYE Parse

        // STEP 1: Process the 'fs220bPYE' element in the XML file
        if ([elementName isEqualToString:@"fs220bPYE"]) {
            
            // STEP 3a: Insert a unique 'fs220bPYE' object
            NSManagedObject *fs220bPYE =
            [importer insertBasicObjectInTargetEntity:@"FS220B_PYE"
                                targetEntityAttribute:@"cu_number"
                                   sourceXMLAttribute:@"cu_number"
                                        attributeDict:attributeDict
                                              context:_importContext];
            
            // Step 4: Manually add extra attribute values
            NSString *acct_fs220bPYE_781 = [attributeDict objectForKey:@"acct_781_fs220bPYE"];
            [fs220bPYE setValue:acct_fs220bPYE_781 forKey:@"acct_781"];
            
            // STEP 6: Save new objects to the persistent store.
            [CoreDataImporter saveContext:_importContext];
            
            // STEP 7: Turn objects into faults to save memory
            [_importContext refreshObject:fs220bPYE mergeChanges:NO];
            
        } // End fs220bPYE Parse

        // STEP 1: Process the 'atm' element in the XML file
        if ([elementName isEqualToString:@"atm"]) {
            
            // STEP 3a: Insert a unique 'atm' object
            NSManagedObject *atmObj =
            [importer insertBasicObjectInTargetEntity:@"ATMLocations"
                                targetEntityAttribute:@"unique_key"
                                   sourceXMLAttribute:@"unique_key"
                                        attributeDict:attributeDict
                                              context:_importContext];
            
            // Step 4: Manually add extra attribute values
            NSString *cu_number = [attributeDict objectForKey:@"cu_number"];
            cu_number = [cu_number stringByReplacingOccurrencesOfString:@" " withString:@""];
            [atmObj setValue:cu_number forKey:@"cu_number"];
            
            NSString *cycle_date = [attributeDict objectForKey:@"cycle_date"];
            [atmObj setValue:cycle_date forKey:@"cycle_date"];
            
            NSString *join_number = [attributeDict objectForKey:@"join_number"];
            [atmObj setValue:join_number forKey:@"join_number"];
            
            NSString *cu_name = [attributeDict objectForKey:@"cu_name"];
            [atmObj setValue:cu_name forKey:@"cu_name"];
            
            NSString *site_name = [attributeDict objectForKey:@"site_name"];
            [atmObj setValue:site_name forKey:@"site_name"];
            
            NSString *site_ID = [attributeDict objectForKey:@"site_ID"];
            [atmObj setValue:site_ID forKey:@"siteID"];
            
            NSString *site_type_name = [attributeDict objectForKey:@"site_type_name"];
            [atmObj setValue:site_type_name forKey:@"site_type_name"];
            
            NSString *site_function_name = [attributeDict objectForKey:@"site_function_name"];
            [atmObj setValue:site_function_name forKey:@"site_function_name"];
            
            NSString *phys_adr_1 = [attributeDict objectForKey:@"phys_adr_1"];
            [atmObj setValue:phys_adr_1 forKey:@"phys_adr_1"];
            
            NSString *phys_adr_2 = [attributeDict objectForKey:@"phys_adr_2"];
            [atmObj setValue:phys_adr_2 forKey:@"phys_adr_2"];
            
            NSString *phys_adr_city = [attributeDict objectForKey:@"phys_adr_city"];
            [atmObj setValue:phys_adr_city forKey:@"phys_adr_city"];
            
            NSString *phys_adr_state_code = [attributeDict objectForKey:@"phys_adr_state_code"];
            [atmObj setValue:phys_adr_state_code forKey:@"phys_adr_state_code"];
            
            NSString *phys_adr_state_name = [attributeDict objectForKey:@"phys_adr_state_name"];
            [atmObj setValue:phys_adr_state_name forKey:@"phys_adr_state_name"];
            
            NSString *phys_adr_postal_code = [attributeDict objectForKey:@"phys_adr_postal_code"];
            [atmObj setValue:phys_adr_postal_code forKey:@"phys_adr_postal_code"];
            
            NSString *phys_adr_county_name = [attributeDict objectForKey:@"phys_adr_county_name"];
            [atmObj setValue:phys_adr_county_name forKey:@"phys_adr_county_name"];
            
            NSString *phys_adr_country = [attributeDict objectForKey:@"phys_adr_country"];
            [atmObj setValue:phys_adr_country forKey:@"phys_adr_country"];
            
            // STEP 6: Save new objects to the persistent store.
            [CoreDataImporter saveContext:_importContext];
            
            // STEP 7: Turn objects into faults to save memory
            [_importContext refreshObject:atmObj mergeChanges:NO];
            
        } // End ATM Parse
        
        // STEP 1: Process the 'Branch' element in the XML file
        if ([elementName isEqualToString:@"branch"]) {
            
            // STEP 3a: Insert a unique 'branch' object
            NSManagedObject *branchObj =
            [importer insertBasicObjectInTargetEntity:@"BranchLocations"
                                targetEntityAttribute:@"unique_key"
                                   sourceXMLAttribute:@"unique_key"
                                        attributeDict:attributeDict
                                              context:_importContext];
            
            // Step 4: Manually add extra attribute values
            NSString *cu_number = [attributeDict objectForKey:@"cu_number"];
            cu_number = [cu_number stringByReplacingOccurrencesOfString:@" " withString:@""];
            [branchObj setValue:cu_number forKey:@"cu_number"];
            
            NSString *cycle_date = [attributeDict objectForKey:@"cycle_date"];
            [branchObj setValue:cycle_date forKey:@"cycle_date"];
            
            NSString *join_number = [attributeDict objectForKey:@"join_number"];
            [branchObj setValue:join_number forKey:@"join_number"];
            
            NSString *siteID = [attributeDict objectForKey:@"siteID"];
            [branchObj setValue:siteID forKey:@"siteID"];
            
            NSString *cu_name = [attributeDict objectForKey:@"cu_name"];
            [branchObj setValue:cu_name forKey:@"cu_name"];
            
            NSString *site_name = [attributeDict objectForKey:@"site_name"];
            [branchObj setValue:site_name forKey:@"site_name"];
            
            NSString *site_type_name = [attributeDict objectForKey:@"site_type_name"];
            [branchObj setValue:site_type_name forKey:@"site_type_name"];
            
            NSString *main_office = [attributeDict objectForKey:@"main_office"];
            [branchObj setValue:main_office forKey:@"main_office"];
            
            NSString *phys_adr_1 = [attributeDict objectForKey:@"phys_adr_1"];
            [branchObj setValue:phys_adr_1 forKey:@"phys_adr_1"];
            
            NSString *phys_adr_2 = [attributeDict objectForKey:@"phys_adr_2"];
            [branchObj setValue:phys_adr_2 forKey:@"phys_adr_2"];
            
            NSString *phys_adr_city = [attributeDict objectForKey:@"phys_adr_city"];
            [branchObj setValue:phys_adr_city forKey:@"phys_adr_city"];
            
            NSString *phys_adr_state_code = [attributeDict objectForKey:@"phys_adr_state_code"];
            [branchObj setValue:phys_adr_state_code forKey:@"phys_adr_state_code"];
            
            NSString *phys_adr_postal_code = [attributeDict objectForKey:@"phys_adr_postal_code"];
            [branchObj setValue:phys_adr_postal_code forKey:@"phys_adr_postal_code"];
            
            NSString *phys_adr_county_name = [attributeDict objectForKey:@"phys_adr_county_name"];
            [branchObj setValue:phys_adr_county_name forKey:@"phys_adr_county_name"];
            
            NSString *phys_adr_country = [attributeDict objectForKey:@"phys_adr_country"];
            [branchObj setValue:phys_adr_country forKey:@"phys_adr_country"];
            
            NSString *mailing_adr_1 = [attributeDict objectForKey:@"mailing_adr_1"];
            [branchObj setValue:mailing_adr_1 forKey:@"mailing_adr_1"];
            
            NSString *mailing_adr_2 = [attributeDict objectForKey:@"mailing_adr_2"];
            [branchObj setValue:mailing_adr_2 forKey:@"mailing_adr_2"];
            
            NSString *mailing_adr_city = [attributeDict objectForKey:@"mailing_adr_city"];
            [branchObj setValue:mailing_adr_city forKey:@"mailing_adr_city"];
            
            NSString *mailing_adr_state_code = [attributeDict objectForKey:@"mailing_adr_state_code"];
            [branchObj setValue:mailing_adr_state_code forKey:@"mailing_adr_state_code"];
            
            NSString *mailing_adr_state_name = [attributeDict objectForKey:@"mailing_adr_state_name"];
            [branchObj setValue:mailing_adr_state_name forKey:@"mailing_adr_state_name"];
            
            NSString *mailing_adr_postal_code = [attributeDict objectForKey:@"mailing_adr_postal_code"];
            [branchObj setValue:mailing_adr_postal_code forKey:@"mailing_adr_postal_code"];

            NSString *phone_number = [attributeDict objectForKey:@"phone_number"];
            [branchObj setValue:phone_number forKey:@"phone_number"];
            
            // STEP 6: Save new objects to the persistent store.
            [CoreDataImporter saveContext:_importContext];
            
            // STEP 7: Turn objects into faults to save memory
            [_importContext refreshObject:branchObj mergeChanges:NO];
            
        } // End Branch Parse

        }];
 
        /*
        // STEP 1: Process only the 'item' element in the XML file
        if ([elementName isEqualToString:@"item"]) {
            
            // STEP 2: Prepare the Core Data Importer
            CoreDataImporter *importer =
            [[CoreDataImporter alloc] initWithUniqueAttributes:
             [self selectedUniqueAttributes]];
            
            // STEP 3a: Insert a unique 'Item' object
            NSManagedObject *item =
            [importer insertBasicObjectInTargetEntity:@"Item"
                                targetEntityAttribute:@"name"
                                   sourceXMLAttribute:@"name"
                                        attributeDict:attributeDict
                                              context:_importContext];
            
            // STEP 3b: Insert a unique 'Unit' object
            NSManagedObject *unit =
            [importer insertBasicObjectInTargetEntity:@"Unit"
                                targetEntityAttribute:@"name"
                                   sourceXMLAttribute:@"unit"
                                        attributeDict:attributeDict
                                              context:_importContext];
            
            // STEP 3c: Insert a unique 'LocationAtHome' object
            NSManagedObject *locationAtHome =
            [importer insertBasicObjectInTargetEntity:@"LocationAtHome"
                                targetEntityAttribute:@"storedIn"
                                   sourceXMLAttribute:@"locationathome"
                                        attributeDict:attributeDict
                                              context:_importContext];
            
            // STEP 3d: Insert a unique 'LocationAtShop' object
            NSManagedObject *locationAtShop =
            [importer insertBasicObjectInTargetEntity:@"LocationAtShop"
                                targetEntityAttribute:@"aisle"
                                   sourceXMLAttribute:@"locationatshop"
                                        attributeDict:attributeDict
                                              context:_importContext];
            
            // STEP 4: Manually add extra attribute values.
            [item setValue:@NO forKey:@"listed"];
            
            // STEP 5: Create relationships
            [item setValue:unit forKey:@"unit"];
            [item setValue:locationAtHome forKey:@"locationAtHome"];
            [item setValue:locationAtShop forKey:@"locationAtShop"];
            
            // STEP 6: Save new objects to the persistent store.
            [CoreDataImporter saveContext:_importContext];
            
            // STEP 7: Turn objects into faults to save memory
            [_importContext refreshObject:item mergeChanges:NO];
            [_importContext refreshObject:unit mergeChanges:NO];
            [_importContext refreshObject:locationAtHome mergeChanges:NO];
            [_importContext refreshObject:locationAtShop mergeChanges:NO];
        }
         */
}

#pragma mark – FIRST DATA CREATE
- (void)cleanBlueCUFirstData {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [cleanUpCUNumber cleanFOICU];
    
    [cleanUpCUNumber cleanFS220];
    [cleanUpCUNumber cleanFS220A];
    [cleanUpCUNumber cleanFS220B];
    [cleanUpCUNumber cleanFS220D];
    [cleanUpCUNumber cleanFS220G];
    [cleanUpCUNumber cleanFS220H];
    
    [cleanUpCUNumber cleanFS220PYE];
    [cleanUpCUNumber cleanFS220APYE];
    [cleanUpCUNumber cleanFS220BPYE];
    
    [cleanUpCUNumber cleanATM];
    [cleanUpCUNumber cleanBranch];

    NSLog(@"CLEAN UP OF CU_NUMBER COMPLETE");
}


@end
