//
//  FS220G+CoreDataProperties.m
//  blue_CU
//
//  Created by Timothy Milz on 2/10/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "FS220G+CoreDataProperties.h"

@implementation FS220G (CoreDataProperties)

+ (NSFetchRequest<FS220G *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"FS220G"];
}

@dynamic acct_440A;
@dynamic acct_660A;
@dynamic cu_number;

@end
