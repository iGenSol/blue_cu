//
//  cleanUpCUNumber.m
//  blue_CU
//
//  Created by Timothy Milz on 12/19/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import "AppDelegate.h"
#import "cleanUpCUNumber.h"

#import "FOICU+CoreDataProperties.h"

#import "FS220+CoreDataProperties.h"
#import "FS220A+CoreDataProperties.h"
#import "FS220B+CoreDataClass.h"
#import "FS220D+CoreDataProperties.h"
#import "FS220G+CoreDataClass.h"
#import "FS220H+CoreDataClass.h"

#import "FS220_PYE+CoreDataClass.h"
#import "FS220A_PYE+CoreDataClass.h"
#import "FS220B_PYE+CoreDataClass.h"

#import "ATMLocations+CoreDataProperties.h"
#import "BranchLocations+CoreDataProperties.h"


@implementation cleanUpCUNumber

#define debug 1

NSString *foicuCleanUpCUNumber = @"";

NSString *fs220CleanUpCUNumber = @"";
NSString *fs220aCleanUpCUNumber = @"";
NSString *fs220bCleanUpCUNumber = @"";
NSString *fs220dCleanUpCUNumber = @"";
NSString *fs220gCleanUpCUNumber = @"";
NSString *fs220hCleanUpCUNumber = @"";

NSString *fs220PYECleanUpCUNumber = @"";
NSString *fs220aPYECleanUpCUNumber = @"";
NSString *fs220bPYECleanUpCUNumber = @"";
    
NSString *atmCleanUpCUNumber = @"";
NSString *branchCleanUpCUNumber = @"";
    
+ (void)cleanFOICU {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];

    NSFetchRequest *foicuCleanUpFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FOICU"];
    NSSortDescriptor *foicuCleanUpSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [foicuCleanUpFetchRequest setSortDescriptors:[NSArray arrayWithObject:foicuCleanUpSortDescriptor]];
    NSArray *fetchedFOICUCleanUpRecords = [cdh.context executeFetchRequest:foicuCleanUpFetchRequest error:nil];
    
    for (FOICU *foicu in fetchedFOICUCleanUpRecords) {
        foicuCleanUpCUNumber = @"";
        foicuCleanUpCUNumber = [foicu.cu_number stringByReplacingOccurrencesOfString:@" " withString:@""];
        foicu.cu_number = foicuCleanUpCUNumber;
    }
    
    [cdh backgroundSaveContext];
}
    
+ (void)cleanFS220 {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *fs220CleanUpFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220"];
    NSSortDescriptor *fs220CleanUpSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220CleanUpFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220CleanUpSortDescriptor]];
    NSArray *fetchedFS220CleanUpRecords = [cdh.context executeFetchRequest:fs220CleanUpFetchRequest error:nil];
    
    for (FS220 *fs220 in fetchedFS220CleanUpRecords) {
        fs220CleanUpCUNumber = @"";
        fs220CleanUpCUNumber = [fs220.cu_number stringByReplacingOccurrencesOfString:@" " withString:@""];
        fs220.cu_number = fs220CleanUpCUNumber;
    }
    
    [cdh backgroundSaveContext];
}
    
+ (void)cleanFS220A {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *fs220aCleanUpFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220A"];
    NSSortDescriptor *fs220aCleanUpSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220aCleanUpFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220aCleanUpSortDescriptor]];
    NSArray *fetchedFS220ACleanUpRecords = [cdh.context executeFetchRequest:fs220aCleanUpFetchRequest error:nil];
    
    for (FS220A *fs220a in fetchedFS220ACleanUpRecords) {
        fs220aCleanUpCUNumber = @"";
        fs220aCleanUpCUNumber = [fs220a.cu_number stringByReplacingOccurrencesOfString:@" " withString:@""];
        fs220a.cu_number = fs220aCleanUpCUNumber;
    }
    
    [cdh backgroundSaveContext];
}

+ (void)cleanFS220B {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *fs220bCleanUpFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220B"];
    NSSortDescriptor *fs220bCleanUpSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220bCleanUpFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220bCleanUpSortDescriptor]];
    NSArray *fetchedFS220BCleanUpRecords = [cdh.context executeFetchRequest:fs220bCleanUpFetchRequest error:nil];
    
    for (FS220B *fs220b in fetchedFS220BCleanUpRecords) {
        fs220bCleanUpCUNumber = @"";
        fs220bCleanUpCUNumber = [fs220b.cu_number stringByReplacingOccurrencesOfString:@" " withString:@""];
        fs220b.cu_number = fs220bCleanUpCUNumber;
    }
    
    [cdh backgroundSaveContext];
}

+ (void)cleanFS220D {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *fs220dCleanUpFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220D"];
    NSSortDescriptor *fs220dCleanUpSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220dCleanUpFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220dCleanUpSortDescriptor]];
    NSArray *fetchedFS220DCleanUpRecords = [cdh.context executeFetchRequest:fs220dCleanUpFetchRequest error:nil];
    
    for (FS220D *fs220d in fetchedFS220DCleanUpRecords) {
        fs220dCleanUpCUNumber = @"";
        fs220dCleanUpCUNumber = [fs220d.cu_number stringByReplacingOccurrencesOfString:@" " withString:@""];
        fs220d.cu_number = fs220dCleanUpCUNumber;
    }
    
    [cdh backgroundSaveContext];
}

+ (void)cleanFS220G {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *fs220gCleanUpFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220G"];
    NSSortDescriptor *fs220gCleanUpSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220gCleanUpFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220gCleanUpSortDescriptor]];
    NSArray *fetchedFS220GCleanUpRecords = [cdh.context executeFetchRequest:fs220gCleanUpFetchRequest error:nil];
    
    for (FS220G *fs220g in fetchedFS220GCleanUpRecords) {
        fs220gCleanUpCUNumber = @"";
        fs220gCleanUpCUNumber = [fs220g.cu_number stringByReplacingOccurrencesOfString:@" " withString:@""];
        fs220g.cu_number = fs220gCleanUpCUNumber;
    }
    
    [cdh backgroundSaveContext];
}

+ (void)cleanFS220H {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *fs220hCleanUpFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220H"];
    NSSortDescriptor *fs220hCleanUpSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220hCleanUpFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220hCleanUpSortDescriptor]];
    NSArray *fetchedFS220HCleanUpRecords = [cdh.context executeFetchRequest:fs220hCleanUpFetchRequest error:nil];
    
    for (FS220H *fs220h in fetchedFS220HCleanUpRecords) {
        fs220hCleanUpCUNumber = @"";
        fs220hCleanUpCUNumber = [fs220h.cu_number stringByReplacingOccurrencesOfString:@" " withString:@""];
        fs220h.cu_number = fs220hCleanUpCUNumber;
    }
    
    [cdh backgroundSaveContext];
}

+ (void)cleanFS220PYE {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *fs220PYECleanUpFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220_PYE"];
    NSSortDescriptor *fs220PYECleanUpSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220PYECleanUpFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220PYECleanUpSortDescriptor]];
    NSArray *fetchedFS220PYECleanUpRecords = [cdh.context executeFetchRequest:fs220PYECleanUpFetchRequest error:nil];
    
    for (FS220_PYE *fs220pye in fetchedFS220PYECleanUpRecords) {
        fs220PYECleanUpCUNumber = @"";
        fs220PYECleanUpCUNumber = [fs220pye.cu_number stringByReplacingOccurrencesOfString:@" " withString:@""];
        fs220pye.cu_number = fs220PYECleanUpCUNumber;
    }
    
    [cdh backgroundSaveContext];
}

+ (void)cleanFS220APYE {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *fs220APYECleanUpFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220A_PYE"];
    NSSortDescriptor *fs220APYECleanUpSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220APYECleanUpFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220APYECleanUpSortDescriptor]];
    NSArray *fetchedFS220APYECleanUpRecords = [cdh.context executeFetchRequest:fs220APYECleanUpFetchRequest error:nil];
    
    for (FS220A_PYE *fs220apye in fetchedFS220APYECleanUpRecords) {
        fs220aPYECleanUpCUNumber = @"";
        fs220aPYECleanUpCUNumber = [fs220apye.cu_number stringByReplacingOccurrencesOfString:@" " withString:@""];
        fs220apye.cu_number = fs220aPYECleanUpCUNumber;
    }
    
    [cdh backgroundSaveContext];
}

+ (void)cleanFS220BPYE {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *fs220BPYECleanUpFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FS220B_PYE"];
    NSSortDescriptor *fs220BPYECleanUpSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [fs220BPYECleanUpFetchRequest setSortDescriptors:[NSArray arrayWithObject:fs220BPYECleanUpSortDescriptor]];
    NSArray *fetchedFS220BPYECleanUpRecords = [cdh.context executeFetchRequest:fs220BPYECleanUpFetchRequest error:nil];
    
    for (FS220B_PYE *fs220bpye in fetchedFS220BPYECleanUpRecords) {
        fs220bPYECleanUpCUNumber = @"";
        fs220bPYECleanUpCUNumber = [fs220bpye.cu_number stringByReplacingOccurrencesOfString:@" " withString:@""];
        fs220bpye.cu_number = fs220bPYECleanUpCUNumber;
    }
    
    [cdh backgroundSaveContext];
}


+ (void)cleanATM {

    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *atmCleanUpFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"ATMLocations"];
    NSSortDescriptor *atmCleanUpSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [atmCleanUpFetchRequest setSortDescriptors:[NSArray arrayWithObject:atmCleanUpSortDescriptor]];
    NSArray *fetchedATMCleanUpRecords = [cdh.context executeFetchRequest:atmCleanUpFetchRequest error:nil];
    
    for (ATMLocations *atmLocations in fetchedATMCleanUpRecords) {
        atmCleanUpCUNumber = @"";
        atmCleanUpCUNumber = [atmLocations.cu_number stringByReplacingOccurrencesOfString:@" " withString:@""];
        atmLocations.cu_number = atmCleanUpCUNumber;
    }
    
    [cdh backgroundSaveContext];

}

+ (void)cleanBranch {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *branchCleanUpFetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"BranchLocations"];
    NSSortDescriptor *branchCleanUpSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"cu_number" ascending:YES];
    [branchCleanUpFetchRequest setSortDescriptors:[NSArray arrayWithObject:branchCleanUpSortDescriptor]];
    NSArray *fetchedBranchCleanUpRecords = [cdh.context executeFetchRequest:branchCleanUpFetchRequest error:nil];
    
    for (BranchLocations *branchLocations in fetchedBranchCleanUpRecords) {
        branchCleanUpCUNumber = @"";
        branchCleanUpCUNumber = [branchLocations.cu_number stringByReplacingOccurrencesOfString:@" " withString:@""];
        branchLocations.cu_number = branchCleanUpCUNumber;
    }
    
    [cdh backgroundSaveContext];
}


@end
