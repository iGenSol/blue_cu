//
//  ATMLocations+CoreDataProperties.m
//  blue_CU
//
//  Created by Timothy Milz on 11/14/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import "ATMLocations+CoreDataProperties.h"

@implementation ATMLocations (CoreDataProperties)

+ (NSFetchRequest<ATMLocations *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ATMLocations"];
}

@dynamic cu_number;
@dynamic cycle_date;
@dynamic join_number;
@dynamic cu_name;
@dynamic siteID;
@dynamic site_name;
@dynamic site_type_name;
@dynamic site_function_name;
@dynamic phys_adr_1;
@dynamic phys_adr_2;
@dynamic phys_adr_city;
@dynamic phys_adr_state_code;
@dynamic phys_adr_state_name;
@dynamic phys_adr_postal_code;
@dynamic phys_adr_county_name;
@dynamic phys_adr_country;
@dynamic unique_key;

@end
