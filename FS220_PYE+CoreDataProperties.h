//
//  FS220_PYE+CoreDataProperties.h
//  blue_CU
//
//  Created by Timothy Milz on 2/10/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "FS220_PYE+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface FS220_PYE (CoreDataProperties)

+ (NSFetchRequest<FS220_PYE *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *cu_number;
@property (nullable, nonatomic, copy) NSString *acct_010;
@property (nullable, nonatomic, copy) NSString *acct_018;
@property (nullable, nonatomic, copy) NSString *acct_025B;
@property (nullable, nonatomic, copy) NSString *acct_083;

@end

NS_ASSUME_NONNULL_END
