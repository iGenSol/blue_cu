//
//  FS220B_PYE+CoreDataProperties.m
//  blue_CU
//
//  Created by Timothy Milz on 2/10/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "FS220B_PYE+CoreDataProperties.h"

@implementation FS220B_PYE (CoreDataProperties)

+ (NSFetchRequest<FS220B_PYE *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"FS220B_PYE"];
}

@dynamic cu_number;
@dynamic acct_781;

@end
