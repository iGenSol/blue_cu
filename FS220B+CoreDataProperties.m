//
//  FS220B+CoreDataProperties.m
//  blue_CU
//
//  Created by Timothy Milz on 2/10/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "FS220B+CoreDataProperties.h"

@implementation FS220B (CoreDataProperties)

+ (NSFetchRequest<FS220B *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"FS220B"];
}

@dynamic acct_659;
@dynamic acct_796E;
@dynamic acct_797E;
@dynamic acct_801;
@dynamic acct_945;
@dynamic cu_number;
@dynamic acct_799D;
@dynamic acct_781;

@end
