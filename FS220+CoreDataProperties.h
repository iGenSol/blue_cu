//
//  FS220+CoreDataProperties.h
//  blue_CU
//
//  Created by Timothy Milz on 2/10/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "FS220+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface FS220 (CoreDataProperties)

+ (NSFetchRequest<FS220 *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *acct_007;
@property (nullable, nonatomic, copy) NSString *acct_008;
@property (nullable, nonatomic, copy) NSString *acct_010;
@property (nullable, nonatomic, copy) NSString *acct_018;
@property (nullable, nonatomic, copy) NSString *acct_025A;
@property (nullable, nonatomic, copy) NSString *acct_025B;
@property (nullable, nonatomic, copy) NSString *acct_041B;
@property (nullable, nonatomic, copy) NSString *acct_083;
@property (nullable, nonatomic, copy) NSString *acct_084;
@property (nullable, nonatomic, copy) NSString *acct_300;
@property (nullable, nonatomic, copy) NSString *acct_340;
@property (nullable, nonatomic, copy) NSString *acct_380;
@property (nullable, nonatomic, copy) NSString *acct_386;
@property (nullable, nonatomic, copy) NSString *acct_550;
@property (nullable, nonatomic, copy) NSString *acct_551;
@property (nullable, nonatomic, copy) NSString *acct_657;
@property (nullable, nonatomic, copy) NSString *acct_668;
@property (nullable, nonatomic, copy) NSString *acct_671;
@property (nullable, nonatomic, copy) NSString *acct_703;
@property (nullable, nonatomic, copy) NSString *acct_712;
@property (nullable, nonatomic, copy) NSString *acct_719;
@property (nullable, nonatomic, copy) NSString *acct_794;
@property (nullable, nonatomic, copy) NSString *acct_799A1;
@property (nullable, nonatomic, copy) NSString *acct_799C1;
@property (nullable, nonatomic, copy) NSString *acct_799C2;
@property (nullable, nonatomic, copy) NSString *acct_825;
@property (nullable, nonatomic, copy) NSString *acct_860C;
@property (nullable, nonatomic, copy) NSString *acct_902;
@property (nullable, nonatomic, copy) NSString *acct_980;
@property (nullable, nonatomic, copy) NSString *cu_number;
@property (nullable, nonatomic, copy) NSString *current_members;
@property (nullable, nonatomic, copy) NSString *potential_members;

@end

NS_ASSUME_NONNULL_END
