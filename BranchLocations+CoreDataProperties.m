//
//  BranchLocations+CoreDataProperties.m
//  blue_CU
//
//  Created by Timothy Milz on 11/29/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import "BranchLocations+CoreDataProperties.h"

@implementation BranchLocations (CoreDataProperties)

+ (NSFetchRequest<BranchLocations *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"BranchLocations"];
}

@dynamic cu_number;
@dynamic cycle_date;
@dynamic join_number;
@dynamic siteID;
@dynamic cu_name;
@dynamic site_name;
@dynamic site_type_name;
@dynamic main_office;
@dynamic phys_adr_1;
@dynamic phys_adr_2;
@dynamic phys_adr_city;
@dynamic phys_adr_state_code;
@dynamic phys_adr_postal_code;
@dynamic phys_adr_county_name;
@dynamic phys_adr_country;
@dynamic mailing_adr_1;
@dynamic mailing_adr_2;
@dynamic mailing_adr_city;
@dynamic mailing_adr_state_code;
@dynamic mailing_adr_state_name;
@dynamic mailing_adr_postal_code;
@dynamic phone_number;
@dynamic unique_key;

@end
