//
//  FS220B+CoreDataClass.h
//  blue_CU
//
//  Created by Timothy Milz on 2/10/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface FS220B : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "FS220B+CoreDataProperties.h"
