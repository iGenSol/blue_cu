//
//  cleanUpCUNumber.h
//  blue_CU
//
//  Created by Timothy Milz on 12/19/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreDataHelper.h"

@interface cleanUpCUNumber : NSObject

+ (void)cleanFOICU;

+ (void)cleanFS220;
+ (void)cleanFS220A;
+ (void)cleanFS220B;
+ (void)cleanFS220D;
+ (void)cleanFS220G;
+ (void)cleanFS220H;
    
+ (void)cleanFS220PYE;
+ (void)cleanFS220APYE;
+ (void)cleanFS220BPYE;

+ (void)cleanATM;
+ (void)cleanBranch;

@end
