//
//  CoreDataTVC.h
//  blue_CU
//
//  Created by Tim Roadley on 19/09/13.
//  Copyright (c) 2013 Tim Roadley. All rights reserved.
//
//  Created & Modified by Timothy Milz on 10/16/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.

#import <UIKit/UIKit.h>
#import "CoreDataHelper.h"

@interface CoreDataTVC : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *frc;
@property (strong, nonatomic) NSString *foundTableDataYN; //for activity indicator

- (void)performFetch;

@end
