//
//  FS220A+CoreDataProperties.h
//  blue_CU
//
//  Created by Timothy Milz on 2/10/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "FS220A+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface FS220A (CoreDataProperties)

+ (NSFetchRequest<FS220A *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *acct_110;
@property (nullable, nonatomic, copy) NSString *acct_115;
@property (nullable, nonatomic, copy) NSString *acct_119;
@property (nullable, nonatomic, copy) NSString *acct_120;
@property (nullable, nonatomic, copy) NSString *acct_124;
@property (nullable, nonatomic, copy) NSString *acct_131;
@property (nullable, nonatomic, copy) NSString *acct_210;
@property (nullable, nonatomic, copy) NSString *acct_350;
@property (nullable, nonatomic, copy) NSString *acct_381;
@property (nullable, nonatomic, copy) NSString *acct_564A;
@property (nullable, nonatomic, copy) NSString *acct_564B;
@property (nullable, nonatomic, copy) NSString *acct_661A;
@property (nullable, nonatomic, copy) NSString *acct_718A;
@property (nullable, nonatomic, copy) NSString *acct_730A;
@property (nullable, nonatomic, copy) NSString *acct_730B;
@property (nullable, nonatomic, copy) NSString *acct_730C;
@property (nullable, nonatomic, copy) NSString *acct_798A;
@property (nullable, nonatomic, copy) NSString *acct_799I;
@property (nullable, nonatomic, copy) NSString *acct_820A;
@property (nullable, nonatomic, copy) NSString *acct_925A;
@property (nullable, nonatomic, copy) NSString *acct_997;
@property (nullable, nonatomic, copy) NSString *cu_number;

@end

NS_ASSUME_NONNULL_END
