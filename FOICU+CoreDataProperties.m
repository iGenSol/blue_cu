//
//  FOICU+CoreDataProperties.m
//  blue_CU
//
//  Created by Timothy Milz on 11/14/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import "FOICU+CoreDataProperties.h"

@implementation FOICU (CoreDataProperties)

+ (NSFetchRequest<FOICU *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"FOICU"];
}

@dynamic attention_of;
@dynamic charter_state;
@dynamic city;
@dynamic cong_dist;
@dynamic county_code;
@dynamic cu_name;
@dynamic cu_number;
@dynamic cu_type;
@dynamic cycle_date;
@dynamic district;
@dynamic issue_date;
@dynamic join_number;
@dynamic limited_inc;
@dynamic listed;
@dynamic peer_group;
@dynamic quarter_flag;
@dynamic region;
@dynamic rssd;
@dynamic se;
@dynamic smsa;
@dynamic state;
@dynamic state_code;
@dynamic street;
@dynamic tom_code;
@dynamic year_opened;
@dynamic zip_code;

@end
