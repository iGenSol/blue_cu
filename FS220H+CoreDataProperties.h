//
//  FS220H+CoreDataProperties.h
//  blue_CU
//
//  Created by Timothy Milz on 2/10/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "FS220H+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface FS220H (CoreDataProperties)

+ (NSFetchRequest<FS220H *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *cu_number;
@property (nullable, nonatomic, copy) NSString *acct_400T;
@property (nullable, nonatomic, copy) NSString *acct_814E;

@end

NS_ASSUME_NONNULL_END
