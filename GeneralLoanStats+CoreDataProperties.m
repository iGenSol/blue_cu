//
//  GeneralLoanStats+CoreDataProperties.m
//  blue_CU
//
//  Created by Timothy Milz on 3/20/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "GeneralLoanStats+CoreDataProperties.h"

@implementation GeneralLoanStats (CoreDataProperties)

+ (NSFetchRequest<GeneralLoanStats *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"GeneralLoanStats"];
}

@dynamic federallyInsuredCUCount;
@dynamic memberCount;
@dynamic totalAssets;
@dynamic averageCUAssets;
@dynamic totalInsuredSharesAndDeposits;
@dynamic returnOnAverageAssets;
@dynamic netIncome;
@dynamic netWorthRatio;
@dynamic averageSharesMember;
@dynamic totalLoans;
@dynamic averageLoanBalance;
@dynamic loanToShareRatio;
@dynamic mortgagesRealEstate;
@dynamic autoLoans;
@dynamic unsecuredCreditCards;
@dynamic other;
@dynamic delinquencyRatio;

@end
