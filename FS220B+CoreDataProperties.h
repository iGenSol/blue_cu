//
//  FS220B+CoreDataProperties.h
//  blue_CU
//
//  Created by Timothy Milz on 2/10/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "FS220B+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface FS220B (CoreDataProperties)

+ (NSFetchRequest<FS220B *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *acct_659;
@property (nullable, nonatomic, copy) NSString *acct_781;
@property (nullable, nonatomic, copy) NSString *acct_796E;
@property (nullable, nonatomic, copy) NSString *acct_797E;
@property (nullable, nonatomic, copy) NSString *acct_799D;
@property (nullable, nonatomic, copy) NSString *acct_801;
@property (nullable, nonatomic, copy) NSString *acct_945;
@property (nullable, nonatomic, copy) NSString *cu_number;

@end

NS_ASSUME_NONNULL_END
