//
//  CoreDataPickerTF.m
//  blue_CU
//
//  Created by Timothy Milz on 10/16/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import "CoreDataPickerTF.h"

@implementation CoreDataPickerTF

#define debug 1

#pragma mark - DELEGATE & DATASOURCE: UIPickerView
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    if (debug==1) {
        NSLog(@"running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (debug==1) {
        NSLog(@"running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    return [self.pickerData count];
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    
    if (debug==1) {
        NSLog(@"running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    return 44.0f;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    
    if (debug==1) {
        NSLog(@"running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    return 300.0f;
}

// Code excluded - Using viewForRow in PickerTF classes so can left justify text

/* -(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
 
 if (debug==1) {
 NSLog(@"running %@ '%@'", self.class, NSStringFromSelector(_cmd));
 }
 
 return [self.pickerData objectAtIndex:row];
 } */


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (debug==1) {
        NSLog(@"running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    NSManagedObject *object = [self.pickerData objectAtIndex:row];
    [self.pickerDelegate selectedObjectID:object.objectID changedForPickerTF:self];
}


#pragma mark - INTERACTION
-(void)done {
    
    if (debug==1) {
        NSLog(@"running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [self resignFirstResponder];
}


#pragma mark - DATA
-(void)fetch:primerPickerValue {
    [NSException raise:NSInternalInconsistencyException format:@"You must override the '%@' method to provide data to the picker", NSStringFromSelector(_cmd)];
}

-(void)selectDefaultRow:primerPickerValue {
    [NSException raise:NSInternalInconsistencyException format:@"You must override the '%@' method to set the default picker row", NSStringFromSelector(_cmd)];
}

#pragma mark - VIEW
-(UIView *)createInputView {
    
    if (debug==1) {
        NSLog(@"running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    self.picker = [[UIPickerView alloc] initWithFrame:CGRectZero];
    self.picker.showsSelectionIndicator = YES;
    self.picker.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.picker.dataSource = self;
    self.picker.delegate = self;
    [self fetch:_pickerValue];
    return self.picker;
}

-(UIView *)createInputAccessoryView {
    
    if (debug==1) {
        NSLog(@"running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    self.showToolBar = YES;
    if (!self.toolBar && self.showToolBar) {
        self.toolBar = [[UIToolbar alloc] init];
        self.toolBar.barStyle = UIBarStyleBlackTranslucent;
        self.toolBar.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        [self.toolBar sizeToFit];
        CGRect frame = self.toolBar.frame;
        frame.size.height = 44.0f;
        self.toolBar.frame = frame;
        
        UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done)];
        
        NSArray *array = [NSArray arrayWithObjects:spacer, doneBtn, nil];
        [self.toolBar setItems:array];
    }
    return self.toolBar;
}

-(id)initWithFrame:(CGRect)aRect {
    
    if (debug==1) {
        NSLog(@"running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    if (self = [super initWithFrame:aRect]) {
        self.inputView = [self createInputView];
        self.inputAccessoryView = [self createInputAccessoryView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    
    if (debug==1) {
        NSLog(@"running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    if (self = [super initWithCoder:aDecoder]) {
        self.inputView = [self createInputView];
        self.inputAccessoryView = [self createInputAccessoryView];
    }
    return self;
}

-(void)deviceDidRotate:(NSNotification*)notification {
    
    if (debug==1) {
        NSLog(@"running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [self.picker setNeedsLayout];
}

@end
