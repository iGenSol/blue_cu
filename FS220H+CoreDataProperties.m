//
//  FS220H+CoreDataProperties.m
//  blue_CU
//
//  Created by Timothy Milz on 2/10/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "FS220H+CoreDataProperties.h"

@implementation FS220H (CoreDataProperties)

+ (NSFetchRequest<FS220H *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"FS220H"];
}

@dynamic cu_number;
@dynamic acct_400T;
@dynamic acct_814E;

@end
