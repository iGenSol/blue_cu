//
//  BranchLocations+CoreDataProperties.h
//  blue_CU
//
//  Created by Timothy Milz on 11/29/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import "BranchLocations+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface BranchLocations (CoreDataProperties)

+ (NSFetchRequest<BranchLocations *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *cu_number;
@property (nullable, nonatomic, copy) NSString *cycle_date;
@property (nullable, nonatomic, copy) NSString *join_number;
@property (nullable, nonatomic, copy) NSString *siteID;
@property (nullable, nonatomic, copy) NSString *cu_name;
@property (nullable, nonatomic, copy) NSString *site_name;
@property (nullable, nonatomic, copy) NSString *site_type_name;
@property (nullable, nonatomic, copy) NSString *main_office;
@property (nullable, nonatomic, copy) NSString *phys_adr_1;
@property (nullable, nonatomic, copy) NSString *phys_adr_2;
@property (nullable, nonatomic, copy) NSString *phys_adr_city;
@property (nullable, nonatomic, copy) NSString *phys_adr_state_code;
@property (nullable, nonatomic, copy) NSString *phys_adr_postal_code;
@property (nullable, nonatomic, copy) NSString *phys_adr_county_name;
@property (nullable, nonatomic, copy) NSString *phys_adr_country;
@property (nullable, nonatomic, copy) NSString *mailing_adr_1;
@property (nullable, nonatomic, copy) NSString *mailing_adr_2;
@property (nullable, nonatomic, copy) NSString *mailing_adr_city;
@property (nullable, nonatomic, copy) NSString *mailing_adr_state_code;
@property (nullable, nonatomic, copy) NSString *mailing_adr_state_name;
@property (nullable, nonatomic, copy) NSString *mailing_adr_postal_code;
@property (nullable, nonatomic, copy) NSString *phone_number;
@property (nullable, nonatomic, copy) NSString *unique_key;

@end

NS_ASSUME_NONNULL_END
