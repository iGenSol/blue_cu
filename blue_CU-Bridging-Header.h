//
//  blue_CU-Bridging-Header.h
//  blue_CU
//
//  Created by Timothy Milz on 4/14/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

/*
To use the Objective-C declarations in files in the same framework target as your Swift code, you’ll need to import those files into the Objective-C umbrella header—the master header for your framework.
 
 Import your Objective-C files by configuring the umbrella header:
        Under Build Settings, in Packaging, make sure the Defines Module setting for the framework target is set to Yes.
        In the umbrella header, import every Objective-C header you want to expose to Swift.

 Swift sees every header you expose publicly in your umbrella header. The contents of the Objective-C files in that framework are automatically available from any Swift file within that framework target, with no import statements. Use classes and other declarations from your Objective-C code with the same Swift syntax you use for system classes.
 */

#ifndef blue_CU_Bridging_Header_h
#define blue_CU_Bridging_Header_h

// Original blue_CU header files exposes for swift code
#import "AppDelegate.h"
#import "CoreDataHelper.h"
#import "selectFOICUViewController.h"
#import "FOICUViewController.h"
#import "FOICU+CoreDataClass.h"
#import "ExtendedInfoViewController.h"
#import "ExtendedInfoTabBarContoller.h"
#import "ATMLocationsViewController.h"
#import "ATMMapLocationViewController.h"
#import "BranchLocationsViewController.h"
#import "BranchMapLocationViewController.h"
#import "ManagementViewController.h"

#endif /* blue_CU_Bridging_Header_h */
