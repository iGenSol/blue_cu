//
//  BranchLocations+CoreDataClass.h
//  blue_CU
//
//  Created by Timothy Milz on 11/29/16.
//  Copyright © 2016 iGenerateSolutions Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface BranchLocations : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "BranchLocations+CoreDataProperties.h"
