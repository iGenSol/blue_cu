//
//  FS220A_PYE+CoreDataProperties.h
//  blue_CU
//
//  Created by Timothy Milz on 2/10/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import "FS220A_PYE+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface FS220A_PYE (CoreDataProperties)

+ (NSFetchRequest<FS220A_PYE *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *cu_number;
@property (nullable, nonatomic, copy) NSString *acct_730B;
@property (nullable, nonatomic, copy) NSString *acct_730C;
@property (nullable, nonatomic, copy) NSString *acct_799I;
@property (nullable, nonatomic, copy) NSString *acct_997;

@end

NS_ASSUME_NONNULL_END
