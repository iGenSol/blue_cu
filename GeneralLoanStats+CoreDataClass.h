//
//  GeneralLoanStats+CoreDataClass.h
//  blue_CU
//
//  Created by Timothy Milz on 3/20/17.
//  Copyright © 2017 iGenerateSolutions Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface GeneralLoanStats : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "GeneralLoanStats+CoreDataProperties.h"
